var Controls = [];

exports.GetById = function() {
    for (var i in Controls) ;
};

exports.C = function(element, style, id) {
    var view;
    "view" == element ? view = Ti.UI.createView(style) : "image" == element ? view = Ti.UI.createImageView(style) : "label" == element ? view = Ti.UI.createLabel(style) : "text" == element ? view = Ti.UI.createTextField(style) : "webview" == element && (view = Ti.UI.createWebView(style));
    if (id) {
        view.id = id;
        Controls.push(view);
    }
    view.Bind = function(event, fun) {
        view.addEventListener(event, fun);
    };
    return view;
};

exports.Controller = function(name, param) {
    return Alloy.createController(name, param).getView();
};

exports.Open = function(name, param) {
    Alloy.createController(name, param).getView().open();
};