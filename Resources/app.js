var Alloy = require("alloy"), _ = Alloy._, Backbone = Alloy.Backbone;

Alloy.Globals.osname = Ti.Platform.osname;

Alloy.Globals.version = Ti.Platform.version;

Alloy.Globals.height = Ti.Platform.displayCaps.platformHeight;

Alloy.Globals.width = Ti.Platform.displayCaps.platformWidth;

Alloy.Globals.WebUrl = "http://www.buyling.com";

Alloy.Globals.ApiUrl = Alloy.Globals.WebUrl + "/ajax";

Alloy.Globals.LoginToken = "";

var iOS7 = function() {
    var version = Titanium.Platform.version.split(".");
    var major = parseInt(version[0], 10);
    if (major >= 7) return true;
    return false;
};

Alloy.Globals.WindowTop = iOS7 ? 20 : 0;

Ti.UI.setBackgroundColor("#fff");

Alloy.createController("index");