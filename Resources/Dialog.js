exports.alertSure = function() {
    var alert = Titanium.UI.createAlertDialog({
        title: "提示",
        buttonNames: [ "确定", "取消" ],
        cancel: 1,
        ok: 0
    });
    var obj = this;
    obj = {
        Show: function(msg, success, cancel) {
            alert.message = msg;
            alert.show();
            alert.addEventListener("click", function(e) {
                0 == e.index ? success() : cancel && cancel();
            });
        }
    };
    return obj;
};

exports.load = function() {
    var messageWin = Titanium.UI.createWindow({
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        borderRadius: 10,
        touchEnabled: false,
        orientationModes: [ Ti.UI.PORTRAIT ]
    });
    var backgroundBox = Ti.UI.createView({
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        borderRadius: 10,
        touchEnabled: false,
        backgroundColor: "#333",
        opacity: .3
    });
    var loadBox = Ti.UI.createView({
        backgroundColor: "#000",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        borderRadius: 10,
        top: Alloy.Globals.height / 2 - 10,
        left: Alloy.Globals.width / 2,
        layout: "vertical"
    });
    var activityIndicator = Ti.UI.createActivityIndicator({
        color: "#fff",
        font: {
            fontSize: 12
        },
        style: Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN,
        top: 3,
        left: 3,
        right: 3,
        bottom: 3,
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE
    });
    loadBox.add(activityIndicator);
    messageWin.add(backgroundBox);
    messageWin.add(loadBox);
    var _load = this;
    _load = {
        Show: function(msg) {
            activityIndicator.message = msg;
            loadBox.left = (Alloy.Globals.width - 18 * msg.length) / 2;
            messageWin.open();
            activityIndicator.show();
        },
        Hide: function() {
            messageWin.close();
            activityIndicator.hide();
        }
    };
    return _load;
};

exports.prompt = function() {
    messageWin = Titanium.UI.createWindow({
        height: 30,
        width: 250,
        bottom: 70,
        borderRadius: 10,
        touchEnabled: false,
        orientationModes: [ Titanium.UI.PORTRAIT, Titanium.UI.UPSIDE_PORTRAIT, Titanium.UI.LANDSCAPE_LEFT, Titanium.UI.LANDSCAPE_RIGHT ]
    });
    "iphone" === Ti.Platform.osname && (messageWin.orientationModes = [ Ti.UI.PORTRAIT ]);
    var messageView = Titanium.UI.createView({
        id: "messageview",
        height: "30dp",
        width: 250,
        borderRadius: 10,
        backgroundColor: "#000",
        opacity: .5,
        touchEnabled: false
    });
    var messageLabel = Titanium.UI.createLabel({
        id: "messagelabel",
        text: "",
        color: "#fff",
        width: 250,
        height: "auto",
        font: {
            fontFamily: "Helvetica Neue",
            fontSize: "13dp"
        },
        textAlign: "center"
    });
    messageWin.add(messageView);
    messageWin.add(messageLabel);
    var _prompt = this;
    _prompt = {
        Show: function(msg, time) {
            messageLabel.text = msg;
            messageWin.open();
            time && setTimeout(function() {
                messageWin.close({
                    opacity: 0,
                    duration: 500
                });
            }, time);
        },
        Hide: function() {
            messageWin.close({
                opacity: 0,
                duration: 500
            });
        }
    };
    return _prompt;
};