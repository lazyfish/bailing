function getCache(url) {
    var sql = "select * from " + cache.config.adapter.collection_name + " where Url='" + url + "' Limit 1 Offset 0";
    Ti.API.info(sql);
    cache.fetch({
        query: sql
    });
    Ti.API.info(cache.length);
    return cache.length > 0 ? cache.at(0) : null;
}

var cache = Alloy.createCollection("HttpCache");

exports.get = function(url, success, error) {
    var ajax = Ti.Network.createHTTPClient({
        onload: function() {
            success(this.responseText);
        },
        onerror: function(e) {
            if (error) {
                error(e);
                return;
            }
            alert("error:" + e.error + "|code:" + e.code);
        }
    });
    ajax.open("GET", url);
    ajax.send();
};

exports.json = function(url, itemFun, finishFun, error, showLoading, loadingMsg) {
    if (showLoading) {
        var actInd = Titanium.UI.createActivityIndicator({
            width: Ti.UI.SIZE,
            height: Ti.UI.SIZE,
            color: "#fff",
            message: loadingMsg
        });
        showLoading.add(actInd);
        actInd.show();
    }
    cache.fetch();
    var curCache = getCache(url);
    if (curCache) {
        Ti.API.info("Read Cache");
        var data = curCache.get("Json");
        Ti.API.info("CacheContent:" + data);
        if (data) {
            var startIndex = data.indexOf("[");
            data.substr(0, startIndex);
            var jsonStr = data.substr(startIndex);
            JSON.parse(jsonStr);
            new Array();
        }
    }
    var ajax = Ti.Network.createHTTPClient({
        onload: function() {
            Ti.API.info("responseText:" + this.responseText);
            if (null == curCache) {
                curCache = Alloy.createModel("HttpCache", {
                    Url: url,
                    Json: this.responseText
                });
                cache.add(curCache);
            } else this.responseText != curCache.get("Json") && curCache.set({
                Url: url,
                Json: this.responseText
            });
            curCache.save();
            Ti.API.info("Read New Data");
            var data = this.responseText;
            if (data) {
                var startIndex = data.indexOf("[");
                var total = data.substr(0, startIndex);
                var jsonStr = data.substr(startIndex);
                var json = JSON.parse(jsonStr);
                var resultArray = new Array();
                for (var i in json) resultArray.push(itemFun(json[i], i, json.length, total));
            }
            finishFun && finishFun(resultArray);
        },
        onerror: function(e) {
            if (error) {
                error(e);
                return;
            }
            alert("error:" + e.error + "|code:" + e.code + "|url:" + url);
        }
    });
    ajax.open("GET", url);
    ajax.send();
};

exports.GetCacheImg = function(url) {
    var curCache = getCache(url);
    if (curCache) return curCache.get("Json");
    curCache = Alloy.createModel("HttpCache", {
        Url: url,
        Json: this.responseText
    });
    cache.add(curCache);
};

exports.post = function(url, param, success, error) {
    var ajax = Ti.Network.createHTTPClient({
        onload: function(e) {
            success(this.responseText, e, this);
        },
        onerror: function(e) {
            if (error) {
                error(e);
                return;
            }
            alert("error:" + e.error);
        }
    });
    ajax.open("POST", url);
    ajax.send(param);
};