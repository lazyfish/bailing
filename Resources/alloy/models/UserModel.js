exports.definition = {
    config: {
        columns: {
            Token: "string"
        },
        adapter: {
            type: "sql",
            collection_name: "UserModel"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("UserModel", exports.definition, []);

collection = Alloy.C("UserModel", exports.definition, model);

exports.Model = model;

exports.Collection = collection;