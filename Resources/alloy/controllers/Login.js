function Controller() {
    function Close() {
        $.win.close();
    }
    function Register() {
        UI.Open("Register");
        $.win.close();
    }
    function Submit() {
        var param = {
            name: $.UserName.value,
            password: $.Password.value
        };
        Ajax.post(Alloy.Globals.ApiUrl + "/user.ashx?action=login", param, function(data) {
            try {
                if ("no exist" == data) alert("当前用户不存在!"); else if ("error" == data) alert("用户名或密码错误，请重新输入!"); else {
                    var users = Alloy.createCollection("UserModel");
                    var model;
                    users.fetch();
                    if (users.length > 0) {
                        model = users.at(0);
                        model.set({
                            Token: data
                        });
                    } else {
                        model = Alloy.createModel("UserModel", {
                            Token: data
                        });
                        users.add(model);
                    }
                    model.save();
                    Alloy.Globals.LoginToken = data;
                    var prompt = new Dialog.prompt();
                    prompt.Show("登录成功！", 2e3);
                    setTimeout(function() {
                        $.win.close();
                    }, 2e3);
                }
            } catch (e) {
                alert(e.message);
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Login";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId78 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId78"
    });
    $.__views.TitleBar.add($.__views.__alloyId78);
    Close ? $.__views.__alloyId78.addEventListener("click", Close) : __defers["$.__views.__alloyId78!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId78.add($.__views.btnBack);
    $.__views.__alloyId79 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "登录",
        id: "__alloyId79"
    });
    $.__views.TitleBar.add($.__views.__alloyId79);
    $.__views.__alloyId80 = Ti.UI.createView({
        height: "100%",
        width: "70dp",
        right: "0dp",
        id: "__alloyId80"
    });
    $.__views.TitleBar.add($.__views.__alloyId80);
    $.__views.__alloyId81 = Ti.UI.createView({
        height: "35dp",
        top: "2dp",
        id: "__alloyId81"
    });
    $.__views.__alloyId80.add($.__views.__alloyId81);
    Register ? $.__views.__alloyId81.addEventListener("click", Register) : __defers["$.__views.__alloyId81!click!Register"] = true;
    $.__views.__alloyId82 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId82"
    });
    $.__views.__alloyId81.add($.__views.__alloyId82);
    $.__views.__alloyId83 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId83"
    });
    $.__views.__alloyId81.add($.__views.__alloyId83);
    $.__views.__alloyId84 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "注册",
        id: "__alloyId84"
    });
    $.__views.__alloyId83.add($.__views.__alloyId84);
    $.__views.__alloyId85 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId85"
    });
    $.__views.__alloyId81.add($.__views.__alloyId85);
    $.__views.__alloyId86 = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#414141",
        top: "40dp",
        id: "__alloyId86"
    });
    $.__views.win.add($.__views.__alloyId86);
    $.__views.__alloyId87 = Ti.UI.createView({
        borderWidth: "1",
        borderColor: "#2F2F2F",
        backgroundColor: "#5E5F5F",
        borderRadius: "20",
        height: Ti.UI.SIZE,
        layout: "vertical",
        width: "90%",
        top: "20dp",
        id: "__alloyId87"
    });
    $.__views.__alloyId86.add($.__views.__alloyId87);
    $.__views.__alloyId88 = Ti.UI.createView({
        top: "5dp",
        left: "20dp",
        right: "20dp",
        height: "40dp",
        id: "__alloyId88"
    });
    $.__views.__alloyId87.add($.__views.__alloyId88);
    $.__views.__alloyId89 = Ti.UI.createLabel({
        left: "0dp",
        width: "55dp",
        font: {
            fontSize: "16dp"
        },
        color: "#fff",
        text: "账号：",
        id: "__alloyId89"
    });
    $.__views.__alloyId88.add($.__views.__alloyId89);
    $.__views.UserName = Ti.UI.createTextField({
        font: {
            fontSize: "17dp"
        },
        left: "55dp",
        right: "0",
        color: "#fff",
        backgroundColor: "#5E5F5F",
        borderColor: "#5E5F5F",
        id: "UserName",
        value: "dsc",
        hintText: "在此输入用户名或手机号"
    });
    $.__views.__alloyId88.add($.__views.UserName);
    $.__views.__alloyId90 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#434343",
        id: "__alloyId90"
    });
    $.__views.__alloyId87.add($.__views.__alloyId90);
    $.__views.__alloyId91 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#6C6D6D",
        id: "__alloyId91"
    });
    $.__views.__alloyId87.add($.__views.__alloyId91);
    $.__views.__alloyId92 = Ti.UI.createView({
        top: "5dp",
        left: "20dp",
        right: "20dp",
        height: "40dp",
        id: "__alloyId92"
    });
    $.__views.__alloyId87.add($.__views.__alloyId92);
    $.__views.__alloyId93 = Ti.UI.createLabel({
        left: "0dp",
        width: "55dp",
        font: {
            fontSize: "16dp"
        },
        color: "#fff",
        text: "密码：",
        id: "__alloyId93"
    });
    $.__views.__alloyId92.add($.__views.__alloyId93);
    $.__views.Password = Ti.UI.createTextField({
        font: {
            fontSize: "17dp"
        },
        left: "55dp",
        right: "0",
        color: "#fff",
        backgroundColor: "#5E5F5F",
        borderColor: "#5E5F5F",
        id: "Password",
        value: "123",
        passwordMask: "true"
    });
    $.__views.__alloyId92.add($.__views.Password);
    $.__views.__alloyId94 = Ti.UI.createView({
        height: "40dp",
        left: "20dp",
        top: "20dp",
        right: "20dp",
        id: "__alloyId94"
    });
    $.__views.__alloyId86.add($.__views.__alloyId94);
    Submit ? $.__views.__alloyId94.addEventListener("click", Submit) : __defers["$.__views.__alloyId94!click!Submit"] = true;
    $.__views.__alloyId95 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId95"
    });
    $.__views.__alloyId94.add($.__views.__alloyId95);
    $.__views.__alloyId96 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId96"
    });
    $.__views.__alloyId94.add($.__views.__alloyId96);
    $.__views.__alloyId97 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "登录",
        id: "__alloyId97"
    });
    $.__views.__alloyId96.add($.__views.__alloyId97);
    $.__views.__alloyId98 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId98"
    });
    $.__views.__alloyId94.add($.__views.__alloyId98);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Ajax = require("Ajax");
    var Dialog = require("Dialog");
    __defers["$.__views.__alloyId78!click!Close"] && $.__views.__alloyId78.addEventListener("click", Close);
    __defers["$.__views.__alloyId81!click!Register"] && $.__views.__alloyId81.addEventListener("click", Register);
    __defers["$.__views.__alloyId94!click!Submit"] && $.__views.__alloyId94.addEventListener("click", Submit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;