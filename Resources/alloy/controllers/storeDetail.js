function Controller() {
    function ReadStoreInfo() {
        var url = Alloy.Globals.ApiUrl + "/store.ashx?action=info&id=" + id;
        Ajax.json(url, function(item) {
            longitude = item.CoordinateLng;
            latitude = item.CoordinateLat;
            vendorId = item.Parent;
            $.Introduce.html = Utility.GetWebView(item.StoreInfo);
            var pics = item.StorePicUrl.split(",");
            pics[0] && ($.Faces.image = Alloy.Globals.WebUrl + pics[0]);
            if (Alloy.Globals.LoginToken) {
                var url = Alloy.Globals.ApiUrl + "/user.ashx?action=getLevelByVendor&idguid=" + Alloy.Globals.LoginToken + "&vendor=" + vendorId;
                Ajax.get(url, function(data) {
                    var json = JSON.parse(data);
                    if (json.length > 0) {
                        vipName = json[0].name;
                        discount = json[0].discount;
                    }
                    BindProduct();
                });
            }
        });
    }
    function OpenMap() {
        UI.Open("storeLocation", {
            longitude: longitude,
            latitude: latitude
        });
    }
    function Close() {
        $.win.close();
    }
    function ShowIntroduce() {
        $.scrolView.setCurrentPage(2);
        $.border3.setBackgroundColor("#bbb");
        $.border2.setBackgroundColor("#bbb");
        $.border1.setBackgroundColor("#B13531");
    }
    function ShowActivity() {
        $.scrolView.setCurrentPage(0);
        $.border2.setBackgroundColor("#B13531");
        $.border1.setBackgroundColor("#bbb");
        $.border3.setBackgroundColor("#bbb");
    }
    function ShowProduct() {
        $.scrolView.setCurrentPage(1);
        $.border3.setBackgroundColor("#B13531");
        $.border1.setBackgroundColor("#bbb");
        $.border2.setBackgroundColor("#bbb");
    }
    function BindProduct() {
        hasLoadedPro = true;
        productUrl = Alloy.Globals.ApiUrl + "/product.ashx?action=getprobystoreid&id=" + id;
        productMoreView = UI.Controller("MoreRow", {
            url: productUrl,
            index: 0,
            itemFun: LoadProductItem,
            finishFun: LoadProductFinish
        });
        Ajax.json(productUrl, LoadProductItem, LoadProductFinish);
    }
    function LoadProductItem(item) {
        var dataItem = UI.Controller("vendorProRow", {
            id: item.id,
            face: item.CoverPicture,
            title: item.ProName,
            content: item.ProContent,
            Storeid: id,
            vipName: vipName,
            vendorId: vendorId,
            discount: discount,
            price: item.oPrice,
            pics: item.ProPics
        });
        productDataList.push(dataItem);
        return dataItem;
    }
    function LoadProductFinish(data, total) {
        $.ProductTable.setData(productDataList);
        parseInt(total) > productDataList.length && $.ProductTable.appendRow(productMoreView);
    }
    function BindActivity() {
        hasLoadedActivity = true;
        activityUrl = Alloy.Globals.ApiUrl + "/product.ashx?action=getprobystoreid&t=2&id=" + id;
        Ti.API.debug("BindActivity");
        activityMoreView = UI.Controller("MoreRow", {
            url: activityUrl,
            index: 0,
            itemFun: LoadActivityItem,
            finishFun: LoadActivityFinish
        });
        Ajax.json(activityUrl, LoadActivityItem, LoadActivityFinish);
    }
    function LoadActivityItem(item) {
        var dataItem = UI.Controller("activityRow", {
            id: item.id,
            face: item.CoverPicture,
            title: item.ProName,
            content: item.ProContent,
            Storeid: id,
            vendorId: vendorId,
            price: item.oPrice,
            pics: item.ProPics
        });
        activityDataList.push(dataItem);
        return dataItem;
    }
    function LoadActivityFinish(data, total) {
        $.ActivityTable.setData(activityDataList);
        parseInt(total) > activityDataList.length && $.ActivityTable.appendRow(activityMoreView);
    }
    function ChangePage(e) {
        0 == e.currentPage ? ShowActivity() : 1 == e.currentPage ? ShowProduct() : 2 == e.currentPage && ShowIntroduce();
    }
    function Fav() {
        if (!haveFav) {
            var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=add&idguid=" + Alloy.Globals.LoginToken + "&conid=" + id + "&contype=2";
            Ajax.get(url, function(data) {
                if ("ok" == data) {
                    var prompt = new Dialog.prompt();
                    prompt.Show("收藏成功！", 2e3);
                    $.btnUnFav.setImage("/star_on.png");
                }
            });
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "storeDetail";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId334 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId334"
    });
    $.__views.TitleBar.add($.__views.__alloyId334);
    Close ? $.__views.__alloyId334.addEventListener("click", Close) : __defers["$.__views.__alloyId334!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId334.add($.__views.btnBack);
    $.__views.Title = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "门店信息",
        id: "Title"
    });
    $.__views.TitleBar.add($.__views.Title);
    $.__views.__alloyId335 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        right: "30dp",
        id: "__alloyId335"
    });
    $.__views.TitleBar.add($.__views.__alloyId335);
    Fav ? $.__views.__alloyId335.addEventListener("click", Fav) : __defers["$.__views.__alloyId335!click!Fav"] = true;
    $.__views.btnUnFav = Ti.UI.createImageView({
        image: "/star_off.png",
        width: "35dp",
        height: Ti.UI.SIZE,
        id: "btnUnFav"
    });
    $.__views.__alloyId335.add($.__views.btnUnFav);
    $.__views.__alloyId336 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        right: "0dp",
        id: "__alloyId336"
    });
    $.__views.TitleBar.add($.__views.__alloyId336);
    $.__views.btnGps = Ti.UI.createImageView({
        image: "/icon_gps.png",
        width: "30dp",
        height: "30dp",
        id: "btnGps"
    });
    $.__views.__alloyId336.add($.__views.btnGps);
    OpenMap ? $.__views.btnGps.addEventListener("click", OpenMap) : __defers["$.__views.btnGps!click!OpenMap"] = true;
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.Faces = Ti.UI.createImageView({
        layout: "horizontal",
        height: "150dp",
        horizontalWrap: false,
        scrollType: "horizontal",
        showHorizontalScrollIndicator: true,
        id: "Faces",
        top: "0"
    });
    $.__views.MainWin.add($.__views.Faces);
    $.__views.__alloyId337 = Ti.UI.createScrollView({
        layout: "horizontal",
        top: "150dp",
        height: "30dp",
        id: "__alloyId337"
    });
    $.__views.MainWin.add($.__views.__alloyId337);
    $.__views.__alloyId338 = Ti.UI.createView({
        layout: "vertical",
        left: "1dp",
        height: "30dp",
        width: "32%",
        id: "__alloyId338"
    });
    $.__views.__alloyId337.add($.__views.__alloyId338);
    ShowActivity ? $.__views.__alloyId338.addEventListener("click", ShowActivity) : __defers["$.__views.__alloyId338!click!ShowActivity"] = true;
    $.__views.__alloyId339 = Ti.UI.createLabel({
        width: "100%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        color: "#333",
        top: "2dp",
        height: "18dp",
        font: {
            fontSize: "13dp"
        },
        text: "活动展示",
        id: "__alloyId339"
    });
    $.__views.__alloyId338.add($.__views.__alloyId339);
    $.__views.border2 = Ti.UI.createView({
        top: "7dp",
        width: "90%",
        height: "3dp",
        id: "border2",
        backgroundColor: "#B13531"
    });
    $.__views.__alloyId338.add($.__views.border2);
    $.__views.__alloyId340 = Ti.UI.createView({
        layout: "vertical",
        left: "1dp",
        height: "30dp",
        width: "32%",
        id: "__alloyId340"
    });
    $.__views.__alloyId337.add($.__views.__alloyId340);
    ShowProduct ? $.__views.__alloyId340.addEventListener("click", ShowProduct) : __defers["$.__views.__alloyId340!click!ShowProduct"] = true;
    $.__views.__alloyId341 = Ti.UI.createLabel({
        width: "100%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        color: "#333",
        top: "2dp",
        height: "18dp",
        font: {
            fontSize: "13dp"
        },
        text: "产品展架",
        id: "__alloyId341"
    });
    $.__views.__alloyId340.add($.__views.__alloyId341);
    $.__views.border3 = Ti.UI.createView({
        top: "7dp",
        width: "90%",
        height: "3dp",
        id: "border3"
    });
    $.__views.__alloyId340.add($.__views.border3);
    $.__views.__alloyId342 = Ti.UI.createView({
        layout: "vertical",
        left: "1dp",
        height: "30dp",
        width: "32%",
        id: "__alloyId342"
    });
    $.__views.__alloyId337.add($.__views.__alloyId342);
    ShowIntroduce ? $.__views.__alloyId342.addEventListener("click", ShowIntroduce) : __defers["$.__views.__alloyId342!click!ShowIntroduce"] = true;
    $.__views.__alloyId343 = Ti.UI.createLabel({
        width: "100%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        color: "#333",
        top: "2dp",
        height: "18dp",
        font: {
            fontSize: "13dp"
        },
        text: "品牌介绍",
        id: "__alloyId343"
    });
    $.__views.__alloyId342.add($.__views.__alloyId343);
    $.__views.border1 = Ti.UI.createView({
        top: "7dp",
        width: "90%",
        height: "3dp",
        id: "border1"
    });
    $.__views.__alloyId342.add($.__views.border1);
    var __alloyId344 = [];
    $.__views.ActivityTable = Ti.UI.createTableView({
        backgroundColor: "#fff",
        separatorStyle: "0",
        id: "ActivityTable"
    });
    __alloyId344.push($.__views.ActivityTable);
    $.__views.ProductTable = Ti.UI.createTableView({
        backgroundColor: "#fff",
        separatorStyle: "0",
        id: "ProductTable"
    });
    __alloyId344.push($.__views.ProductTable);
    $.__views.__alloyId345 = Ti.UI.createScrollView({
        id: "__alloyId345"
    });
    __alloyId344.push($.__views.__alloyId345);
    $.__views.Introduce = Ti.UI.createWebView({
        color: "#333",
        font: {
            fontSize: "14dp"
        },
        disableBounce: "true",
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "Introduce"
    });
    $.__views.__alloyId345.add($.__views.Introduce);
    $.__views.scrolView = Ti.UI.createScrollableView({
        views: __alloyId344,
        id: "scrolView",
        height: Ti.UI.SIZE,
        top: "180dp"
    });
    $.__views.MainWin.add($.__views.scrolView);
    ChangePage ? $.__views.scrolView.addEventListener("scrollend", ChangePage) : __defers["$.__views.scrolView!scrollend!ChangePage"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var Dialog = require("Dialog");
    var Utility = require("Utility");
    var args = arguments[0] || {};
    var id = args.id || "";
    var name = args.name || "";
    var hasLoadedPro = false;
    var hasLoadedActivity = false;
    var longitude = 0;
    var latitude = 0;
    var vendorId = "";
    var discount = 100;
    var vipName = "";
    var activityUrl = "";
    var activityMoreView = null;
    var activityDataList = new Array();
    var productMoreView = null;
    var productDataList = new Array();
    $.Title.setText(name);
    ReadStoreInfo();
    BindActivity();
    var haveFav = false;
    __defers["$.__views.__alloyId334!click!Close"] && $.__views.__alloyId334.addEventListener("click", Close);
    __defers["$.__views.__alloyId335!click!Fav"] && $.__views.__alloyId335.addEventListener("click", Fav);
    __defers["$.__views.btnGps!click!OpenMap"] && $.__views.btnGps.addEventListener("click", OpenMap);
    __defers["$.__views.__alloyId338!click!ShowActivity"] && $.__views.__alloyId338.addEventListener("click", ShowActivity);
    __defers["$.__views.__alloyId340!click!ShowProduct"] && $.__views.__alloyId340.addEventListener("click", ShowProduct);
    __defers["$.__views.__alloyId342!click!ShowIntroduce"] && $.__views.__alloyId342.addEventListener("click", ShowIntroduce);
    __defers["$.__views.scrolView!scrollend!ChangePage"] && $.__views.scrolView.addEventListener("scrollend", ChangePage);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;