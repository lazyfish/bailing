function Controller() {
    function BindActivity() {
        var url = Alloy.Globals.ApiUrl + "/product.ashx?action=getgroup&psize=6&NowPage=" + pageIndex;
        Ajax.json(url, function(item) {
            return UI.Controller("activityRow", {
                id: item.id,
                face: item.CoverPicture,
                title: item.ProName,
                content: item.ProContent,
                Storeid: item.StoreId,
                vendorId: item.VendorId,
                price: item.Price,
                pics: item.ProPics
            });
        }, function(data) {
            $.table.setData(data);
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Activity";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.win = Ti.UI.createView({
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.body = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#fff",
        id: "body"
    });
    $.__views.win.add($.__views.body);
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox"
    });
    $.__views.body.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var pageIndex = 1;
    BindActivity();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;