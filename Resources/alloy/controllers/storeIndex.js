function Controller() {
    function InitInfoBox() {
        try {
            GetInfoItem("商家简介", function() {
                Alloy.createController("vendorIntroduce").getView().open();
            });
            GetInfoItem("商品展示", function() {
                Alloy.createController("vendorProduct").getView().open();
            });
            GetInfoItem("连锁门店", function() {
                Alloy.createController("vendorStore").getView().open();
            });
            GetInfoItem("热线电话  400-000-00000", function() {
                Titanium.Platform.openURL("tel:10086");
            }, false);
        } catch (ex) {
            alert(ex.message);
        }
    }
    function GetInfoItem(title, clickEvent, addBorder) {
        addBorder = addBorder || true;
        var view = Ti.UI.createView({
            height: "40dp",
            width: Ti.UI.FILL
        });
        var label = Ti.UI.createLabel({
            text: title,
            color: "#fff",
            height: "40dp",
            left: "30dp",
            textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
            font: {
                fontSize: "15dp",
                fontWeight: "bold"
            }
        });
        var ic = Ti.UI.createImageView({
            image: "/ic_arror_right.png",
            width: "7dp",
            height: Ti.UI.SIZE,
            right: "20dp"
        });
        clickEvent && view.addEventListener("click", function(e) {
            clickEvent(e);
        });
        if (addBorder) {
            var border = Ti.UI.createView({
                height: "1dp",
                backgroundColor: "#2E2E2E"
            });
            var border2 = Ti.UI.createView({
                height: "1dp",
                backgroundColor: "#6C6D6D"
            });
            $.InfoBox.add(border);
            $.InfoBox.add(border2);
        }
        view.add(label);
        view.add(ic);
        $.InfoBox.add(view);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "storeIndex";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    exports.destroy = function() {};
    _.extend($, $.__views);
    try {
        InitInfoBox();
    } catch (ex) {
        alert(ex.message);
    }
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;