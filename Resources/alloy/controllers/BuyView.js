function Controller() {
    function Close() {
        $.win.close();
    }
    function BeforeLoad(e) {
        Ti.API.info("BeforeLoad" + e.url);
    }
    function Load(e) {
        Ti.API.info("load" + e.url);
        if (-1 != e.url.indexOf("call_back_url.aspx")) loadView.Show("订单处理中，请稍等"); else if (-1 != e.url.indexOf("payfinish.html")) {
            loadView.Hide();
            closeFun();
            Close();
            if (-1 != e.url.indexOf("result=ok")) {
                alert("交易成功");
                UI.Open("Order", {
                    State: "7"
                });
            } else {
                alert("交易过程出现未知错误，请到订单中完成支付");
                UI.Open("Order", {
                    State: "1"
                });
            }
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "BuyView";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId17 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId17"
    });
    $.__views.TitleBar.add($.__views.__alloyId17);
    Close ? $.__views.__alloyId17.addEventListener("click", Close) : __defers["$.__views.__alloyId17!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId17.add($.__views.btnBack);
    $.__views.__alloyId18 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "帐单在线支付",
        id: "__alloyId18"
    });
    $.__views.TitleBar.add($.__views.__alloyId18);
    $.__views.WebView = Ti.UI.createWebView({
        id: "WebView",
        top: "40dp"
    });
    $.__views.win.add($.__views.WebView);
    Load ? $.__views.WebView.addEventListener("load", Load) : __defers["$.__views.WebView!load!Load"] = true;
    BeforeLoad ? $.__views.WebView.addEventListener("beforeload", BeforeLoad) : __defers["$.__views.WebView!beforeload!BeforeLoad"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Dialog = require("Dialog");
    var args = arguments[0] || {};
    var sum = args.sum;
    var url = Alloy.Globals.WebUrl + "/Mobile/alipay/MobilePay.aspx?action=add&idguid=" + Alloy.Globals.LoginToken + "&sum=" + sum;
    var closeFun = args.close;
    $.WebView.url = url;
    Ti.API.debug(url);
    var loadView = new Dialog.load();
    __defers["$.__views.__alloyId17!click!Close"] && $.__views.__alloyId17.addEventListener("click", Close);
    __defers["$.__views.WebView!load!Load"] && $.__views.WebView.addEventListener("load", Load);
    __defers["$.__views.WebView!beforeload!BeforeLoad"] && $.__views.WebView.addEventListener("beforeload", BeforeLoad);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;