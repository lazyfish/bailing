function Controller() {
    function Close() {
        $.win.close();
    }
    function GotoCar() {
        UI.Open("BuyCar");
    }
    function SetProductContent(content) {
        $.Introduce.html = Utility.GetWebView(content);
    }
    function BindPics() {
        var url = Alloy.Globals.ApiUrl + "/product.ashx?action=getpropic&id=" + id;
        Ajax.json(url, function(item) {
            var view = UI.C("view", {
                left: "10dp",
                width: Ti.UI.SIZE,
                height: Ti.UI.SIZE
            });
            var pic = UI.C("image", {
                image: Alloy.Globals.WebUrl + item.PictureUrl,
                height: "150dp",
                width: Ti.UI.SIZE
            });
            view.add(pic);
            $.Faces.add(view);
        });
    }
    function Fav() {
        if (!haveFav) {
            var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=add&idguid=" + Alloy.Globals.LoginToken + "&conid=" + id + "&storeid=" + StoreId + "&contype=1";
            Ajax.get(url, function(data) {
                if ("ok" == data) {
                    var prompt = new Dialog.prompt();
                    prompt.Show("收藏成功！", 2e3);
                    $.btnUnFav.setImage("/star_on.png");
                }
            });
        }
    }
    function Buy() {
        var url = Alloy.Globals.ApiUrl + "/car.ashx?action=add";
        var param = {
            idguid: Alloy.Globals.LoginToken,
            storeid: StoreId,
            vendorId: vendorId,
            pid: id,
            Discount: 1,
            SelNum: 1
        };
        Ajax.post(url, param, function(data) {
            if ("ok" == data) {
                var prompt = new Dialog.prompt();
                $.btnCarNumber.text = parseInt($.btnCarNumber.text) + 1;
                prompt.Show("已放入购物车！", 2e3);
            }
        });
    }
    function BindComment() {
        var url = Alloy.Globals.ApiUrl + "/comment.ashx?action=list&proid=" + id + "&storeid=" + StoreId;
        Ajax.json(url, function(item) {
            return Alloy.createController("commentRow", {
                title: item.username,
                content: item.cm_content,
                date: item.addtime
            }).getView();
        }, function(data) {
            $.CommendTable.setData(data);
        });
    }
    function ShowIntroduce() {
        $.scrolView.setCurrentPage(0);
        $.border2.setBackgroundColor("#bbb");
        $.border1.setBackgroundColor("#B13531");
    }
    function ShowCommend() {
        $.scrolView.setCurrentPage(1);
        $.border2.setBackgroundColor("#B13531");
        $.border1.setBackgroundColor("#bbb");
        BindComment();
    }
    function ChangePage(e) {
        if (0 == e.currentPage) {
            $.border2.setBackgroundColor("#bbb");
            $.border1.setBackgroundColor("#B13531");
        } else if (1 == e.currentPage) {
            $.border2.setBackgroundColor("#B13531");
            $.border1.setBackgroundColor("#bbb");
        }
    }
    function Reward() {
        UI.Open("Dial", {
            id: id,
            sum: price,
            storeId: StoreId,
            vendorId: vendorId
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "activityInfo";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId193 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId193"
    });
    $.__views.TitleBar.add($.__views.__alloyId193);
    Close ? $.__views.__alloyId193.addEventListener("click", Close) : __defers["$.__views.__alloyId193!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId193.add($.__views.btnBack);
    $.__views.__alloyId194 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "活动介绍",
        id: "__alloyId194"
    });
    $.__views.TitleBar.add($.__views.__alloyId194);
    $.__views.__alloyId195 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        right: "60dp",
        id: "__alloyId195"
    });
    $.__views.TitleBar.add($.__views.__alloyId195);
    Fav ? $.__views.__alloyId195.addEventListener("click", Fav) : __defers["$.__views.__alloyId195!click!Fav"] = true;
    $.__views.btnUnFav = Ti.UI.createImageView({
        image: "/star_off.png",
        width: "35dp",
        height: Ti.UI.SIZE,
        id: "btnUnFav"
    });
    $.__views.__alloyId195.add($.__views.btnUnFav);
    $.__views.__alloyId196 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        right: "0dp",
        id: "__alloyId196"
    });
    $.__views.TitleBar.add($.__views.__alloyId196);
    GotoCar ? $.__views.__alloyId196.addEventListener("click", GotoCar) : __defers["$.__views.__alloyId196!click!GotoCar"] = true;
    $.__views.btnCar = Ti.UI.createView({
        right: "5dp",
        height: "30dp",
        width: Ti.UI.SIZE,
        backgroundColor: "#BA3D38",
        borderRadius: "5dp",
        layout: "horizontal",
        id: "btnCar"
    });
    $.__views.__alloyId196.add($.__views.btnCar);
    $.__views.btnCarIcon = Ti.UI.createImageView({
        image: "/ic_car.png",
        left: "5dp",
        width: "25dp",
        height: Ti.UI.SIZE,
        id: "btnCarIcon"
    });
    $.__views.btnCar.add($.__views.btnCarIcon);
    $.__views.__alloyId197 = Ti.UI.createView({
        width: "4dp",
        id: "__alloyId197"
    });
    $.__views.btnCar.add($.__views.__alloyId197);
    $.__views.btnCarNumber = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "12dp"
        },
        right: "10dp",
        text: "0",
        id: "btnCarNumber"
    });
    $.__views.btnCar.add($.__views.btnCarNumber);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.Faces = Ti.UI.createScrollView({
        layout: "horizontal",
        height: "150dp",
        horizontalWrap: false,
        scrollType: "horizontal",
        showHorizontalScrollIndicator: true,
        id: "Faces",
        top: "0"
    });
    $.__views.MainWin.add($.__views.Faces);
    $.__views.__alloyId198 = Ti.UI.createView({
        layout: "horizontal",
        top: "150dp",
        height: "30dp",
        id: "__alloyId198"
    });
    $.__views.MainWin.add($.__views.__alloyId198);
    $.__views.__alloyId199 = Ti.UI.createView({
        layout: "vertical",
        left: "1dp",
        height: "30dp",
        width: "49%",
        id: "__alloyId199"
    });
    $.__views.__alloyId198.add($.__views.__alloyId199);
    ShowIntroduce ? $.__views.__alloyId199.addEventListener("click", ShowIntroduce) : __defers["$.__views.__alloyId199!click!ShowIntroduce"] = true;
    $.__views.__alloyId200 = Ti.UI.createLabel({
        width: "100%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        color: "#333",
        top: "2dp",
        height: "18dp",
        font: {
            fontSize: "13dp"
        },
        text: "活动介绍",
        id: "__alloyId200"
    });
    $.__views.__alloyId199.add($.__views.__alloyId200);
    $.__views.border1 = Ti.UI.createView({
        top: "7dp",
        width: "90%",
        height: "3dp",
        backgroundColor: "#B13531",
        id: "border1"
    });
    $.__views.__alloyId199.add($.__views.border1);
    $.__views.__alloyId201 = Ti.UI.createView({
        layout: "vertical",
        left: "1dp",
        height: "30dp",
        width: "49%",
        id: "__alloyId201"
    });
    $.__views.__alloyId198.add($.__views.__alloyId201);
    ShowCommend ? $.__views.__alloyId201.addEventListener("click", ShowCommend) : __defers["$.__views.__alloyId201!click!ShowCommend"] = true;
    $.__views.__alloyId202 = Ti.UI.createLabel({
        width: "100%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        color: "#333",
        top: "2dp",
        height: "18dp",
        font: {
            fontSize: "13dp"
        },
        text: "活动评论",
        id: "__alloyId202"
    });
    $.__views.__alloyId201.add($.__views.__alloyId202);
    $.__views.border2 = Ti.UI.createView({
        top: "7dp",
        width: "90%",
        height: "3dp",
        backgroundColor: "#bbb",
        id: "border2"
    });
    $.__views.__alloyId201.add($.__views.border2);
    var __alloyId203 = [];
    $.__views.__alloyId204 = Ti.UI.createView({
        layout: "vertical",
        id: "__alloyId204"
    });
    __alloyId203.push($.__views.__alloyId204);
    $.__views.Title = Ti.UI.createLabel({
        font: {
            fontSize: 15
        },
        top: 5,
        id: "Title",
        left: "10"
    });
    $.__views.__alloyId204.add($.__views.Title);
    $.__views.Price = Ti.UI.createLabel({
        font: {
            fontSize: 13
        },
        top: 5,
        id: "Price",
        left: "10"
    });
    $.__views.__alloyId204.add($.__views.Price);
    $.__views.Introduce = Ti.UI.createWebView({
        color: "#333",
        font: {
            fontSize: "14dp"
        },
        disableBounce: "true",
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "Introduce"
    });
    $.__views.__alloyId204.add($.__views.Introduce);
    $.__views.CommendTable = Ti.UI.createTableView({
        backgroundColor: "#fff",
        separatorStyle: 0,
        id: "CommendTable"
    });
    __alloyId203.push($.__views.CommendTable);
    $.__views.scrolView = Ti.UI.createScrollableView({
        views: __alloyId203,
        id: "scrolView",
        height: Ti.UI.SIZE,
        top: "180dp",
        bottom: "50dp"
    });
    $.__views.MainWin.add($.__views.scrolView);
    ChangePage ? $.__views.scrolView.addEventListener("scrollend", ChangePage) : __defers["$.__views.scrolView!scrollend!ChangePage"] = true;
    $.__views.__alloyId205 = Ti.UI.createView({
        height: "40dp",
        bottom: "10",
        id: "__alloyId205"
    });
    $.__views.MainWin.add($.__views.__alloyId205);
    $.__views.__alloyId206 = Ti.UI.createView({
        height: "35dp",
        width: "40%",
        left: "10",
        id: "__alloyId206"
    });
    $.__views.__alloyId205.add($.__views.__alloyId206);
    Buy ? $.__views.__alloyId206.addEventListener("click", Buy) : __defers["$.__views.__alloyId206!click!Buy"] = true;
    $.__views.__alloyId207 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId207"
    });
    $.__views.__alloyId206.add($.__views.__alloyId207);
    $.__views.__alloyId208 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId208"
    });
    $.__views.__alloyId206.add($.__views.__alloyId208);
    $.__views.btnText = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "加入购物车",
        id: "btnText"
    });
    $.__views.__alloyId208.add($.__views.btnText);
    $.__views.__alloyId209 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId209"
    });
    $.__views.__alloyId206.add($.__views.__alloyId209);
    $.__views.__alloyId210 = Ti.UI.createView({
        height: "35dp",
        width: "40%",
        right: "10",
        id: "__alloyId210"
    });
    $.__views.__alloyId205.add($.__views.__alloyId210);
    Reward ? $.__views.__alloyId210.addEventListener("click", Reward) : __defers["$.__views.__alloyId210!click!Reward"] = true;
    $.__views.__alloyId211 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId211"
    });
    $.__views.__alloyId210.add($.__views.__alloyId211);
    $.__views.__alloyId212 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId212"
    });
    $.__views.__alloyId210.add($.__views.__alloyId212);
    $.__views.btnText = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "摇奖",
        id: "btnText"
    });
    $.__views.__alloyId212.add($.__views.btnText);
    $.__views.__alloyId213 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId213"
    });
    $.__views.__alloyId210.add($.__views.__alloyId213);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Ajax = require("Ajax");
    var Dialog = require("Dialog");
    var Utility = require("Utility");
    var args = arguments[0] || {};
    var id = args.id || "";
    args.face || "";
    args.title || "";
    var StoreId = args.Storeid || "";
    var vendorId = args.vendorId || "";
    args.content || "";
    args.discount || "";
    var price = args.price || "";
    BindPics();
    Ajax.json(Alloy.Globals.ApiUrl + "/product.ashx?action=info&id=" + id, function(item) {
        $.Title.setText(item.ProName);
        $.Price.setText(String.format("参与价：%.2f元", parseFloat(item.Price)));
        SetProductContent(item.ProContent);
    });
    Alloy.Globals.LoginToken && Ajax.get(Alloy.Globals.ApiUrl + "/car.ashx?action=count&idguid=" + Alloy.Globals.LoginToken, function(data) {
        $.btnCarNumber.text = data;
    });
    var haveFav = false;
    __defers["$.__views.__alloyId193!click!Close"] && $.__views.__alloyId193.addEventListener("click", Close);
    __defers["$.__views.__alloyId195!click!Fav"] && $.__views.__alloyId195.addEventListener("click", Fav);
    __defers["$.__views.__alloyId196!click!GotoCar"] && $.__views.__alloyId196.addEventListener("click", GotoCar);
    __defers["$.__views.__alloyId199!click!ShowIntroduce"] && $.__views.__alloyId199.addEventListener("click", ShowIntroduce);
    __defers["$.__views.__alloyId201!click!ShowCommend"] && $.__views.__alloyId201.addEventListener("click", ShowCommend);
    __defers["$.__views.scrolView!scrollend!ChangePage"] && $.__views.scrolView.addEventListener("scrollend", ChangePage);
    __defers["$.__views.__alloyId206!click!Buy"] && $.__views.__alloyId206.addEventListener("click", Buy);
    __defers["$.__views.__alloyId210!click!Reward"] && $.__views.__alloyId210.addEventListener("click", Reward);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;