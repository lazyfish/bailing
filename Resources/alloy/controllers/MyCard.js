function Controller() {
    function Close() {
        $.win.close();
    }
    function LoadItem(item) {
        var dataItem = UI.Controller("CardRow", {
            data: item
        });
        dataList.push(dataItem);
        return dataItem;
    }
    function LoadFinish(data, total) {
        $.table.setData(dataList);
        parseInt(total) > dataList.length && $.table.appendRow(moreView);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "MyCard";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId100 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId100"
    });
    $.__views.TitleBar.add($.__views.__alloyId100);
    Close ? $.__views.__alloyId100.addEventListener("click", Close) : __defers["$.__views.__alloyId100!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId100.add($.__views.btnBack);
    $.__views.__alloyId101 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "我的卡包",
        id: "__alloyId101"
    });
    $.__views.TitleBar.add($.__views.__alloyId101);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox"
    });
    $.__views.MainWin.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Ajax = require("Ajax");
    var url = "";
    var moreView = null;
    var dataList = new Array();
    var url = Alloy.Globals.ApiUrl + "/user.ashx?action=card&idguid=" + Alloy.Globals.LoginToken;
    moreView || (moreView = UI.Controller("MoreRow", {
        url: url,
        index: 0,
        itemFun: LoadItem,
        finishFun: LoadFinish
    }));
    Ti.API.debug(url);
    Ajax.json(url, LoadItem, LoadFinish);
    __defers["$.__views.__alloyId100!click!Close"] && $.__views.__alloyId100.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;