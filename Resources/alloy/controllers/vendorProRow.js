function Controller() {
    function GotoDetail() {
        Alloy.createController("product", {
            id: id,
            face: pics,
            title: title,
            Storeid: Storeid,
            vendorId: vendorId,
            discount: discount,
            price: price,
            content: content
        }).getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vendorProRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.vendorProRow = Ti.UI.createTableViewRow({
        id: "vendorProRow"
    });
    $.__views.vendorProRow && $.addTopLevelView($.__views.vendorProRow);
    GotoDetail ? $.__views.vendorProRow.addEventListener("click", GotoDetail) : __defers["$.__views.vendorProRow!click!GotoDetail"] = true;
    $.__views.__alloyId363 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId363"
    });
    $.__views.vendorProRow.add($.__views.__alloyId363);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId363.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId364 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        height: Ti.UI.SIZE,
        id: "__alloyId364"
    });
    $.__views.row.add($.__views.__alloyId364);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId364.add($.__views.title);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "13dp"
        },
        id: "content"
    });
    $.__views.__alloyId364.add($.__views.content);
    $.__views.discountPrice = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "13dp"
        },
        id: "discountPrice"
    });
    $.__views.__alloyId364.add($.__views.discountPrice);
    $.__views.__alloyId365 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId365"
    });
    $.__views.__alloyId363.add($.__views.__alloyId365);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var id = args.id || "";
    var face = args.face || "";
    var title = args.title || "";
    var content = args.content || "";
    var price = args.price || "";
    var Storeid = args.Storeid || "";
    var pics = args.pics || "";
    var vendorId = args.vendorId || "";
    var discount = args.discount || "";
    var vipName = args.vipName || "";
    $.face.setImage(Alloy.Globals.WebUrl + face);
    $.title.setText(title);
    $.content.setText("原价：" + parseFloat(price).toFixed(2) + "元");
    vipName && $.discountPrice.setText(vipName + "：" + parseFloat(price).toFixed(2) + "元");
    __defers["$.__views.vendorProRow!click!GotoDetail"] && $.__views.vendorProRow.addEventListener("click", GotoDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;