function Controller() {
    function Close() {
        $.win.close();
    }
    function Login() {
        UI.Open("Login");
        $.win.close();
    }
    function Submit() {
        if (5 > $.txtUserName.value.length && $.txtUserName.value.length > 15) {
            alert("用户名应多于5个字且少于15个字");
            return false;
        }
        if ($.txtPwd.value != $.txtPwd2.value) {
            alert("两次输入的密码不匹配，请重新输入");
            return false;
        }
        var param = {
            UserName: $.txtUserName.value,
            Password: $.txtPwd.value,
            Tel: $.txtTel.value
        };
        Ajax.post(Alloy.Globals.ApiUrl + "/user.ashx?action=reg", param, function(data) {
            try {
                if ("exist user" == data) alert("此用户已存在，请换个用户名!"); else {
                    var users = Alloy.createCollection("UserModel");
                    var model;
                    users.fetch();
                    if (users.length > 0) {
                        model = users.at(0);
                        model.set({
                            Token: data
                        });
                    } else {
                        model = Alloy.createModel("UserModel", {
                            Token: data
                        });
                        users.add(model);
                    }
                    model.save();
                    Alloy.Globals.LoginToken = data;
                    var prompt = new Dialog.prompt();
                    prompt.Show("注册成功！", 2e3);
                    setTimeout(function() {
                        $.win.close();
                    }, 2e3);
                }
            } catch (e) {
                alert(e.message);
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Register";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId116 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId116"
    });
    $.__views.TitleBar.add($.__views.__alloyId116);
    Close ? $.__views.__alloyId116.addEventListener("click", Close) : __defers["$.__views.__alloyId116!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId116.add($.__views.btnBack);
    $.__views.__alloyId117 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "用户注册",
        id: "__alloyId117"
    });
    $.__views.TitleBar.add($.__views.__alloyId117);
    $.__views.__alloyId118 = Ti.UI.createView({
        height: "100%",
        width: "70dp",
        right: "0dp",
        id: "__alloyId118"
    });
    $.__views.TitleBar.add($.__views.__alloyId118);
    $.__views.__alloyId119 = Ti.UI.createView({
        height: "35dp",
        top: "2dp",
        id: "__alloyId119"
    });
    $.__views.__alloyId118.add($.__views.__alloyId119);
    Login ? $.__views.__alloyId119.addEventListener("click", Login) : __defers["$.__views.__alloyId119!click!Login"] = true;
    $.__views.__alloyId120 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId120"
    });
    $.__views.__alloyId119.add($.__views.__alloyId120);
    $.__views.__alloyId121 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId121"
    });
    $.__views.__alloyId119.add($.__views.__alloyId121);
    $.__views.__alloyId122 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "登录",
        id: "__alloyId122"
    });
    $.__views.__alloyId121.add($.__views.__alloyId122);
    $.__views.__alloyId123 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId123"
    });
    $.__views.__alloyId119.add($.__views.__alloyId123);
    $.__views.__alloyId124 = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#414141",
        top: "40dp",
        id: "__alloyId124"
    });
    $.__views.win.add($.__views.__alloyId124);
    $.__views.__alloyId125 = Ti.UI.createView({
        borderWidth: "1",
        borderColor: "#2F2F2F",
        backgroundColor: "#5E5F5F",
        borderRadius: "20",
        height: Ti.UI.SIZE,
        layout: "vertical",
        width: "90%",
        top: "20dp",
        id: "__alloyId125"
    });
    $.__views.__alloyId124.add($.__views.__alloyId125);
    $.__views.__alloyId126 = Ti.UI.createView({
        top: "5dp",
        left: "20dp",
        right: "20dp",
        height: "40dp",
        id: "__alloyId126"
    });
    $.__views.__alloyId125.add($.__views.__alloyId126);
    $.__views.__alloyId127 = Ti.UI.createLabel({
        left: "0dp",
        width: "85dp",
        font: {
            fontSize: "16dp"
        },
        color: "#fff",
        text: "账号：",
        id: "__alloyId127"
    });
    $.__views.__alloyId126.add($.__views.__alloyId127);
    $.__views.txtUserName = Ti.UI.createTextField({
        font: {
            fontSize: "16dp"
        },
        left: "85dp",
        right: "0",
        color: "#fff",
        backgroundColor: "#5E5F5F",
        borderColor: "#5E5F5F",
        id: "txtUserName",
        hintText: "在此输入5-15位的用户名",
        value: "dsc"
    });
    $.__views.__alloyId126.add($.__views.txtUserName);
    $.__views.__alloyId128 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#434343",
        id: "__alloyId128"
    });
    $.__views.__alloyId125.add($.__views.__alloyId128);
    $.__views.__alloyId129 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#2D2D2D",
        id: "__alloyId129"
    });
    $.__views.__alloyId125.add($.__views.__alloyId129);
    $.__views.__alloyId130 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#6C6D6D",
        id: "__alloyId130"
    });
    $.__views.__alloyId125.add($.__views.__alloyId130);
    $.__views.__alloyId131 = Ti.UI.createView({
        top: "5dp",
        left: "20dp",
        right: "20dp",
        height: "40dp",
        id: "__alloyId131"
    });
    $.__views.__alloyId125.add($.__views.__alloyId131);
    $.__views.__alloyId132 = Ti.UI.createLabel({
        left: "0dp",
        width: "85dp",
        font: {
            fontSize: "16dp"
        },
        color: "#fff",
        text: "密码：",
        id: "__alloyId132"
    });
    $.__views.__alloyId131.add($.__views.__alloyId132);
    $.__views.txtPwd = Ti.UI.createTextField({
        font: {
            fontSize: "16dp"
        },
        left: "85dp",
        right: "0",
        color: "#fff",
        backgroundColor: "#5E5F5F",
        borderColor: "#5E5F5F",
        id: "txtPwd",
        passwordMask: "true",
        value: "123"
    });
    $.__views.__alloyId131.add($.__views.txtPwd);
    $.__views.__alloyId133 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#434343",
        id: "__alloyId133"
    });
    $.__views.__alloyId125.add($.__views.__alloyId133);
    $.__views.__alloyId134 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#2D2D2D",
        id: "__alloyId134"
    });
    $.__views.__alloyId125.add($.__views.__alloyId134);
    $.__views.__alloyId135 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#6C6D6D",
        id: "__alloyId135"
    });
    $.__views.__alloyId125.add($.__views.__alloyId135);
    $.__views.__alloyId136 = Ti.UI.createView({
        top: "5dp",
        left: "20dp",
        right: "20dp",
        height: "40dp",
        id: "__alloyId136"
    });
    $.__views.__alloyId125.add($.__views.__alloyId136);
    $.__views.__alloyId137 = Ti.UI.createLabel({
        left: "0dp",
        width: "85dp",
        font: {
            fontSize: "16dp"
        },
        color: "#fff",
        text: "密码确认：",
        id: "__alloyId137"
    });
    $.__views.__alloyId136.add($.__views.__alloyId137);
    $.__views.txtPwd2 = Ti.UI.createTextField({
        font: {
            fontSize: "16dp"
        },
        left: "85dp",
        right: "0",
        color: "#fff",
        backgroundColor: "#5E5F5F",
        borderColor: "#5E5F5F",
        id: "txtPwd2",
        passwordMask: "true",
        value: "123"
    });
    $.__views.__alloyId136.add($.__views.txtPwd2);
    $.__views.__alloyId138 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#434343",
        id: "__alloyId138"
    });
    $.__views.__alloyId125.add($.__views.__alloyId138);
    $.__views.__alloyId139 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#2D2D2D",
        id: "__alloyId139"
    });
    $.__views.__alloyId125.add($.__views.__alloyId139);
    $.__views.__alloyId140 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#6C6D6D",
        id: "__alloyId140"
    });
    $.__views.__alloyId125.add($.__views.__alloyId140);
    $.__views.__alloyId141 = Ti.UI.createView({
        top: "5dp",
        left: "20dp",
        right: "20dp",
        height: "40dp",
        id: "__alloyId141"
    });
    $.__views.__alloyId125.add($.__views.__alloyId141);
    $.__views.__alloyId142 = Ti.UI.createLabel({
        left: "0dp",
        width: "85dp",
        font: {
            fontSize: "16dp"
        },
        color: "#fff",
        text: "手机号：",
        id: "__alloyId142"
    });
    $.__views.__alloyId141.add($.__views.__alloyId142);
    $.__views.txtTel = Ti.UI.createTextField({
        font: {
            fontSize: "16dp"
        },
        left: "85dp",
        right: "0",
        color: "#fff",
        backgroundColor: "#5E5F5F",
        borderColor: "#5E5F5F",
        id: "txtTel",
        hintText: "选填项，可用于登录"
    });
    $.__views.__alloyId141.add($.__views.txtTel);
    $.__views.__alloyId143 = Ti.UI.createView({
        height: "40dp",
        left: "20dp",
        top: "20dp",
        right: "20dp",
        id: "__alloyId143"
    });
    $.__views.__alloyId124.add($.__views.__alloyId143);
    Submit ? $.__views.__alloyId143.addEventListener("click", Submit) : __defers["$.__views.__alloyId143!click!Submit"] = true;
    $.__views.__alloyId144 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId144"
    });
    $.__views.__alloyId143.add($.__views.__alloyId144);
    $.__views.__alloyId145 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId145"
    });
    $.__views.__alloyId143.add($.__views.__alloyId145);
    $.__views.__alloyId146 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "提交信息",
        id: "__alloyId146"
    });
    $.__views.__alloyId145.add($.__views.__alloyId146);
    $.__views.__alloyId147 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId147"
    });
    $.__views.__alloyId143.add($.__views.__alloyId147);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var Dialog = require("Dialog");
    __defers["$.__views.__alloyId116!click!Close"] && $.__views.__alloyId116.addEventListener("click", Close);
    __defers["$.__views.__alloyId119!click!Login"] && $.__views.__alloyId119.addEventListener("click", Login);
    __defers["$.__views.__alloyId143!click!Submit"] && $.__views.__alloyId143.addEventListener("click", Submit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;