function Controller() {
    function Init() {
        windowGround = Ti.UI.createView({
            width: Ti.UI.FILL,
            height: Ti.UI.FILL,
            left: "0",
            top: "0",
            right: "0",
            bottom: "0",
            opacity: 0,
            zIndex: 10,
            visible: false,
            backgroundColor: "#000"
        });
        windowGround.addEventListener("click", function() {
            windowGround.animate({
                opacity: 0,
                visible: false,
                duration: 500
            });
            typeWin.animate({
                opacity: 0,
                visible: false,
                duration: 500
            });
            childTypeWin.animate({
                opacity: 0,
                visible: false,
                duration: 500
            });
        });
        $.win.add(windowGround);
        BindTable();
    }
    function BindTable() {
        var data = [];
        for (var i = 0; 10 > i; i++) data.push(Alloy.createController("vendorProRow", {
            face: "/images/ad1.jpg",
            title: "Row" + i,
            content: "Content" + i
        }).getView());
        $.table.setData(data);
    }
    function GotoDetail() {
        Alloy.createController("product").getView().open();
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vendorProduct";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#404141",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId366 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId366"
    });
    $.__views.TitleBar.add($.__views.__alloyId366);
    Close ? $.__views.__alloyId366.addEventListener("click", Close) : __defers["$.__views.__alloyId366!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId366.add($.__views.btnBack);
    $.__views.__alloyId367 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "产品展示",
        id: "__alloyId367"
    });
    $.__views.TitleBar.add($.__views.__alloyId367);
    $.__views.MainWin = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#fff",
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.Toolbar = Ti.UI.createView({
        height: "45dp",
        backgroundImage: "/tag_bg.png",
        width: Ti.UI.FILL,
        id: "Toolbar"
    });
    $.__views.MainWin.add($.__views.Toolbar);
    $.__views.__alloyId368 = Ti.UI.createView({
        top: "5dp",
        height: "35dp",
        backgroundImage: "/store_type_bg_a.png",
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId368"
    });
    $.__views.Toolbar.add($.__views.__alloyId368);
    $.__views.__alloyId369 = Ti.UI.createView({
        id: "__alloyId369"
    });
    $.__views.__alloyId368.add($.__views.__alloyId369);
    $.__views.__alloyId370 = Ti.UI.createLabel({
        text: "类别",
        id: "__alloyId370"
    });
    $.__views.__alloyId368.add($.__views.__alloyId370);
    $.__views.__alloyId371 = Ti.UI.createLabel({
        width: 100,
        font: {
            fontSize: "14dp"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#84868D",
        text: "排序",
        id: "__alloyId371"
    });
    $.__views.__alloyId368.add($.__views.__alloyId371);
    $.__views.__alloyId372 = Ti.UI.createView({
        width: 100,
        font: {
            fontSize: 14
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#84868D",
        id: "__alloyId372"
    });
    $.__views.__alloyId368.add($.__views.__alloyId372);
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox"
    });
    $.__views.MainWin.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    GotoDetail ? $.__views.table.addEventListener("click", GotoDetail) : __defers["$.__views.table!click!GotoDetail"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    var windowGround;
    Init();
    var typeWin = Ti.UI.createView({
        backgroundColor: "#f3f3f3",
        width: "150dp",
        left: "5dp",
        height: "220dp",
        top: "70dp",
        keepScreenOn: true,
        opacity: 0,
        visible: false,
        zIndex: 20
    });
    var childTypeWin = Ti.UI.createView({
        backgroundColor: "#ccc",
        width: "150dp",
        left: "155dp",
        height: "220dp",
        top: "70dp",
        opacity: 0,
        visible: false,
        zIndex: 20
    });
    $.win.add(typeWin);
    $.win.add(childTypeWin);
    var data = [], childData = [];
    for (var i = 0; 7 > i; i++) {
        data.push(Ti.UI.createTableViewRow({
            title: "Type" + i,
            height: "30dp",
            color: "#333",
            font: {
                fontSize: "14dp"
            }
        }));
        childData.push(Ti.UI.createTableViewRow({
            title: "ChildType" + i,
            height: "30dp",
            color: "#333",
            font: {
                fontSize: "14dp"
            }
        }));
        0 == i && data[i].setBackgroundColor("#ccc");
    }
    var lastTypeIndex = 0;
    var listView = Ti.UI.createTableView({
        separatorColor: "#bbb",
        backgroundColor: "#f3f3f3",
        data: data
    });
    listView.addEventListener("click", function(e) {
        var lastRow = e.section.getRows()[lastTypeIndex];
        lastRow.setBackgroundColor("#f3f3f3");
        var current = e.row;
        current.setBackgroundColor("#ccc");
        listView.updateRow(lastTypeIndex, lastRow);
        listView.updateRow(e.index, current);
        lastTypeIndex = e.index;
    });
    var listView2 = Ti.UI.createTableView({
        separatorColor: "#ccc",
        backgroundColor: "#ccc",
        data: childData
    });
    typeWin.add(listView);
    childTypeWin.add(listView2);
    __defers["$.__views.__alloyId366!click!Close"] && $.__views.__alloyId366.addEventListener("click", Close);
    __defers["$.__views.table!click!GotoDetail"] && $.__views.table.addEventListener("click", GotoDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;