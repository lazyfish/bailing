function Controller() {
    function LoadType() {
        Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetTypeByUser&idguid=" + Alloy.Globals.LoginToken, TypeItemFun, TypeLoaded);
    }
    function TypeItemFun(item, i) {
        0 == i && $.TypeGrid.removeAllChildren();
        try {
            $.TypeGrid.add(InitTypeItme(item.id, item.ItemName, Alloy.Globals.WebUrl + item.PicUrl, item.IsShow));
        } catch (e) {
            Ti.API.debug("Error:" + JSON.stringify(e));
        }
    }
    function TypeLoaded() {
        $.TypeGrid.add(InitTypeItme("0", "添加", "/ic_add.png"));
    }
    function InitTypeItme(id, name, face, show) {
        var viewWidth = Alloy.Globals.width / 3 - 20;
        var viewStyle = {
            width: viewWidth,
            height: viewWidth,
            backgroundImage: "/bg1.png",
            left: "10",
            top: "10",
            right: "10"
        };
        var imgStyle = {
            width: "30dp",
            height: "30dp"
        };
        var titleStyle = {
            top: "10dp",
            color: "#fff",
            textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
            font: {
                fontSize: "14dp"
            }
        };
        var viewBox = UI.C("view", viewStyle);
        var viewCenter = UI.C("view", {
            layout: "vertical",
            width: Ti.UI.SIZE,
            height: Ti.UI.SIZE
        });
        var icon = UI.C("image", imgStyle);
        face && (icon.image = face);
        var title = UI.C("label", titleStyle);
        title.text = name;
        viewCenter.add(icon);
        viewCenter.add(title);
        viewBox.add(viewCenter);
        viewBox.tid = id;
        viewBox.Bind("click", function() {
            "0" == this.tid ? UI.Open("Type", {
                callFun: LoadType
            }) : UI.Open("vendor", {
                type: this.tid
            });
        });
        "False" == show && viewBox.Bind("longpress", function() {
            var eventObj = this;
            var alertSure = new Dialog.alertSure();
            alertSure.Show("确认要删除此类别？", function() {
                var url = Alloy.Globals.ApiUrl + "/allclass.ashx?action=DelUserType&idguid=" + Alloy.Globals.LoginToken + "&TypeId=" + eventObj.tid;
                Ajax.get(url, function() {});
                $.TypeGrid.remove(eventObj);
            });
        });
        return viewBox;
    }
    function LoadAd() {
        Ajax.json(Alloy.Globals.ApiUrl + "/ad.ashx?action=list", AdItemFun, function() {
            for (var i in titles) titles[i].Bind("click", function() {
                $.AdImages.scrollToView(parseInt(this.index));
                for (var j in AdTitleList) AdTitleList[j].setBackgroundColor("#666");
                AdTitleList[this.index].setBackgroundColor("#C13C46");
            });
        });
    }
    function AdItemFun(item, i, length) {
        var titleWidth = Alloy.Globals.width / length;
        var img = UI.C("image", {
            image: Alloy.Globals.WebUrl + item.PicUrl,
            width: "100%",
            height: "100%"
        });
        var titleStyle = {
            width: titleWidth,
            height: "30dp",
            textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
            backgroundColor: "#666",
            backgroundSelectedColor: "#C13C46",
            color: "#fff",
            font: {
                fontSize: "14dp"
            }
        };
        0 == i && (titleStyle.backgroundColor = "#C13C46");
        titleStyle.text = item.PicAlt;
        var title = UI.C("label", titleStyle);
        title.index = i;
        AdTitleList.push(title);
        $.AdImages.addView(img);
        $.AdTitles.add(title);
        titles.push(title);
    }
    function ChangeAdTitle(e) {
        for (var i in AdTitleList) AdTitleList[i].setBackgroundColor("#666");
        AdTitleList[e.currentPage].setBackgroundColor("#C13C46");
    }
    function Like() {
        UI.Open("Like");
    }
    function Shake() {
        UI.Open("Shake");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "main";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createView({
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.AdBox = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "AdBox",
        top: "0dp"
    });
    $.__views.win.add($.__views.AdBox);
    var __alloyId226 = [];
    $.__views.AdImages = Ti.UI.createScrollableView({
        height: "150dp",
        views: __alloyId226,
        id: "AdImages"
    });
    $.__views.AdBox.add($.__views.AdImages);
    ChangeAdTitle ? $.__views.AdImages.addEventListener("scrollend", ChangeAdTitle) : __defers["$.__views.AdImages!scrollend!ChangeAdTitle"] = true;
    $.__views.AdTitles = Ti.UI.createView({
        height: "30dp",
        layout: "horizontal",
        id: "AdTitles"
    });
    $.__views.AdBox.add($.__views.AdTitles);
    $.__views.TypeGridBox = Ti.UI.createScrollView({
        top: "180dp",
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        horizontalWrap: true,
        layout: "vertical",
        scrollType: "vertical",
        showVerticalScrollIndicator: true,
        showHorizontalScrollIndicator: false,
        id: "TypeGridBox"
    });
    $.__views.win.add($.__views.TypeGridBox);
    $.__views.TypeGrid = Ti.UI.createView({
        layout: "horizontal",
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        contentWidth: "100%",
        horizontalWrap: true,
        id: "TypeGrid"
    });
    $.__views.TypeGridBox.add($.__views.TypeGrid);
    $.__views.__alloyId227 = Ti.UI.createView({
        backgroundColor: "#ccc",
        top: 4,
        height: 1,
        width: Ti.UI.FILL,
        id: "__alloyId227"
    });
    $.__views.TypeGridBox.add($.__views.__alloyId227);
    $.__views.__alloyId228 = Ti.UI.createView({
        height: "25",
        width: Ti.UI.FILL,
        id: "__alloyId228"
    });
    $.__views.TypeGridBox.add($.__views.__alloyId228);
    Like ? $.__views.__alloyId228.addEventListener("click", Like) : __defers["$.__views.__alloyId228!click!Like"] = true;
    $.__views.__alloyId229 = Ti.UI.createLabel({
        font: {
            fontSize: 14
        },
        left: 10,
        top: 5,
        color: "#333",
        text: "猜你喜欢",
        id: "__alloyId229"
    });
    $.__views.__alloyId228.add($.__views.__alloyId229);
    $.__views.__alloyId230 = Ti.UI.createImageView({
        image: "/ic_arrow.png",
        height: 15,
        right: 10,
        top: 5,
        width: Ti.UI.SIZE,
        id: "__alloyId230"
    });
    $.__views.__alloyId228.add($.__views.__alloyId230);
    $.__views.__alloyId231 = Ti.UI.createView({
        backgroundColor: "#ccc",
        top: 4,
        height: 1,
        width: Ti.UI.FILL,
        id: "__alloyId231"
    });
    $.__views.TypeGridBox.add($.__views.__alloyId231);
    $.__views.__alloyId232 = Ti.UI.createView({
        height: "25",
        width: Ti.UI.FILL,
        id: "__alloyId232"
    });
    $.__views.TypeGridBox.add($.__views.__alloyId232);
    Shake ? $.__views.__alloyId232.addEventListener("click", Shake) : __defers["$.__views.__alloyId232!click!Shake"] = true;
    $.__views.__alloyId233 = Ti.UI.createLabel({
        font: {
            fontSize: 14
        },
        left: 10,
        top: 5,
        color: "#333",
        text: "摇一摇",
        id: "__alloyId233"
    });
    $.__views.__alloyId232.add($.__views.__alloyId233);
    $.__views.__alloyId234 = Ti.UI.createImageView({
        image: "/ic_arrow.png",
        height: 15,
        right: 10,
        top: 5,
        width: Ti.UI.SIZE,
        id: "__alloyId234"
    });
    $.__views.__alloyId232.add($.__views.__alloyId234);
    $.__views.__alloyId235 = Ti.UI.createView({
        height: "10",
        id: "__alloyId235"
    });
    $.__views.TypeGridBox.add($.__views.__alloyId235);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    args.callback || "";
    new Array();
    var AdTitleList = new Array();
    var titles = new Array();
    var Ajax = require("Ajax");
    var UI = require("UI");
    var Dialog = require("Dialog");
    LoadAd();
    LoadType();
    __defers["$.__views.AdImages!scrollend!ChangeAdTitle"] && $.__views.AdImages.addEventListener("scrollend", ChangeAdTitle);
    __defers["$.__views.__alloyId228!click!Like"] && $.__views.__alloyId228.addEventListener("click", Like);
    __defers["$.__views.__alloyId232!click!Shake"] && $.__views.__alloyId232.addEventListener("click", Shake);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;