function Controller() {
    function Read() {
        var url = Alloy.Globals.ApiUrl + "/store.ashx?action=shake&lng=" + location.longitude + "&lat=" + location.latitude;
        Ajax.json(url, function(item, i) {
            if (0 == i) {
                var face = Alloy.Globals.WebUrl + item.StorePicUrl;
                var title = item.StoreName;
                var id = item.id;
                var content = item.ContactTel;
                var range = item.Range + "米";
                $.face.setImage(face);
                $.title.setText(title);
                $.content.setText(content);
                $.range.setText(range);
                $.content.addEventListener("click", function(e) {
                    Ti.Platform.openURL("tel:" + content);
                    e.cancelBubble = true;
                });
                storeId = id;
            }
        }, function() {});
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Shake";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.__alloyId166 = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#fff",
        id: "__alloyId166"
    });
    $.__views.win.add($.__views.__alloyId166);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.__alloyId166.add($.__views.TitleBar);
    $.__views.__alloyId167 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId167"
    });
    $.__views.TitleBar.add($.__views.__alloyId167);
    Close ? $.__views.__alloyId167.addEventListener("click", Close) : __defers["$.__views.__alloyId167!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId167.add($.__views.btnBack);
    $.__views.__alloyId168 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "摇一摇",
        id: "__alloyId168"
    });
    $.__views.TitleBar.add($.__views.__alloyId168);
    $.__views.__alloyId169 = Ti.UI.createView({
        backgroundImage: "/shake_shop_billboard_bg.png",
        id: "__alloyId169"
    });
    $.__views.__alloyId166.add($.__views.__alloyId169);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "30dp",
        right: "30dp",
        id: "row"
    });
    $.__views.__alloyId169.add($.__views.row);
    $.__views.__alloyId170 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId170"
    });
    $.__views.row.add($.__views.__alloyId170);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "30dp",
        right: "30dp",
        id: "row",
        visible: "false"
    });
    $.__views.__alloyId170.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        image: "http://placehold.it/50x50",
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId171 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        id: "__alloyId171"
    });
    $.__views.row.add($.__views.__alloyId171);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId171.add($.__views.title);
    $.__views.__alloyId172 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId172"
    });
    $.__views.__alloyId171.add($.__views.__alloyId172);
    $.__views.__alloyId173 = Ti.UI.createImageView({
        left: "0dp",
        image: "/ic_phone.png",
        height: "15dp",
        width: Ti.UI.SIZE,
        id: "__alloyId173"
    });
    $.__views.__alloyId172.add($.__views.__alloyId173);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "15dp"
        },
        id: "content",
        left: "15dp"
    });
    $.__views.__alloyId172.add($.__views.content);
    $.__views.__alloyId174 = Ti.UI.createView({
        layout: "horizontal",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        id: "__alloyId174"
    });
    $.__views.__alloyId171.add($.__views.__alloyId174);
    $.__views.__alloyId175 = Ti.UI.createImageView({
        image: "/ic_location3.png",
        width: Ti.UI.SIZE,
        height: "15dp",
        id: "__alloyId175"
    });
    $.__views.__alloyId174.add($.__views.__alloyId175);
    $.__views.range = Ti.UI.createLabel({
        color: "#333",
        font: {
            fontSize: "13dp"
        },
        id: "range"
    });
    $.__views.__alloyId174.add($.__views.range);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var location = null;
    var hasLocation = false;
    var hasShake = false;
    var storeId = 0;
    Ti.Geolocation.purpose = "Get Current Location";
    Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
    if (Ti.Geolocation.locationServicesEnabled) {
        Ti.Geolocation.distanceFilter = 10;
        Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_NETWORK;
    } else alert("请启用您的GPS定位服务");
    Titanium.Geolocation.addEventListener("location", function(e) {
        location = e.coords;
        hasLocation = true;
        hasShake && Read();
    });
    Ti.Geolocation.getCurrentPosition(function(e) {
        if (e.success) {
            location = e.coords;
            hasLocation = true;
            hasShake && Read();
        }
    });
    Ti.Gesture.addEventListener("shake", function() {
        hasShake = true;
        $.row.setVisible(true);
        hasLocation && Read();
    });
    __defers["$.__views.__alloyId167!click!Close"] && $.__views.__alloyId167.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;