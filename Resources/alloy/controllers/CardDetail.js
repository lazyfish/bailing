function Controller() {
    function ReadBailing() {
        var url = Alloy.Globals.ApiUrl + "/user.ashx?action=blcard&idguid=" + Alloy.Globals.LoginToken;
        Ti.API.debug("LoadCard" + url);
        Ajax.json(url, function(item, i) {
            if (i > 0) return false;
            NextExperience || (NextExperience = item.Grade);
            $.Title.setText("我的百领卡");
            $.Name.setText(String.format("会员卡名称：%s", item.Name));
            $.Discount.setText(String.format("享有折扣：%.2f折", parseFloat(item.Discount) / 100));
            $.Remark.html = Utility.GetWebView(item.Remark);
            $.Progress.setWidth(100 * (parseFloat(NextExperience) / parseFloat(item.Grade)) + "%");
            $.CarFace.setImage(Alloy.Globals.WebUrl + item.Face);
            vendorId = item.VendorId;
            vendorName = item.VendorName;
        });
    }
    function BindCardDetail() {
        var url = Alloy.Globals.ApiUrl + "/user.ashx?action=cardinfo&id=" + id + "&idguid=" + Alloy.Globals.LoginToken;
        Ti.API.debug("LoadCard" + url);
        Ajax.json(url, function(item) {
            NextExperience || (NextExperience = item.Experience);
            $.Vendor.setText(String.format("商     家：%s", item.VendorName));
            $.Name.setText(String.format("会员卡名称：%s", item.Name));
            $.Discount.setText(String.format("享有折扣：%.2f折", parseFloat(item.Discount) / 100));
            $.Remark.html = Utility.GetWebView(item.Remark);
            $.Progress.setWidth(100 * (parseFloat(item.Experience) / parseFloat(NextExperience)) + "%");
            $.CarFace.setImage(Alloy.Globals.WebUrl + item.Face);
            vendorId = item.VendorId;
            vendorName = item.VendorName;
        });
    }
    function Close() {
        $.win.close();
    }
    function Go() {
        UI.Open("vendorStore", {
            id: vendorId,
            vendor: vendorName
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "CardDetail";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId19 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId19"
    });
    $.__views.TitleBar.add($.__views.__alloyId19);
    Close ? $.__views.__alloyId19.addEventListener("click", Close) : __defers["$.__views.__alloyId19!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId19.add($.__views.btnBack);
    $.__views.Title = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "会员卡信息",
        id: "Title"
    });
    $.__views.TitleBar.add($.__views.Title);
    $.__views.MainWin = Ti.UI.createScrollView({
        top: "40dp",
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        horizontalWrap: true,
        layout: "vertical",
        scrollType: "vertical",
        showVerticalScrollIndicator: true,
        showHorizontalScrollIndicator: false,
        id: "MainWin",
        bottom: "40"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.__alloyId20 = Ti.UI.createView({
        layout: "vertical",
        left: "10",
        right: "10",
        height: Ti.UI.SIZE,
        id: "__alloyId20"
    });
    $.__views.MainWin.add($.__views.__alloyId20);
    $.__views.CarFace = Ti.UI.createImageView({
        width: "90%",
        height: Ti.UI.SIZE,
        id: "CarFace"
    });
    $.__views.__alloyId20.add($.__views.CarFace);
    $.__views.__alloyId21 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId21"
    });
    $.__views.__alloyId20.add($.__views.__alloyId21);
    $.__views.__alloyId22 = Ti.UI.createLabel({
        text: "经验值",
        left: "0",
        id: "__alloyId22"
    });
    $.__views.__alloyId21.add($.__views.__alloyId22);
    $.__views.ProgressBox = Ti.UI.createView({
        left: 70,
        backgroundColor: "#bbb",
        height: 18,
        borderRadius: 10,
        id: "ProgressBox"
    });
    $.__views.__alloyId21.add($.__views.ProgressBox);
    $.__views.Progress = Ti.UI.createView({
        left: "0",
        width: 0,
        backgroundColor: "#0087D1",
        height: Ti.UI.FILL,
        borderRadius: 10,
        id: "Progress"
    });
    $.__views.ProgressBox.add($.__views.Progress);
    $.__views.Vendor = Ti.UI.createLabel({
        font: {
            fontSize: 13
        },
        height: Ti.UI.SIZE,
        top: 5,
        width: Ti.UI.FILL,
        color: "#333",
        id: "Vendor"
    });
    $.__views.__alloyId20.add($.__views.Vendor);
    $.__views.Name = Ti.UI.createLabel({
        font: {
            fontSize: 13
        },
        height: Ti.UI.SIZE,
        top: 5,
        width: Ti.UI.FILL,
        color: "#333",
        id: "Name"
    });
    $.__views.__alloyId20.add($.__views.Name);
    $.__views.Discount = Ti.UI.createLabel({
        font: {
            fontSize: 13
        },
        height: Ti.UI.SIZE,
        top: 5,
        width: Ti.UI.FILL,
        color: "#333",
        id: "Discount"
    });
    $.__views.__alloyId20.add($.__views.Discount);
    $.__views.Remark = Ti.UI.createWebView({
        id: "Remark",
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE
    });
    $.__views.__alloyId20.add($.__views.Remark);
    $.__views.GoVendorButton = Ti.UI.createView({
        height: "40dp",
        id: "GoVendorButton",
        bottom: "0"
    });
    $.__views.win.add($.__views.GoVendorButton);
    $.__views.__alloyId23 = Ti.UI.createView({
        height: "35dp",
        width: "90%",
        id: "__alloyId23"
    });
    $.__views.GoVendorButton.add($.__views.__alloyId23);
    Go ? $.__views.__alloyId23.addEventListener("click", Go) : __defers["$.__views.__alloyId23!click!Go"] = true;
    $.__views.__alloyId24 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId24"
    });
    $.__views.__alloyId23.add($.__views.__alloyId24);
    $.__views.__alloyId25 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId25"
    });
    $.__views.__alloyId23.add($.__views.__alloyId25);
    $.__views.__alloyId26 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "去逛一下",
        id: "__alloyId26"
    });
    $.__views.__alloyId25.add($.__views.__alloyId26);
    $.__views.__alloyId27 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId27"
    });
    $.__views.__alloyId23.add($.__views.__alloyId27);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var Utility = require("Utility");
    var UI = require("UI");
    var args = arguments[0] || {};
    var id = args.id || "";
    var NextExperience = args.NextExperience || 0;
    var vendorId;
    var vendorName;
    Ti.API.debug("id:", id, "NextExperience", NextExperience);
    if (id) {
        BindCardDetail();
        $.GoVendorButton.setVisible(true);
    } else {
        ReadBailing();
        $.GoVendorButton.setVisible(false);
    }
    __defers["$.__views.__alloyId19!click!Close"] && $.__views.__alloyId19.addEventListener("click", Close);
    __defers["$.__views.__alloyId23!click!Go"] && $.__views.__alloyId23.addEventListener("click", Go);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;