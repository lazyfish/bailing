function Controller() {
    function GotoProduct() {
        "1" == type ? UI.Open("product", {
            id: pid
        }) : UI.Open("activityInfo", {
            id: pid
        });
    }
    function Del(e) {
        var url = Alloy.Globals.ApiUrl + "/car.ashx?action=del&id=" + id;
        Ajax.get(url, function(data) {
            if ("ok" == data) {
                var prompt = new Dialog.prompt();
                prompt.Show("删除成功！", 2e3);
                delFun(e);
            }
        });
        e.cancelBubble = true;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "BuyCarRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.BuyCarRow = Ti.UI.createTableViewRow({
        id: "BuyCarRow"
    });
    $.__views.BuyCarRow && $.addTopLevelView($.__views.BuyCarRow);
    GotoProduct ? $.__views.BuyCarRow.addEventListener("click", GotoProduct) : __defers["$.__views.BuyCarRow!click!GotoProduct"] = true;
    $.__views.__alloyId13 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId13"
    });
    $.__views.BuyCarRow.add($.__views.__alloyId13);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId13.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId14 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        right: "30dp",
        height: Ti.UI.SIZE,
        id: "__alloyId14"
    });
    $.__views.row.add($.__views.__alloyId14);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId14.add($.__views.title);
    $.__views.price = Ti.UI.createLabel({
        font: {
            fontSize: "14dp"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        id: "price"
    });
    $.__views.__alloyId14.add($.__views.price);
    $.__views.__alloyId15 = Ti.UI.createImageView({
        right: "0",
        width: "30dp",
        height: Ti.UI.SIZE,
        image: "/ic_del.png",
        id: "__alloyId15"
    });
    $.__views.row.add($.__views.__alloyId15);
    Del ? $.__views.__alloyId15.addEventListener("click", Del) : __defers["$.__views.__alloyId15!click!Del"] = true;
    $.__views.__alloyId16 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId16"
    });
    $.__views.__alloyId13.add($.__views.__alloyId16);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Ajax = require("Ajax");
    var Dialog = require("Dialog");
    var args = arguments[0] || {};
    var id = args.id;
    var pid = args.pid;
    var num = args.num;
    var price = args.price;
    var name = args.name;
    var face = args.face;
    var type = args.type;
    var delFun = args.delFun;
    $.face.setImage(Alloy.Globals.WebUrl + face);
    $.title.setText(name);
    $.price.setText(String.format("售价:%.2f元  数量:%d", parseFloat(price), parseInt(num)));
    __defers["$.__views.BuyCarRow!click!GotoProduct"] && $.__views.BuyCarRow.addEventListener("click", GotoProduct);
    __defers["$.__views.__alloyId15!click!Del"] && $.__views.__alloyId15.addEventListener("click", Del);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;