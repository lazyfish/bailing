function Controller() {
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vendorIntroduce";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        layout: "vertical",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId359 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId359"
    });
    $.__views.TitleBar.add($.__views.__alloyId359);
    Close ? $.__views.__alloyId359.addEventListener("click", Close) : __defers["$.__views.__alloyId359!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId359.add($.__views.btnBack);
    $.__views.__alloyId360 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "商家介绍",
        id: "__alloyId360"
    });
    $.__views.TitleBar.add($.__views.__alloyId360);
    $.__views.Title = Ti.UI.createLabel({
        top: "10dp",
        color: "#333",
        font: {
            fontSize: "18dp",
            fontWeight: "normal"
        },
        text: "悦华酒店",
        id: "Title"
    });
    $.__views.win.add($.__views.Title);
    $.__views.__alloyId361 = Ti.UI.createView({
        backgroundColor: "#fff",
        height: "2dp",
        id: "__alloyId361"
    });
    $.__views.win.add($.__views.__alloyId361);
    $.__views.__alloyId362 = Ti.UI.createScrollView({
        id: "__alloyId362"
    });
    $.__views.win.add($.__views.__alloyId362);
    $.__views.Content = Ti.UI.createLabel({
        left: "5dp",
        right: "5dp",
        top: "10dp",
        color: "#666",
        font: {
            fontSize: "14dp"
        },
        text: "厦门悦华酒店是静、美、雅和温馨并具有中国文化特色的五星级花园式豪华商务会议酒店。“悦华”的使命是“专业温馨服务•美好商旅生活”。\n				五星悦华。厦门悦华酒店是厦门建发旅游集团五星级豪华酒店连锁品牌—“悦华”的旗舰店，是厦门特区第一家、也是福建省第一家五星级酒店。酒店地处厦门经济特区发祥地，位于厦门海湾型旅游港口风景城市的几何中心，往来厦门国际机场、火炬高新技术产业开发区、繁华商业街中山路、轮渡、海上花园鼓浪屿和SM城市购物广场等均在10余分钟车程，交通十分便利。\n				绿色悦华。悦华酒店是金叶级绿色饭店，酒店占地20万平方米，最漂亮的是她价值连城的生态花园—馨悦园，面积达7万平方米。郁郁葱葱的凤凰山、风光旖旎的馨悦湖、春色满园的绿茵、争奇斗艳的鲜花、翩翩起舞的蝴蝶伴随着中国都市里极其罕见的超大型稀有生态花园。悦华四季如春、自然奢华，风景如画，静、美、雅、温馨并具中国文化特色的“都市绿洲”浑然天成，是成功商务、会议和度假的世外桃源。\n				商务悦华。酒店共拥有427间套各式五星级豪华花园、湾景、庭院或别墅景观客房。馨悦商务楼花园客房视野开阔，推窗即是悦华上万平方米的皇家花园，6-7楼客房更可欣赏厦门西港海湾美景。完全独立的海宇行政楼宇空间非常宽敞、全程行政管家服务、独立行政中心配置，房内高速宽带网络、会议室、阅览室、网吧、自助餐厅和行政酒廊一应俱全，更加方便，是来厦商旅访问的高端贵宾聚集之所。\n				健康悦华。酒店康体设施一应俱全，是高档的休闲、健康、运动场所，拥有室内恒温水疗游泳馆、1690米店内花园跑道和2个国际标准网球场，有氧运动的自然健康生活方式在这里精彩体现，随时享受大自然，从中获取心灵的满足。\n				会奖悦华。悦华酒店荣获厦门市人民政府颁发的“最佳会议场所”称号，悦华素有“会议专家”的美誉，提供一流的商务会议管家服务。\n				会议宴会场所主要有1100㎡高度6米的无柱的千人会议宴会大厅、370㎡国际会议厅、530㎡的宴会大厅和超级奢华的总统别墅贵宾楼大厅，同时配备最新的和专业的会议设施，包括六国语言同步传译系统和环形麦克风系统等高科技会议设备。酒店曾成功承办中日韩部长级通信、亚太经合组织APEC和ABB、MSD、赛诺菲、施耐德等世界500强跨国公司等最高规格国际会议。\n				2009年4月23日，亚洲贸易促进论坛（ATPF）第22届年会在厦门悦华酒店举行，亚太地区21个经济体的贸促机构主要负责人、中国贸促会行业及地方分会代表100多人参加会议，此次是ATPF首次在非首都地区举办。这是对厦门、也是对悦华酒店接待能力的高度认可。\n				2010年，悦华酒店更是承接了8个国家200多名部长级官员或商业领袖参加的第二届世界投资论坛WIF2010会议。共分8个重要国宾接待团组，分别是一级接待的冰岛总统奥拉维尔•格里姆松、莫桑比克总理艾雷斯•阿里团组；二级接待的保加利亚副总统安格尔•马琳、加纳副总统约翰•马哈马、秘鲁副总统路易斯•詹彼得里•罗哈斯、蒙古国副总理米耶贡布•恩赫包勒德、津巴布韦副总理阿瑟•穆坦巴拉、牙买加众议长德尔罗伊•卓团组。一个酒店同时承接如此高规模的会议接待，这在中国酒店史上也多不见。\n				2012年1月3日举行的“第六届亚洲酒店业领袖峰会暨2011亚洲酒店业影响力百强”评选中，厦门悦华酒店因“悦华”品牌知名度和名誉度，独有的都市绿洲花园环境，一流的花园客房及会议宴会场所，赢得业界高度赞赏，荣获“中国最佳会议会展酒店”荣誉称号。\n				美食悦华。“一流宴会美食•高端宴会首选”。悦华宴会菜以装饰精美、口感健康细腻、富含生态、时尚、文化、健康的美食，结合国宾级服务标准而形成悦华美食品牌。一批专业的宴会设计专家，接待了数百位海内外国家元首、政要和商界名流，屡受好评，蜚声海内外。中式招牌菜有悦华佛跳墙是福建“闽菜”的首席招牌菜；西式招牌菜有法式局蜗牛、法国芝士局龙虾、特选安格斯牛扒等。金牌厨师，厨艺精湛，悦华贵宾宴喜获“福建名宴”， 多位资深大厨荣登2010厦门金厨。行政总厨林星煌荣获中国烹饪学会颁发的“2011年度中华金厨奖”。\n				温馨悦华。“温馨悦华•商旅之家”。“悦华”品牌经过20余年深厚的服务文化沉淀和积累，以“一笑二轻三热情”（微笑；说话轻、动作轻；迎客热情、待客热情、送客热情）的服务形象演绎着中国东方式的温馨服务，以悦华的专业标准为宾客提供“满意+惊喜”的优质服务，成为厦门酒店业的服务标杆。\n				荣耀悦华。悦华延续着其辉煌发展之路，曾接待过胡锦涛主席、江泽民主席、朱镕基总理等党和国家领导人、美国前总统理查德•尼克松、新加坡前总理李光耀、吴庆瑞博士、荷兰女王、汤加国王和文莱苏丹、KODAK、DELL全球总裁等。悦华在1989年被评为全国首批四星级酒店（当时中国最高星级酒店），1997年成为福建省第一家五星级酒店，是厦门唯一的中国名酒店组织CFHC成员，也是厦门市首家加入国际金钥匙组织的酒店。悦华酒店是2003年“中国旅游知名品牌”和2002—2005年“中国外商投资先进饭店”。酒店依托于福建企业集团100强之首的厦门建发集团，在泉州和武夷山成功运营“悦华”五星级高端酒店，福州悦华酒店、天津悦华酒店也陆续建设中。“悦华”这一高端的五星级品牌酒店正迅速崛起……\n				正如新加坡前总理李光耀题词所说：“悦华酒店会使投资者和其他来访者倍觉惬意。她是一个有价值的存在。当这些来访者考虑将资金投入这个经济特区时，悦华酒店可以保证他们住在熟悉的、温馨的、舒适的环境中，像家一样。”\n				我们诚挚欢迎您光临—“最浪漫的城市•厦门”、“最温馨的酒店•悦华”！。”\n\n				地址:中国福建省厦门湖里区悦华路101号,邮编361006\n				Add: 101 Yue Hua Road，Huli District，Xiamen，Fujian，China 361006\n				电话Tel:+0086-592-6023333 传真Fax:+0086-592-6021431\n				http://www.yeohwahotels.com/xm E-mail: homexm@yeohwa.com",
        id: "Content"
    });
    $.__views.__alloyId362.add($.__views.Content);
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.__alloyId359!click!Close"] && $.__views.__alloyId359.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;