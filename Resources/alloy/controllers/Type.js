function Controller() {
    function LoadParent() {
        var url = Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=0";
        Ajax.json(url, function(item) {
            var view = InitItem(item.id, item.ItemName);
            view.Bind("click", function() {
                var children = $.ParentType.getChildren();
                for (var i in children) children[i].setBackgroundImage("/content_bg.png");
                this.setBackgroundImage("/catogory_bg_b.png");
                selectedId = "";
                LoadChild(this.tid);
            });
            $.ParentType.add(view);
        });
    }
    function LoadChild(id) {
        $.ChildType.removeAllChildren();
        var url = Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + id;
        Ajax.json(url, function(item) {
            var view = InitItem(item.id, item.ItemName);
            view.Bind("click", function() {
                selectedId = this.tid;
                var children = $.ChildType.getChildren();
                for (var i in children) children[i].setBackgroundImage("/content_bg.png");
                this.setBackgroundImage("/catogory_bg_b.png");
            });
            $.ChildType.add(view);
        });
    }
    function InitItem(id, name) {
        var viewStyle = {
            width: Ti.UI.SIZE,
            backgroundImage: "/content_bg.png",
            backgroundFocusedImage: "/catogory_bg_b.png",
            backgroundSelectedImage: "/catogory_bg_b.png",
            height: 30,
            left: "10",
            top: "10"
        };
        var fontStyle = {
            width: 18 * name.length,
            height: 30,
            textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
            left: "10",
            right: "10",
            font: {
                size: 12
            },
            text: name,
            color: "#333333"
        };
        var viewItem = UI.C("view", viewStyle);
        var title = UI.C("label", fontStyle);
        viewItem.add(title);
        viewItem.tid = id;
        return viewItem;
    }
    function Ok() {
        if ("" != selectedId) {
            var url = Alloy.Globals.ApiUrl + "/allclass.ashx?action=AddUserType&idguid=" + Alloy.Globals.LoginToken + "&TypeId=" + selectedId;
            Ti.API.debug("添加类别：" + url);
            Ajax.get(url, function(data) {
                var prompt = new Dialog.prompt();
                if ("ok" == data) {
                    prompt.Show("添加成功", 2e3);
                    callFun();
                } else prompt.Show("添加失败，请重新添加", 2e3);
            });
        }
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Type";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win",
        layout: "vertical"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId188 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId188"
    });
    $.__views.TitleBar.add($.__views.__alloyId188);
    Close ? $.__views.__alloyId188.addEventListener("click", Close) : __defers["$.__views.__alloyId188!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId188.add($.__views.btnBack);
    $.__views.__alloyId189 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "添加快捷类别",
        id: "__alloyId189"
    });
    $.__views.TitleBar.add($.__views.__alloyId189);
    $.__views.__alloyId190 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        right: "0dp",
        id: "__alloyId190"
    });
    $.__views.TitleBar.add($.__views.__alloyId190);
    $.__views.btnOk = Ti.UI.createImageView({
        image: "/ok.png",
        width: "25dp",
        right: "5dp",
        height: "25dp",
        id: "btnOk"
    });
    $.__views.__alloyId190.add($.__views.btnOk);
    Ok ? $.__views.btnOk.addEventListener("click", Ok) : __defers["$.__views.btnOk!click!Ok"] = true;
    $.__views.__alloyId191 = Ti.UI.createLabel({
        height: 30,
        backgroundImage: "/catogory_bar.png",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        font: {
            size: 13
        },
        color: "#333333",
        width: Ti.UI.FILL,
        text: "选择大分类",
        id: "__alloyId191"
    });
    $.__views.win.add($.__views.__alloyId191);
    $.__views.ParentType = Ti.UI.createView({
        layout: "horizontal",
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        contentWidth: "100%",
        horizontalWrap: true,
        id: "ParentType"
    });
    $.__views.win.add($.__views.ParentType);
    $.__views.__alloyId192 = Ti.UI.createLabel({
        height: 30,
        backgroundImage: "/catogory_bar.png",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        font: {
            size: 13
        },
        color: "#333333",
        width: Ti.UI.FILL,
        text: "选择子分类",
        id: "__alloyId192"
    });
    $.__views.win.add($.__views.__alloyId192);
    $.__views.ChildType = Ti.UI.createScrollView({
        layout: "horizontal",
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        contentWidth: "100%",
        horizontalWrap: true,
        id: "ChildType"
    });
    $.__views.win.add($.__views.ChildType);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var Dialog = require("Dialog");
    var args = arguments[0] || {};
    var callFun = args.callFun || null;
    LoadParent();
    var selectedId = "";
    __defers["$.__views.__alloyId188!click!Close"] && $.__views.__alloyId188.addEventListener("click", Close);
    __defers["$.__views.btnOk!click!Ok"] && $.__views.btnOk.addEventListener("click", Ok);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;