function Controller() {
    function Close() {
        $.win.close();
    }
    function Reload(e) {
        alert(e.url);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "ShareWeibo";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId176 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId176"
    });
    $.__views.TitleBar.add($.__views.__alloyId176);
    Close ? $.__views.__alloyId176.addEventListener("click", Close) : __defers["$.__views.__alloyId176!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId176.add($.__views.btnBack);
    $.__views.__alloyId177 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "分享微博",
        id: "__alloyId177"
    });
    $.__views.TitleBar.add($.__views.__alloyId177);
    $.__views.__alloyId178 = Ti.UI.createWebView({
        url: "http://qzo2o.s1.kezhishang.com/mobile/weiboshare.aspx",
        top: "40dp",
        id: "__alloyId178"
    });
    $.__views.win.add($.__views.__alloyId178);
    Reload ? $.__views.__alloyId178.addEventListener("beforeload", Reload) : __defers["$.__views.__alloyId178!beforeload!Reload"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.__alloyId176!click!Close"] && $.__views.__alloyId176.addEventListener("click", Close);
    __defers["$.__views.__alloyId178!beforeload!Reload"] && $.__views.__alloyId178.addEventListener("beforeload", Reload);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;