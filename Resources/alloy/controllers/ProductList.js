function Controller() {
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "ProductList";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#404141",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId114 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId114"
    });
    $.__views.TitleBar.add($.__views.__alloyId114);
    Close ? $.__views.__alloyId114.addEventListener("click", Close) : __defers["$.__views.__alloyId114!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId114.add($.__views.btnBack);
    $.__views.__alloyId115 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "搜索到的产品",
        id: "__alloyId115"
    });
    $.__views.TitleBar.add($.__views.__alloyId115);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox"
    });
    $.__views.MainWin.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#404141",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var args = arguments[0] || {};
    args.type || "";
    var keyword = args.keyword || "";
    var url = Alloy.Globals.ApiUrl + "/product.ashx?action=search&keyword" + keyword;
    Ajax.json(url, function(item) {
        Ti.API.info(item.ProContent);
        return UI.Controller("vendorProRow", {
            id: item.id,
            face: item.CoverPicture,
            title: item.ProName,
            content: item.ProContent,
            price: item.oPrice,
            pics: item.ProPics
        });
    }, function(data) {
        $.table.setData(data);
    });
    __defers["$.__views.__alloyId114!click!Close"] && $.__views.__alloyId114.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;