function Controller() {
    function BindTable() {
        Titanium.Geolocation.addEventListener("location", function(e) {
            hasLoad || GetTable(e.coords);
        });
        Ti.Geolocation.getCurrentPosition(function(e) {
            e.success && (hasLoad || GetTable(e.coords));
        });
    }
    function GetTable(coords) {
        hasLoad = true;
        var url = Alloy.Globals.ApiUrl + "/store.ashx?action=random&lng=" + coords.longitude + "&lat=" + coords.latitude;
        Ajax.json(url, CreateStoreItem, FinishLoadStore);
    }
    function CreateStoreItem(item) {
        return Alloy.createController("vendorStoreRow", {
            face: Alloy.Globals.WebUrl + item.StorePicUrl,
            title: item.StoreName,
            id: item.id,
            content: item.ContactTel,
            range: item.Range + "米"
        }).getView();
    }
    function FinishLoadStore(data) {
        $.table.setData(data);
        actInd.hide();
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Like";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.__alloyId75 = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#fff",
        id: "__alloyId75"
    });
    $.__views.win.add($.__views.__alloyId75);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.__alloyId75.add($.__views.TitleBar);
    $.__views.__alloyId76 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId76"
    });
    $.__views.TitleBar.add($.__views.__alloyId76);
    Close ? $.__views.__alloyId76.addEventListener("click", Close) : __defers["$.__views.__alloyId76!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId76.add($.__views.btnBack);
    $.__views.__alloyId77 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "猜您喜欢",
        id: "__alloyId77"
    });
    $.__views.TitleBar.add($.__views.__alloyId77);
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox"
    });
    $.__views.__alloyId75.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    BindTable();
    Ti.Geolocation.purpose = "Get Current Location";
    Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
    if (Ti.Geolocation.locationServicesEnabled) {
        Ti.Geolocation.distanceFilter = 10;
        Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_NETWORK;
    } else alert("请启用您的GPS定位服务");
    var actInd = Titanium.UI.createActivityIndicator({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#fff",
        message: "获取位置中..."
    });
    $.win.add(actInd);
    actInd.show();
    var hasLoad = false;
    __defers["$.__views.__alloyId76!click!Close"] && $.__views.__alloyId76.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;