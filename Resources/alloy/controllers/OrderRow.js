function Controller() {
    function GotoDetail() {
        UI.Open("OrderDetail", {
            id: id,
            sn: sn,
            price: price,
            time: time,
            OrderState: OrderState
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "OrderRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.OrderRow = Ti.UI.createTableViewRow({
        id: "OrderRow"
    });
    $.__views.OrderRow && $.addTopLevelView($.__views.OrderRow);
    GotoDetail ? $.__views.OrderRow.addEventListener("click", GotoDetail) : __defers["$.__views.OrderRow!click!GotoDetail"] = true;
    $.__views.__alloyId111 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId111"
    });
    $.__views.OrderRow.add($.__views.__alloyId111);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row",
        bottom: "10dp"
    });
    $.__views.__alloyId111.add($.__views.row);
    $.__views.title = Ti.UI.createLabel({
        top: "0",
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.row.add($.__views.title);
    $.__views.price = Ti.UI.createLabel({
        top: "25dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "13dp"
        },
        id: "price"
    });
    $.__views.row.add($.__views.price);
    $.__views.time = Ti.UI.createLabel({
        top: "25dp",
        right: "0",
        color: "#666",
        font: {
            fontSize: "13dp"
        },
        id: "time"
    });
    $.__views.row.add($.__views.time);
    $.__views.__alloyId112 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#999",
        id: "__alloyId112"
    });
    $.__views.__alloyId111.add($.__views.__alloyId112);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var DateUtility = require("DateUtility");
    var args = arguments[0] || {};
    var id = args.id || "";
    var sn = args.sn || "";
    var price = args.price || "";
    var time = args.time || "";
    var OrderState = args.OrderState || "";
    $.title.text = sn;
    $.price.text = String.format("总额：%.2f元", parseFloat(price));
    $.time.text = DateUtility.ToString(time, "yyyy-MM-dd HH:mm:ss");
    __defers["$.__views.OrderRow!click!GotoDetail"] && $.__views.OrderRow.addEventListener("click", GotoDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;