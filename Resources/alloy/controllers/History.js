function Controller() {
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "History";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId70 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId70"
    });
    $.__views.TitleBar.add($.__views.__alloyId70);
    Close ? $.__views.__alloyId70.addEventListener("click", Close) : __defers["$.__views.__alloyId70!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId70.add($.__views.btnBack);
    $.__views.__alloyId71 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "浏览历史",
        id: "__alloyId71"
    });
    $.__views.TitleBar.add($.__views.__alloyId71);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.MainWin.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    require("DateUtility");
    var url = Alloy.Globals.ApiUrl + "/history.ashx?action=list&idguid=" + Alloy.Globals.LoginToken;
    Ti.API.debug(url);
    Ajax.json(url, function(item) {
        return UI.Controller("HistoryRow", {
            id: item.id,
            pid: item.Pid,
            price: item.Price,
            name: item.ProName,
            time: item.AddTime,
            face: item.CoverPicture
        });
    }, function(data) {
        $.table.setData(data);
    });
    __defers["$.__views.__alloyId70!click!Close"] && $.__views.__alloyId70.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;