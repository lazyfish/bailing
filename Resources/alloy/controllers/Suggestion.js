function Controller() {
    function Submit() {
        Ajax.post(Alloy.Globals.ApiUrl + "/system.ashx?action=suggestion", {
            Content: $.Content.getValue(),
            idguid: Alloy.Globals.LoginToken
        }, function() {
            var prompt = new Dialog.prompt();
            prompt.Show("提交成功，感谢您对百领的支持", 1e3);
        });
        $.win.close();
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Suggestion";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId179 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId179"
    });
    $.__views.TitleBar.add($.__views.__alloyId179);
    Close ? $.__views.__alloyId179.addEventListener("click", Close) : __defers["$.__views.__alloyId179!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId179.add($.__views.btnBack);
    $.__views.__alloyId180 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "意见反馈",
        id: "__alloyId180"
    });
    $.__views.TitleBar.add($.__views.__alloyId180);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin",
        left: "10",
        right: "10",
        bottom: "10",
        layout: "horizontal"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.__alloyId181 = Ti.UI.createLabel({
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        text: "意见箱",
        id: "__alloyId181"
    });
    $.__views.MainWin.add($.__views.__alloyId181);
    $.__views.Content = Ti.UI.createTextArea({
        borderWidth: 2,
        borderColor: "#bbb",
        borderRadius: 5,
        color: "#888",
        textAlign: "left",
        hintText: "请在此输入您对百领的建议吧",
        height: 100,
        id: "Content",
        width: Ti.UI.FILL
    });
    $.__views.MainWin.add($.__views.Content);
    $.__views.__alloyId182 = Ti.UI.createView({
        height: "40dp",
        bottom: "0",
        id: "__alloyId182"
    });
    $.__views.MainWin.add($.__views.__alloyId182);
    $.__views.__alloyId183 = Ti.UI.createView({
        height: "35dp",
        width: "90%",
        id: "__alloyId183"
    });
    $.__views.__alloyId182.add($.__views.__alloyId183);
    Submit ? $.__views.__alloyId183.addEventListener("click", Submit) : __defers["$.__views.__alloyId183!click!Submit"] = true;
    $.__views.__alloyId184 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId184"
    });
    $.__views.__alloyId183.add($.__views.__alloyId184);
    $.__views.__alloyId185 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId185"
    });
    $.__views.__alloyId183.add($.__views.__alloyId185);
    $.__views.__alloyId186 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "提交",
        id: "__alloyId186"
    });
    $.__views.__alloyId185.add($.__views.__alloyId186);
    $.__views.__alloyId187 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId187"
    });
    $.__views.__alloyId183.add($.__views.__alloyId187);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var Dialog = require("Dialog");
    __defers["$.__views.__alloyId179!click!Close"] && $.__views.__alloyId179.addEventListener("click", Close);
    __defers["$.__views.__alloyId183!click!Submit"] && $.__views.__alloyId183.addEventListener("click", Submit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;