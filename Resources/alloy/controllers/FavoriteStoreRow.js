function Controller() {
    function GotoDetail() {
        Alloy.createController("storeDetail", {
            id: storeId
        }).getView().open();
    }
    function Del(e) {
        var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=del&id=" + id;
        Ajax.get(url, function(data) {
            if ("ok" == data) {
                var prompt = new Dialog.prompt();
                prompt.Show("删除成功！", 2e3);
                delFun(e);
            }
        });
        e.cancelBubble = true;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "FavoriteStoreRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.FavoriteStoreRow = Ti.UI.createTableViewRow({
        id: "FavoriteStoreRow"
    });
    $.__views.FavoriteStoreRow && $.addTopLevelView($.__views.FavoriteStoreRow);
    GotoDetail ? $.__views.FavoriteStoreRow.addEventListener("click", GotoDetail) : __defers["$.__views.FavoriteStoreRow!click!GotoDetail"] = true;
    $.__views.__alloyId62 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId62"
    });
    $.__views.FavoriteStoreRow.add($.__views.__alloyId62);
    $.__views.row = Ti.UI.createView({
        backgroundFocusedColor: "#ADCFE6",
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId62.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        image: "http://placehold.it/50x50",
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderColor: "#fff",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId63 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        width: Ti.UI.SIZE,
        right: "30dp",
        height: Ti.UI.SIZE,
        id: "__alloyId63"
    });
    $.__views.row.add($.__views.__alloyId63);
    $.__views.title = Ti.UI.createLabel({
        color: "#fff",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId63.add($.__views.title);
    $.__views.__alloyId64 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId64"
    });
    $.__views.__alloyId63.add($.__views.__alloyId64);
    $.__views.__alloyId65 = Ti.UI.createImageView({
        top: "10dp",
        left: "0dp",
        image: "/ic_phone.png",
        width: "10dp",
        height: Ti.UI.SIZE,
        id: "__alloyId65"
    });
    $.__views.__alloyId64.add($.__views.__alloyId65);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#eee",
        font: {
            fontSize: "15dp"
        },
        id: "content",
        left: "15dp"
    });
    $.__views.__alloyId64.add($.__views.content);
    $.__views.__alloyId66 = Ti.UI.createImageView({
        right: "0",
        width: "30dp",
        height: Ti.UI.SIZE,
        image: "/ic_del.png",
        id: "__alloyId66"
    });
    $.__views.row.add($.__views.__alloyId66);
    Del ? $.__views.__alloyId66.addEventListener("click", Del) : __defers["$.__views.__alloyId66!click!Del"] = true;
    $.__views.__alloyId67 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#343434",
        id: "__alloyId67"
    });
    $.__views.__alloyId62.add($.__views.__alloyId67);
    $.__views.__alloyId68 = Ti.UI.createView({
        height: "1dp",
        bottom: "1dp",
        backgroundColor: "#4F5050",
        id: "__alloyId68"
    });
    $.__views.__alloyId62.add($.__views.__alloyId68);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var Dialog = require("Dialog");
    var args = arguments[0] || {};
    var id = args.id || "";
    var storeId = args.storeId || "";
    var face = args.face || "";
    var title = args.title || "";
    var content = args.content || "";
    var delFun = args.delFun || "";
    $.face.setImage(face);
    $.title.setText(title);
    $.content.setText(content);
    $.content.addEventListener("click", function(e) {
        Ti.Platform.openURL("tel:" + content);
        e.cancelBubble = true;
    });
    __defers["$.__views.FavoriteStoreRow!click!GotoDetail"] && $.__views.FavoriteStoreRow.addEventListener("click", GotoDetail);
    __defers["$.__views.__alloyId66!click!Del"] && $.__views.__alloyId66.addEventListener("click", Del);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;