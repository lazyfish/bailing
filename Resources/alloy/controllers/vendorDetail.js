function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vendorDetail";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        layout: "vertical",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId354 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId354"
    });
    $.__views.TitleBar.add($.__views.__alloyId354);
    Close ? $.__views.__alloyId354.addEventListener("click", Close) : __defers["$.__views.__alloyId354!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId354.add($.__views.btnBack);
    $.__views.__alloyId355 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "商家信息",
        id: "__alloyId355"
    });
    $.__views.TitleBar.add($.__views.__alloyId355);
    var __alloyId356 = [];
    $.__views.__alloyId357 = Ti.UI.createImageView({
        backgroundImage: "/images/ad1.jpg",
        width: "100%",
        height: "100%",
        id: "__alloyId357"
    });
    __alloyId356.push($.__views.__alloyId357);
    $.__views.__alloyId358 = Ti.UI.createImageView({
        backgroundImage: "/images/ad2.jpg",
        width: "100%",
        height: "100%",
        id: "__alloyId358"
    });
    __alloyId356.push($.__views.__alloyId358);
    $.__views.FaceBox = Ti.UI.createScrollableView({
        views: __alloyId356,
        id: "FaceBox"
    });
    $.__views.win.add($.__views.FaceBox);
    $.__views.Title = Ti.UI.createLabel({
        id: "Title",
        text: "悦华酒店"
    });
    $.__views.win.add($.__views.Title);
    $.__views.InfoBox = Ti.UI.createView({
        id: "InfoBox"
    });
    $.__views.win.add($.__views.InfoBox);
    exports.destroy = function() {};
    _.extend($, $.__views);
    __defers["$.__views.__alloyId354!click!Close"] && $.__views.__alloyId354.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;