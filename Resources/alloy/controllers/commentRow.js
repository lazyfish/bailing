function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "commentRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.commentRow = Ti.UI.createTableViewRow({
        layout: "vertical",
        id: "commentRow"
    });
    $.__views.commentRow && $.addTopLevelView($.__views.commentRow);
    $.__views.Title = Ti.UI.createLabel({
        color: "#333",
        left: "10dp",
        font: {
            fontSize: "15dp"
        },
        id: "Title"
    });
    $.__views.commentRow.add($.__views.Title);
    $.__views.Content = Ti.UI.createLabel({
        color: "#666",
        left: "10dp",
        top: "10dp",
        font: {
            fontSize: "14dp"
        },
        id: "Content"
    });
    $.__views.commentRow.add($.__views.Content);
    $.__views.Time = Ti.UI.createLabel({
        color: "#999",
        right: "10dp",
        font: {
            fontSize: "12dp"
        },
        id: "Time"
    });
    $.__views.commentRow.add($.__views.Time);
    $.__views.__alloyId217 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId217"
    });
    $.__views.commentRow.add($.__views.__alloyId217);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var title = args.title || "";
    var content = args.content || "";
    var date = args.date || new Date();
    var dateUtility = require("DateUtility");
    $.Title.setText(title);
    $.Content.setText(content);
    date ? $.Time.setText(dateUtility.ToString(date, "yyyy-MM-dd HH:mm:ss")) : $.Time.setText("----");
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;