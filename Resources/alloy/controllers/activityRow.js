function Controller() {
    function GotoDetail() {
        Alloy.createController("activityInfo", {
            id: id,
            face: pics,
            title: title,
            Storeid: Storeid,
            vendorId: vendorId,
            content: content,
            price: price
        }).getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "activityRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.activityRow = Ti.UI.createTableViewRow({
        id: "activityRow"
    });
    $.__views.activityRow && $.addTopLevelView($.__views.activityRow);
    GotoDetail ? $.__views.activityRow.addEventListener("click", GotoDetail) : __defers["$.__views.activityRow!click!GotoDetail"] = true;
    $.__views.__alloyId214 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId214"
    });
    $.__views.activityRow.add($.__views.__alloyId214);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId214.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId215 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        height: Ti.UI.SIZE,
        id: "__alloyId215"
    });
    $.__views.row.add($.__views.__alloyId215);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId215.add($.__views.title);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "15dp"
        },
        id: "content"
    });
    $.__views.__alloyId215.add($.__views.content);
    $.__views.__alloyId216 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId216"
    });
    $.__views.__alloyId214.add($.__views.__alloyId216);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var id = args.id || "";
    var face = args.face || "";
    var title = args.title || "";
    var content = args.content || "";
    var price = args.price || "";
    var Storeid = args.Storeid || "";
    var pics = args.pics || "";
    var vendorId = args.vendorId || "";
    Ti.API.info(id);
    $.face.setImage(Alloy.Globals.WebUrl + face);
    $.title.setText(title);
    $.content.setText(String.format("参与价：%.2f元", parseFloat(price)));
    __defers["$.__views.activityRow!click!GotoDetail"] && $.__views.activityRow.addEventListener("click", GotoDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;