function Controller() {
    function Init() {
        LoadIndustryType();
        LoadArea();
        LoadSortItem();
        BindTable();
    }
    function BindTable() {
        url = Alloy.Globals.ApiUrl + "/store.ashx?action=getverdonlist&t=" + type + "&area=" + area + "&keyword=" + keyword + "&sort=" + sort;
        moreView || (moreView = UI.Controller("MoreRow", {
            url: url,
            index: 0,
            itemFun: LoadItem,
            finishFun: LoadFinish
        }));
        Ti.API.info(url);
        Ajax.json(url, LoadItem, LoadFinish);
    }
    function LoadItem(item) {
        var dataItem = Alloy.createController("vendorRow", {
            id: item.id,
            face: Alloy.Globals.WebUrl + item.StorePicUrl,
            title: item.StoreName,
            content: ""
        }).getView();
        dataList.push(dataItem);
        return dataItem;
    }
    function LoadFinish(data, total) {
        $.table.setData(dataList);
        parseInt(total) > dataList.length && $.table.appendRow(moreView);
    }
    function LoadIndustryType() {
        typeDialog = new ListDialog.option();
        typeDialog.Init($.win, {
            left: "5dp",
            top: "70dp"
        }, {
            width: "300dp",
            height: "240dp"
        }, [], []);
        typeDialog.OnRootClick = function(e) {
            if (0 == e.id) {
                type = e.id;
                BindTable();
                typeDialog.Hide();
            } else Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + e.id, LoadingIndustryChildItem, LoadedIndustryChildItem);
        };
        typeDialog.OnChildClick = function(e) {
            type = e.id;
            BindTable();
            typeDialog.Hide();
        };
        Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=0", LoadingIndustryRootItem, LoadedIndustryRootItem);
    }
    function LoadingIndustryRootItem(item) {
        return {
            id: item.id,
            name: item.ItemName
        };
    }
    function LoadedIndustryRootItem(data) {
        data.unshift({
            id: "0",
            name: "全部"
        });
        typeDialog.BindData(data);
        data.length > 1 && Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + type || data[1].id, LoadingIndustryChildItem, LoadedIndustryChildItem);
        typeDialog.SetCurrentRoot(type);
    }
    function LoadingIndustryChildItem(item) {
        return {
            id: item.id,
            name: item.ItemName,
            parent: item.ParentId
        };
    }
    function LoadedIndustryChildItem(data) {
        data.unshift({
            id: typeDialog._rootId,
            name: "全部"
        });
        typeDialog.BindData(null, data);
    }
    function LoadSortItem() {
        sortDialog = new ListDialog.option();
        left = Alloy.Globals.width / 2;
        sortDialog.Init($.win, {
            left: left,
            top: "70dp"
        }, {
            width: "140dp",
            height: "80dp"
        }, []);
        var data = [ {
            id: 0,
            name: "默认排序"
        }, {
            id: 1,
            name: "按人气从高到低"
        } ];
        sortDialog.OnRootClick = function(e) {
            sort = e.id;
            BindTable();
            sortDialog.Hide();
        };
        sortDialog.BindData(data);
        sortDialog.SetCurrentRoot(0);
    }
    function LoadArea() {
        areaDialog = new ListDialog.option();
        left = Alloy.Globals.width / 2 - 70;
        areaDialog.Init($.win, {
            left: left,
            top: "70dp"
        }, {
            width: "140dp",
            height: "240dp"
        }, []);
        Ajax.json(Alloy.Globals.ApiUrl + "/ct.ashx?action=getarea&code=350500", function(item) {
            return {
                id: item.CTCode,
                name: item.CTName
            };
        }, function(data) {
            data.unshift({
                id: "350500",
                name: "全城"
            });
            areaDialog.BindData(data);
        });
        areaDialog.OnRootClick = function(e) {
            area = e.id;
            BindTable();
            areaDialog.Hide();
        };
    }
    function ShowTypeWin() {
        typeDialog.Show();
        $.SortTab.setBackgroundImage("");
        $.SortTab.setColor("#84868D");
        $.AreaTab.setBackgroundImage("");
        $.AreaTab.setColor("#84868D");
        $.TypeTab.setBackgroundImage("/tool_left.png");
        $.TypeTab.setColor("#fff");
    }
    function ShowAreaWin() {
        areaDialog.Show();
        $.SortTab.setBackgroundImage("");
        $.SortTab.setColor("#84868D");
        $.AreaTab.setBackgroundImage("/tool_mid.png");
        $.AreaTab.setColor("#fff");
        $.TypeTab.setBackgroundImage("");
        $.TypeTab.setColor("#84868D");
    }
    function ShowSortWin() {
        sortDialog.Show();
        $.SortTab.setBackgroundImage("/tool_right.png");
        $.SortTab.setColor("#fff");
        $.AreaTab.setBackgroundImage("");
        $.AreaTab.setColor("#84868D");
        $.TypeTab.setBackgroundImage("");
        $.TypeTab.setColor("#84868D");
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vendor";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.__alloyId348 = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#fff",
        id: "__alloyId348"
    });
    $.__views.win.add($.__views.__alloyId348);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.__alloyId348.add($.__views.TitleBar);
    $.__views.__alloyId349 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId349"
    });
    $.__views.TitleBar.add($.__views.__alloyId349);
    Close ? $.__views.__alloyId349.addEventListener("click", Close) : __defers["$.__views.__alloyId349!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId349.add($.__views.btnBack);
    $.__views.__alloyId350 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "商家列表",
        id: "__alloyId350"
    });
    $.__views.TitleBar.add($.__views.__alloyId350);
    $.__views.__alloyId351 = Ti.UI.createView({
        height: "45dp",
        backgroundImage: "/tag_bg.png",
        width: Ti.UI.FILL,
        id: "__alloyId351"
    });
    $.__views.__alloyId348.add($.__views.__alloyId351);
    $.__views.__alloyId352 = Ti.UI.createView({
        top: "5dp",
        height: "35dp",
        backgroundImage: "/store_type_bg_a.png",
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId352"
    });
    $.__views.__alloyId351.add($.__views.__alloyId352);
    $.__views.__alloyId353 = Ti.UI.createView({
        top: "5dp",
        right: 5,
        left: 5,
        height: "25dp",
        backgroundImage: "/store_type_bg_b.png",
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId353"
    });
    $.__views.__alloyId352.add($.__views.__alloyId353);
    $.__views.TypeTab = Ti.UI.createLabel({
        width: 100,
        font: {
            fontSize: 14
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#fff",
        backgroundImage: "/tool_left.png",
        text: "类别",
        id: "TypeTab"
    });
    $.__views.__alloyId353.add($.__views.TypeTab);
    ShowTypeWin ? $.__views.TypeTab.addEventListener("click", ShowTypeWin) : __defers["$.__views.TypeTab!click!ShowTypeWin"] = true;
    $.__views.AreaTab = Ti.UI.createLabel({
        width: 100,
        font: {
            fontSize: "14dp"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#84868D",
        text: "地区",
        id: "AreaTab"
    });
    $.__views.__alloyId353.add($.__views.AreaTab);
    ShowAreaWin ? $.__views.AreaTab.addEventListener("click", ShowAreaWin) : __defers["$.__views.AreaTab!click!ShowAreaWin"] = true;
    $.__views.SortTab = Ti.UI.createLabel({
        width: 100,
        font: {
            fontSize: 14
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#84868D",
        text: "默认排序",
        id: "SortTab"
    });
    $.__views.__alloyId353.add($.__views.SortTab);
    ShowSortWin ? $.__views.SortTab.addEventListener("click", ShowSortWin) : __defers["$.__views.SortTab!click!ShowSortWin"] = true;
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox"
    });
    $.__views.__alloyId348.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var ListDialog = require("ListDialog");
    var args = arguments[0] || {};
    var type = args.type || "";
    var keyword = args.keyword || "";
    var area = "";
    var sort = 0;
    var typeDialog;
    var areaDialog;
    var sortDialog = null;
    var url = "";
    var moreView = null;
    var dataList = new Array();
    Init();
    __defers["$.__views.__alloyId349!click!Close"] && $.__views.__alloyId349.addEventListener("click", Close);
    __defers["$.__views.TypeTab!click!ShowTypeWin"] && $.__views.TypeTab.addEventListener("click", ShowTypeWin);
    __defers["$.__views.AreaTab!click!ShowAreaWin"] && $.__views.AreaTab.addEventListener("click", ShowAreaWin);
    __defers["$.__views.SortTab!click!ShowSortWin"] && $.__views.SortTab.addEventListener("click", ShowSortWin);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;