function Controller() {
    function Search(e) {
        dataList = new Array();
        $.HotKeyword.setVisible(false);
        $.search.blur();
        0 == searchType ? BindProduct(e.value) : BindVendor(e.value);
    }
    function BindProduct(keyword) {
        $.VendorTable.setVisible(false);
        $.ProductTable.setVisible(true);
        url = Alloy.Globals.ApiUrl + "/product.ashx?action=search&keyword=" + keyword;
        moreView = UI.Controller("MoreRow", {
            url: url,
            index: 0,
            itemFun: LoadProductItem,
            finishFun: LoadProductFinish
        });
        Ajax.json(url, LoadProductItem, LoadProductFinish);
    }
    function LoadProductItem(item) {
        var dataItem = UI.Controller("vendorProRow", {
            id: item.id,
            face: item.CoverPicture,
            title: item.ProName,
            content: item.ProContent,
            Storeid: item.StoreId,
            vendorId: item.VendorId,
            price: item.oPrice,
            pics: item.ProPics
        });
        dataList.push(dataItem);
        return dataItem;
    }
    function LoadProductFinish(data, total) {
        $.ProductTable.setData(dataList);
        parseInt(total) > dataList.length && $.ProductTable.appendRow(moreView);
    }
    function BindVendor(keyword) {
        $.VendorTable.setVisible(true);
        $.ProductTable.setVisible(false);
        url = Alloy.Globals.ApiUrl + "/store.ashx?action=getverdonlist&t=&area=&keyword=" + keyword + "&sort=";
        moreView = UI.Controller("MoreRow", {
            url: url,
            index: 0,
            itemFun: LoadVendorItem,
            finishFun: LoadVendorFinish
        });
        Ti.API.debug(url);
        Ajax.json(url, LoadVendorItem, LoadVendorFinish);
    }
    function LoadVendorItem(item) {
        var dataItem = Alloy.createController("vendorRow", {
            id: item.id,
            face: Alloy.Globals.WebUrl + item.StorePicUrl,
            title: item.StoreName,
            content: ""
        }).getView();
        dataList.push(dataItem);
        return dataItem;
    }
    function LoadVendorFinish() {
        $.VendorTable.setData(dataList);
        parseInt(total) > dataList.length && $.VendorTable.appendRow(moreView);
    }
    function ChangeSearchType(e) {
        searchType = e.index;
        Ti.API.debug(e.index);
    }
    function LoadDefaultKeyword() {
        var url = Alloy.Globals.ApiUrl + "/system.ashx";
        Ajax.json(url, function(item, i) {
            var keyList = item.HotTerm.split(",");
            for (var i in keyList) keyList[i] && $.HotKeyword.add(InitItem(keyList[i]));
        });
    }
    function InitItem(name) {
        var viewStyle = {
            width: Ti.UI.SIZE,
            backgroundImage: "/content_bg.png",
            backgroundFocusedImage: "/catogory_bg_b.png",
            backgroundSelectedImage: "/catogory_bg_b.png",
            height: 30,
            left: "10",
            top: "10"
        };
        var fontStyle = {
            width: 18 * name.length,
            height: 30,
            textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
            left: "10",
            right: "10",
            font: {
                size: 12
            },
            text: name,
            color: "#333333"
        };
        var viewItem = UI.C("view", viewStyle);
        var title = UI.C("label", fontStyle);
        viewItem.add(title);
        return viewItem;
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Search";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win",
        layout: "vertical"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId148 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId148"
    });
    $.__views.TitleBar.add($.__views.__alloyId148);
    Close ? $.__views.__alloyId148.addEventListener("click", Close) : __defers["$.__views.__alloyId148!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId148.add($.__views.btnBack);
    $.__views.__alloyId149 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "搜索信息",
        id: "__alloyId149"
    });
    $.__views.TitleBar.add($.__views.__alloyId149);
    $.__views.__alloyId150 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId150"
    });
    $.__views.win.add($.__views.__alloyId150);
    $.__views.search = Ti.UI.createSearchBar({
        id: "search",
        hintText: "在此输入搜索关键字",
        height: "43",
        top: "0"
    });
    $.__views.__alloyId150.add($.__views.search);
    Search ? $.__views.search.addEventListener("return", Search) : __defers["$.__views.search!return!Search"] = true;
    $.__views.__alloyId151 = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#fff",
        left: "10",
        right: "10",
        top: "10",
        id: "__alloyId151"
    });
    $.__views.win.add($.__views.__alloyId151);
    $.__views.__alloyId152 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        layout: "horizontal",
        id: "__alloyId152"
    });
    $.__views.__alloyId151.add($.__views.__alloyId152);
    $.__views.__alloyId153 = Ti.UI.createLabel({
        font: {
            fontSize: "13"
        },
        text: "请选择搜索分类",
        fontSize: "13",
        width: Ti.UI.SIZE,
        coloc: "#333",
        id: "__alloyId153"
    });
    $.__views.__alloyId152.add($.__views.__alloyId153);
    $.__views.__alloyId154 = Ti.UI.iOS.createTabbedBar({
        labels: [ "商品", "商家" ],
        backgroundColor: "#336699",
        style: Titanium.UI.iPhone.SystemButtonStyle.BAR,
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        left: 10,
        index: 0,
        id: "__alloyId154"
    });
    $.__views.__alloyId152.add($.__views.__alloyId154);
    ChangeSearchType ? $.__views.__alloyId154.addEventListener("click", ChangeSearchType) : __defers["$.__views.__alloyId154!click!ChangeSearchType"] = true;
    $.__views.__alloyId155 = Ti.UI.createView({
        id: "__alloyId155"
    });
    $.__views.__alloyId151.add($.__views.__alloyId155);
    $.__views.HotKeyword = Ti.UI.createView({
        layout: "horizontal",
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        contentWidth: "100%",
        horizontalWrap: true,
        id: "HotKeyword"
    });
    $.__views.__alloyId155.add($.__views.HotKeyword);
    $.__views.VendorTable = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "VendorTable",
        width: Ti.UI.FILL,
        visible: "false"
    });
    $.__views.__alloyId155.add($.__views.VendorTable);
    $.__views.ProductTable = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "ProductTable",
        width: Ti.UI.FILL,
        visible: "false"
    });
    $.__views.__alloyId155.add($.__views.ProductTable);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var searchType = 0;
    var url = "";
    var moreView = null;
    var dataList = new Array();
    LoadDefaultKeyword();
    __defers["$.__views.__alloyId148!click!Close"] && $.__views.__alloyId148.addEventListener("click", Close);
    __defers["$.__views.search!return!Search"] && $.__views.search.addEventListener("return", Search);
    __defers["$.__views.__alloyId154!click!ChangeSearchType"] && $.__views.__alloyId154.addEventListener("click", ChangeSearchType);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;