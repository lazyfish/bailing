function Controller() {
    function GotoDetail() {
        Alloy.createController("storeDetail", {
            id: id,
            name: title
        }).getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vendorStoreRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.vendorStoreRow = Ti.UI.createTableViewRow({
        id: "vendorStoreRow"
    });
    $.__views.vendorStoreRow && $.addTopLevelView($.__views.vendorStoreRow);
    GotoDetail ? $.__views.vendorStoreRow.addEventListener("click", GotoDetail) : __defers["$.__views.vendorStoreRow!click!GotoDetail"] = true;
    $.__views.__alloyId379 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId379"
    });
    $.__views.vendorStoreRow.add($.__views.__alloyId379);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId379.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId380 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        id: "__alloyId380"
    });
    $.__views.row.add($.__views.__alloyId380);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId380.add($.__views.title);
    $.__views.__alloyId381 = Ti.UI.createView({
        height: Ti.UI.SIZE,
        id: "__alloyId381"
    });
    $.__views.__alloyId380.add($.__views.__alloyId381);
    $.__views.__alloyId382 = Ti.UI.createImageView({
        top: "10dp",
        left: "0dp",
        image: "/ic_phone.png",
        width: "10dp",
        height: Ti.UI.SIZE,
        id: "__alloyId382"
    });
    $.__views.__alloyId381.add($.__views.__alloyId382);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "15dp"
        },
        id: "content",
        left: "15dp"
    });
    $.__views.__alloyId381.add($.__views.content);
    $.__views.__alloyId383 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId383"
    });
    $.__views.__alloyId379.add($.__views.__alloyId383);
    $.__views.__alloyId384 = Ti.UI.createView({
        layout: "horizontal",
        right: "10dp",
        height: Ti.UI.SIZE,
        bottom: "10dp",
        width: Ti.UI.SIZE,
        id: "__alloyId384"
    });
    $.__views.vendorStoreRow.add($.__views.__alloyId384);
    $.__views.__alloyId385 = Ti.UI.createImageView({
        image: "/ic_location3.png",
        left: "0dp",
        width: Ti.UI.SIZE,
        height: "10",
        id: "__alloyId385"
    });
    $.__views.__alloyId384.add($.__views.__alloyId385);
    $.__views.range = Ti.UI.createLabel({
        color: "#333",
        font: {
            fontSize: "13dp"
        },
        id: "range"
    });
    $.__views.__alloyId384.add($.__views.range);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var id = args.id || "";
    var face = args.face || "";
    var title = args.title || "";
    var content = args.content || "";
    var range = args.range || "";
    $.face.setImage(face);
    $.title.setText(title);
    $.content.setText(content);
    $.range.setText(range);
    $.content.addEventListener("click", function(e) {
        Ti.Platform.openURL("tel:" + content);
        e.cancelBubble = true;
    });
    __defers["$.__views.vendorStoreRow!click!GotoDetail"] && $.__views.vendorStoreRow.addEventListener("click", GotoDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;