function Controller() {
    function ChangeMainTab() {
        ChangeTab(0);
    }
    function ChangeActivityTab() {
        ChangeTab(1);
    }
    function ChangeStoreTab() {
        ChangeTab(2);
    }
    function ChangePeopleTab() {
        "" == Alloy.Globals.LoginToken ? UI.Open("Login") : ChangeTab(3);
    }
    function ChangeMoreTab() {
        ChangeTab(4);
    }
    function InitTab(index) {
        for (var i in tabs) if (index != i) {
            tabs[i].setColor("#9D9E9F");
            tabIcons[i].setBackgroundImage(tabJson[i].icon);
        }
    }
    function ChangeTab(index) {
        if (null == views[index]) {
            views[index] = UI.Controller(tabJson[index].controller, {
                callback: tabJson[index].callbackEvent
            });
            $.MainView.add(views[index]);
        }
        views[index].top = 0;
        views[index].left = 0;
        $.lblTitle.setText(tabTitle[index]);
        tabs[index].setColor("#CC0000");
        tabIcons[index].setBackgroundImage(tabJson[index].hoverIcon);
        InitTab(index);
        for (var i in views) views[i] && views[i].setVisible(i == index);
    }
    function Search() {
        UI.Open("Search");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.index = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        exitOnClose: "true",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.IndexTitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#fff",
        height: "40dp",
        Layout: "horizontal",
        id: "IndexTitleBar"
    });
    $.__views.index.add($.__views.IndexTitleBar);
    $.__views.__alloyId219 = Ti.UI.createImageView({
        image: "/ic_location2.png",
        left: "10dp",
        width: Ti.UI.SIZE,
        height: "25dp",
        id: "__alloyId219"
    });
    $.__views.IndexTitleBar.add($.__views.__alloyId219);
    $.__views.lblTitle = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "泉州",
        id: "lblTitle",
        left: "40dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT
    });
    $.__views.IndexTitleBar.add($.__views.lblTitle);
    $.__views.__alloyId220 = Ti.UI.createImageView({
        image: "/ic_titlebtn_search.png",
        right: "10dp",
        width: "30dp",
        height: "30dp",
        id: "__alloyId220"
    });
    $.__views.IndexTitleBar.add($.__views.__alloyId220);
    Search ? $.__views.__alloyId220.addEventListener("click", Search) : __defers["$.__views.__alloyId220!click!Search"] = true;
    $.__views.MainView = Ti.UI.createView({
        top: "40dp",
        bottom: "50dp",
        id: "MainView"
    });
    $.__views.index.add($.__views.MainView);
    $.__views.TabGroup = Ti.UI.createView({
        backgroundImage: "/tag_bg.png",
        height: "50dp",
        bottom: "0dp",
        layout: "horizontal",
        id: "TabGroup"
    });
    $.__views.index.add($.__views.TabGroup);
    $.__views.__alloyId221 = Ti.UI.createView({
        width: "20%",
        layout: "vertical",
        focusable: "true",
        id: "__alloyId221"
    });
    $.__views.TabGroup.add($.__views.__alloyId221);
    ChangeMainTab ? $.__views.__alloyId221.addEventListener("click", ChangeMainTab) : __defers["$.__views.__alloyId221!click!ChangeMainTab"] = true;
    $.__views.HomeIcon = Ti.UI.createImageView({
        top: "5dp",
        width: "25dp",
        height: "25dp",
        backgroundImage: "/tag_home.png",
        backgroundFocusedImage: "/tag_home.png",
        backgroundSelectedImage: "/tag_home_pressed.png",
        id: "HomeIcon"
    });
    $.__views.__alloyId221.add($.__views.HomeIcon);
    $.__views.HomeView = Ti.UI.createLabel({
        color: "#CC0000",
        font: {
            fontSize: "12dp"
        },
        text: "首页",
        id: "HomeView"
    });
    $.__views.__alloyId221.add($.__views.HomeView);
    $.__views.__alloyId222 = Ti.UI.createView({
        width: "20%",
        layout: "vertical",
        id: "__alloyId222"
    });
    $.__views.TabGroup.add($.__views.__alloyId222);
    ChangeActivityTab ? $.__views.__alloyId222.addEventListener("click", ChangeActivityTab) : __defers["$.__views.__alloyId222!click!ChangeActivityTab"] = true;
    $.__views.ActivityIcon = Ti.UI.createImageView({
        top: "5dp",
        width: "25dp",
        height: "25dp",
        backgroundImage: "/tag_group.png",
        backgroundFocusedImage: "/tag_group.png",
        backgroundSelectedImage: "/tag_group_pressed.png",
        id: "ActivityIcon"
    });
    $.__views.__alloyId222.add($.__views.ActivityIcon);
    $.__views.ActivityView = Ti.UI.createLabel({
        color: "#9D9E9F",
        font: {
            fontSize: "12dp"
        },
        text: "活动",
        id: "ActivityView"
    });
    $.__views.__alloyId222.add($.__views.ActivityView);
    $.__views.__alloyId223 = Ti.UI.createView({
        width: "20%",
        layout: "vertical",
        id: "__alloyId223"
    });
    $.__views.TabGroup.add($.__views.__alloyId223);
    ChangeStoreTab ? $.__views.__alloyId223.addEventListener("click", ChangeStoreTab) : __defers["$.__views.__alloyId223!click!ChangeStoreTab"] = true;
    $.__views.StoreIcon = Ti.UI.createImageView({
        top: "5dp",
        width: "25dp",
        height: "25dp",
        backgroundImage: "/tag_store.png",
        backgroundFocusedImage: "/tag_store.png",
        backgroundSelectedImage: "/tag_store_pressed.png",
        id: "StoreIcon"
    });
    $.__views.__alloyId223.add($.__views.StoreIcon);
    $.__views.StoreView = Ti.UI.createLabel({
        color: "#9D9E9F",
        font: {
            fontSize: "12dp"
        },
        text: "周边店",
        id: "StoreView"
    });
    $.__views.__alloyId223.add($.__views.StoreView);
    $.__views.__alloyId224 = Ti.UI.createView({
        width: "20%",
        layout: "vertical",
        id: "__alloyId224"
    });
    $.__views.TabGroup.add($.__views.__alloyId224);
    ChangePeopleTab ? $.__views.__alloyId224.addEventListener("click", ChangePeopleTab) : __defers["$.__views.__alloyId224!click!ChangePeopleTab"] = true;
    $.__views.PeopleIcon = Ti.UI.createImageView({
        top: "5dp",
        width: "25dp",
        height: "25dp",
        backgroundImage: "/tag_people.png",
        backgroundFocusedImage: "/tag_people.png",
        backgroundSelectedImage: "/tag_people_pressed.png",
        id: "PeopleIcon"
    });
    $.__views.__alloyId224.add($.__views.PeopleIcon);
    $.__views.PeopleView = Ti.UI.createLabel({
        color: "#9D9E9F",
        font: {
            fontSize: "12dp"
        },
        text: "我的",
        id: "PeopleView"
    });
    $.__views.__alloyId224.add($.__views.PeopleView);
    $.__views.__alloyId225 = Ti.UI.createView({
        width: "20%",
        layout: "vertical",
        id: "__alloyId225"
    });
    $.__views.TabGroup.add($.__views.__alloyId225);
    ChangeMoreTab ? $.__views.__alloyId225.addEventListener("click", ChangeMoreTab) : __defers["$.__views.__alloyId225!click!ChangeMoreTab"] = true;
    $.__views.MoreIcon = Ti.UI.createImageView({
        top: "5dp",
        width: "25dp",
        height: "25dp",
        backgroundImage: "/tag_more.png",
        backgroundFocusedImage: "/tag_more.png",
        backgroundSelectedImage: "/tag_more_pressed.png",
        id: "MoreIcon"
    });
    $.__views.__alloyId225.add($.__views.MoreIcon);
    $.__views.MoreView = Ti.UI.createLabel({
        color: "#9D9E9F",
        font: {
            fontSize: "12dp"
        },
        text: "更多",
        id: "MoreView"
    });
    $.__views.__alloyId225.add($.__views.MoreView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var users = Alloy.createCollection("UserModel");
    users.fetch();
    users.length > 0 && (Alloy.Globals.LoginToken = users.at(0).get("Token"));
    Ti.API.debug("LoginId:" + Alloy.Globals.LoginToken);
    $.index.open();
    var mainTab = {
        icon: "/tag_home.png",
        hoverIcon: "/tag_home_pressed.png",
        controller: "main",
        callbackEvent: null
    };
    var activityTab = {
        icon: "/tag_group.png",
        hoverIcon: "/tag_group_pressed.png",
        controller: "Activity",
        callbackEvent: null
    };
    var storeTab = {
        icon: "/tag_store.png",
        hoverIcon: "/tag_store_pressed.png",
        controller: "store",
        callbackEvent: null
    };
    var peopleTab = {
        icon: "/tag_people.png",
        hoverIcon: "/tag_people_pressed.png",
        controller: "self",
        callbackEvent: null
    };
    var moreTab = {
        icon: "/tag_more.png",
        hoverIcon: "/tag_more_pressed.png",
        controller: "more",
        callbackEvent: null
    };
    var tabJson = new Array(mainTab, activityTab, storeTab, peopleTab, moreTab);
    var tabs = [ $.HomeView, $.ActivityView, $.StoreView, $.PeopleView, $.MoreView ];
    var tabIcons = [ $.HomeIcon, $.ActivityIcon, $.StoreIcon, $.PeopleIcon, $.MoreIcon ];
    var tabTitle = [ "泉州", "活动信息", "周边店", "个人信息", "更多" ];
    var mainView = null;
    var storeView = null;
    var peopleView = null;
    var moreView = null;
    var views = [ mainView, storeView, peopleView, moreView ];
    views[0] = UI.Controller(tabJson[0].controller, {
        callback: tabJson[0].callbackEvent
    });
    $.MainView.add(views[0]);
    $.HomeIcon.setBackgroundImage(mainTab.hoverIcon);
    __defers["$.__views.__alloyId220!click!Search"] && $.__views.__alloyId220.addEventListener("click", Search);
    __defers["$.__views.__alloyId221!click!ChangeMainTab"] && $.__views.__alloyId221.addEventListener("click", ChangeMainTab);
    __defers["$.__views.__alloyId222!click!ChangeActivityTab"] && $.__views.__alloyId222.addEventListener("click", ChangeActivityTab);
    __defers["$.__views.__alloyId223!click!ChangeStoreTab"] && $.__views.__alloyId223.addEventListener("click", ChangeStoreTab);
    __defers["$.__views.__alloyId224!click!ChangePeopleTab"] && $.__views.__alloyId224.addEventListener("click", ChangePeopleTab);
    __defers["$.__views.__alloyId225!click!ChangeMoreTab"] && $.__views.__alloyId225.addEventListener("click", ChangeMoreTab);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;