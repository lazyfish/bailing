function Controller() {
    function Init() {
        LoadIndustryType();
        LoadArea();
        BindTable();
    }
    function LoadIndustryType() {
        typeDialog = new ListDialog.option();
        typeDialog.Init($.win, {
            left: "5dp",
            top: "30dp"
        }, {
            width: "300dp",
            height: "240dp"
        }, [], []);
        typeDialog.OnRootClick = function(e) {
            if (0 == e.id) {
                type = e.id;
                BindTable();
                typeDialog.Hide();
            } else Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + e.id, LoadingIndustryChildItem, LoadedIndustryChildItem);
        };
        typeDialog.OnChildClick = function(e) {
            type = e.id;
            BindTable();
            typeDialog.Hide();
        };
        Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=0", LoadingIndustryRootItem, LoadedIndustryRootItem);
    }
    function LoadingIndustryRootItem(item) {
        return {
            id: item.id,
            name: item.ItemName
        };
    }
    function LoadedIndustryRootItem(data) {
        data.unshift({
            id: "0",
            name: "全部"
        });
        typeDialog.BindData(data);
        data.length > 1 && Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + data[1].id, LoadingIndustryChildItem, LoadedIndustryChildItem);
    }
    function LoadingIndustryChildItem(item) {
        return {
            id: item.id,
            name: item.ItemName,
            parent: item.ParentId
        };
    }
    function LoadedIndustryChildItem(data) {
        data.unshift({
            id: typeDialog._rootId,
            name: "全部"
        });
        typeDialog.BindData(null, data);
    }
    function LoadArea() {
        areaDialog = new ListDialog.option();
        left = Alloy.Globals.width / 2;
        areaDialog.Init($.win, {
            left: left,
            top: "30dp"
        }, {
            width: "140dp",
            height: "240dp"
        }, []);
        Ajax.json(Alloy.Globals.ApiUrl + "/ct.ashx?action=getarea&code=350500", function(item) {
            return {
                id: item.CTCode,
                name: item.CTName
            };
        }, function(data) {
            data.unshift({
                id: "350500",
                name: "全城"
            });
            areaDialog.BindData(data);
        });
        areaDialog.OnRootClick = function(e) {
            area = e.id;
            BindTable();
            areaDialog.Hide();
        };
    }
    function BindTable() {
        Titanium.Geolocation.addEventListener("location", function(e) {
            hasLoad || GetTable(e.coords);
        });
        Ti.Geolocation.getCurrentPosition(function(e) {
            e.success && (hasLoad || GetTable(e.coords));
        });
    }
    function GetTable(coords) {
        hasLoad = true;
        url = Alloy.Globals.ApiUrl + "/store.ashx?action=getstorelist&lng=" + coords.longitude + "&lat=" + coords.latitude;
        type && (url += "&t=" + type);
        area && (url += "&area=" + area);
        Ti.API.info(url);
        moreView || (moreView = UI.Controller("MoreRow", {
            url: url,
            index: 0,
            itemFun: CreateStoreItem,
            finishFun: FinishLoadStore
        }));
        Ajax.json(url, CreateStoreItem, FinishLoadStore);
    }
    function CreateStoreItem(item) {
        var dataItem = Alloy.createController("vendorStoreRow", {
            face: Alloy.Globals.WebUrl + item.StorePicUrl,
            title: item.StoreName,
            id: item.id,
            content: item.ContactTel,
            range: item.Range + "米"
        }).getView();
        dataList.push(dataItem);
        return dataItem;
    }
    function FinishLoadStore(data, total) {
        $.table.setData(dataList);
        parseInt(total) > dataList.length && $.table.appendRow(moreView);
        actInd.hide();
    }
    function ShowTypeWin() {
        typeDialog.Show();
        $.AreaTab.setBackgroundImage("");
        $.AreaTab.setColor("#84868D");
        $.TypeTab.setBackgroundImage("/tool_left.png");
        $.TypeTab.setColor("#fff");
    }
    function ShowAreaWin() {
        areaDialog.Show();
        $.AreaTab.setBackgroundImage("/tool_right.png");
        $.AreaTab.setColor("#fff");
        $.TypeTab.setBackgroundImage("");
        $.TypeTab.setColor("#84868D");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "store";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createView({
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.body = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#fff",
        id: "body"
    });
    $.__views.win.add($.__views.body);
    $.__views.__alloyId331 = Ti.UI.createView({
        height: "45dp",
        backgroundImage: "/tag_bg.png",
        width: Ti.UI.FILL,
        id: "__alloyId331"
    });
    $.__views.body.add($.__views.__alloyId331);
    $.__views.__alloyId332 = Ti.UI.createView({
        top: "5dp",
        height: "35dp",
        backgroundImage: "/store_type_bg_a.png",
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId332"
    });
    $.__views.__alloyId331.add($.__views.__alloyId332);
    $.__views.__alloyId333 = Ti.UI.createView({
        top: "5dp",
        right: 5,
        left: 5,
        height: "25dp",
        backgroundImage: "/store_type_bg_b.png",
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId333"
    });
    $.__views.__alloyId332.add($.__views.__alloyId333);
    $.__views.TypeTab = Ti.UI.createLabel({
        width: 100,
        font: {
            fontSize: 14
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#fff",
        backgroundImage: "/tool_left.png",
        text: "类别",
        id: "TypeTab"
    });
    $.__views.__alloyId333.add($.__views.TypeTab);
    ShowTypeWin ? $.__views.TypeTab.addEventListener("click", ShowTypeWin) : __defers["$.__views.TypeTab!click!ShowTypeWin"] = true;
    $.__views.AreaTab = Ti.UI.createLabel({
        width: 100,
        font: {
            fontSize: "14dp"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#84868D",
        text: "地区",
        id: "AreaTab"
    });
    $.__views.__alloyId333.add($.__views.AreaTab);
    ShowAreaWin ? $.__views.AreaTab.addEventListener("click", ShowAreaWin) : __defers["$.__views.AreaTab!click!ShowAreaWin"] = true;
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox",
        bottom: "30"
    });
    $.__views.body.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var ListDialog = require("ListDialog");
    var typeDialog;
    var areaDialog;
    var type = "";
    var area = "";
    var url = "";
    var moreView = null;
    Init();
    var actInd = Titanium.UI.createActivityIndicator({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#fff",
        message: "获取位置中..."
    });
    $.win.add(actInd);
    actInd.show();
    Ti.Geolocation.purpose = "Get Current Location";
    Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
    if ("ios" == Alloy.Globals.osname) if (Ti.Geolocation.locationServicesEnabled) {
        Ti.Geolocation.distanceFilter = 10;
        Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_NETWORK;
    } else alert("请启用您的GPS定位服务"); else if ("android" == Alloy.Globals.osname) {
        var providerGps = Ti.Geolocation.Android.createLocationProvider({
            name: Ti.Geolocation.PROVIDER_NETWORK,
            minUpdateDistance: 0,
            minUpdateTime: 0
        });
        Ti.Geolocation.Android.addLocationProvider(providerGps);
        Ti.Geolocation.Android.manualMode = true;
    }
    var hasLoad = false;
    var dataList = new Array();
    __defers["$.__views.TypeTab!click!ShowTypeWin"] && $.__views.TypeTab.addEventListener("click", ShowTypeWin);
    __defers["$.__views.AreaTab!click!ShowAreaWin"] && $.__views.AreaTab.addEventListener("click", ShowAreaWin);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;