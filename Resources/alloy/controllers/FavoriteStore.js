function Controller() {
    function Close() {
        $.win.close();
    }
    function RemoveRow(e) {
        $.table.deleteRow(e.index);
    }
    function LoadItem(item) {
        var dataItem = UI.Controller("FavoriteStoreRow", {
            face: Alloy.Globals.WebUrl + item.StorePicUrl,
            title: item.StoreName,
            id: item.id,
            storeId: item.storeId,
            content: item.ContactTel,
            delFun: RemoveRow
        });
        dataList.push(dataItem);
        return dataItem;
    }
    function LoadFinish(data, total) {
        $.table.setData(dataList);
        parseInt(total) > dataList.length && $.table.appendRow(moreView);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "FavoriteStore";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#404141",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId60 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId60"
    });
    $.__views.TitleBar.add($.__views.__alloyId60);
    Close ? $.__views.__alloyId60.addEventListener("click", Close) : __defers["$.__views.__alloyId60!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId60.add($.__views.btnBack);
    $.__views.__alloyId61 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "收藏的门店",
        id: "__alloyId61"
    });
    $.__views.TitleBar.add($.__views.__alloyId61);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox",
        bottom: "40dp"
    });
    $.__views.MainWin.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#404141",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Ajax = require("Ajax");
    require("Dialog");
    arguments[0] || {};
    var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=list&idguid=" + Alloy.Globals.LoginToken + "&t=2";
    var moreView = null;
    var dataList = new Array();
    Ti.API.info(url);
    moreView || (moreView = UI.Controller("MoreRow", {
        url: url,
        index: 0,
        itemFun: LoadItem,
        finishFun: LoadFinish
    }));
    Ajax.json(url, LoadItem, LoadFinish);
    __defers["$.__views.__alloyId60!click!Close"] && $.__views.__alloyId60.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;