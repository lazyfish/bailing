function Controller() {
    function Close() {
        $.win.close();
    }
    function Submit() {
        var url = Alloy.Globals.ApiUrl + "/comment.ashx?action=add&idguid=" + Alloy.Globals.LoginToken;
        var param = {
            content: $.txtContent.value,
            proid: pid,
            storeid: storeid
        };
        Ajax.post(url, param, function() {
            var prompt = new Dialog.prompt();
            prompt.Show("评论成功！", 2e3);
            setTimeout(function() {
                $.win.close();
            }, 1500);
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "CommentSubmit";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#404141",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId36 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId36"
    });
    $.__views.TitleBar.add($.__views.__alloyId36);
    Close ? $.__views.__alloyId36.addEventListener("click", Close) : __defers["$.__views.__alloyId36!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId36.add($.__views.btnBack);
    $.__views.Title = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "提交评价",
        id: "Title"
    });
    $.__views.TitleBar.add($.__views.Title);
    $.__views.__alloyId37 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        right: "0dp",
        id: "__alloyId37"
    });
    $.__views.TitleBar.add($.__views.__alloyId37);
    Submit ? $.__views.__alloyId37.addEventListener("click", Submit) : __defers["$.__views.__alloyId37!click!Submit"] = true;
    $.__views.btnOk = Ti.UI.createImageView({
        image: "/ok.png",
        width: "25dp",
        right: "5dp",
        height: "25dp",
        id: "btnOk"
    });
    $.__views.__alloyId37.add($.__views.btnOk);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.__alloyId38 = Ti.UI.createView({
        layout: "horizontal",
        left: "10dp",
        right: "10dp",
        id: "__alloyId38"
    });
    $.__views.MainWin.add($.__views.__alloyId38);
    $.__views.__alloyId39 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "14dp"
        },
        text: "评价星级",
        left: "10dp",
        id: "__alloyId39"
    });
    $.__views.__alloyId38.add($.__views.__alloyId39);
    $.__views.__alloyId40 = Ti.UI.createImageView({
        left: "5dp",
        image: "/star_off.png",
        width: "35dp",
        height: Ti.UI.SIZE,
        id: "__alloyId40"
    });
    $.__views.__alloyId38.add($.__views.__alloyId40);
    $.__views.__alloyId41 = Ti.UI.createImageView({
        left: "5dp",
        image: "/star_off.png",
        width: "35dp",
        height: Ti.UI.SIZE,
        id: "__alloyId41"
    });
    $.__views.__alloyId38.add($.__views.__alloyId41);
    $.__views.__alloyId42 = Ti.UI.createImageView({
        left: "5dp",
        image: "/star_off.png",
        width: "35dp",
        height: Ti.UI.SIZE,
        id: "__alloyId42"
    });
    $.__views.__alloyId38.add($.__views.__alloyId42);
    $.__views.__alloyId43 = Ti.UI.createImageView({
        left: "5dp",
        image: "/star_off.png",
        width: "35dp",
        height: Ti.UI.SIZE,
        id: "__alloyId43"
    });
    $.__views.__alloyId38.add($.__views.__alloyId43);
    $.__views.__alloyId44 = Ti.UI.createImageView({
        left: "5dp",
        image: "/star_off.png",
        width: "35dp",
        height: Ti.UI.SIZE,
        id: "__alloyId44"
    });
    $.__views.__alloyId38.add($.__views.__alloyId44);
    $.__views.txtContent = Ti.UI.createTextArea({
        editable: true,
        hintText: "在此输入您对产品的评价",
        accessibilityHint: "在此输入您对产品的评价",
        height: 70,
        width: 300,
        top: 60,
        font: {
            fontSize: "15dp"
        },
        color: "#888",
        textAlign: "left",
        borderWidth: "1dp",
        borderColor: "#fff",
        borderRadius: 5,
        suppressReturn: false,
        id: "txtContent"
    });
    $.__views.MainWin.add($.__views.txtContent);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var Dialog = require("Dialog");
    var args = arguments[0] || {};
    var pid = args.pid || "";
    var storeid = args.storeid || "";
    __defers["$.__views.__alloyId36!click!Close"] && $.__views.__alloyId36.addEventListener("click", Close);
    __defers["$.__views.__alloyId37!click!Submit"] && $.__views.__alloyId37.addEventListener("click", Submit);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;