function Controller() {
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "storeLocation";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId346 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId346"
    });
    $.__views.TitleBar.add($.__views.__alloyId346);
    Close ? $.__views.__alloyId346.addEventListener("click", Close) : __defers["$.__views.__alloyId346!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId346.add($.__views.btnBack);
    $.__views.__alloyId347 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "地图",
        id: "__alloyId347"
    });
    $.__views.TitleBar.add($.__views.__alloyId347);
    $.__views.MapView = Ti.UI.createWebView({
        id: "MapView",
        top: "40dp"
    });
    $.__views.win.add($.__views.MapView);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var longitude = args.longitude;
    var latitude = args.latitude;
    Ti.Geolocation.purpose = "Get Current Location";
    Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
    if (Ti.Geolocation.locationServicesEnabled) {
        Ti.Geolocation.distanceFilter = 10;
        Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_NETWORK;
    } else alert("Please enable location services");
    Ti.Geolocation.getCurrentPosition(function(e) {
        var url = Alloy.Globals.WebUrl + "/mobile/map.aspx";
        e.success && (url += "?long=" + e.coords.longitude + "&lat=" + e.coords.latitude + "&tolong=" + longitude + "&tolat=" + latitude);
        Ti.API.info(url);
        $.MapView.url = url;
    });
    $.MapView.addEventListener("beforeload", function() {});
    __defers["$.__views.__alloyId346!click!Close"] && $.__views.__alloyId346.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;