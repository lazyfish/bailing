function Controller() {
    function GotoDetail() {
        UI.Open("ArticleDetail", {
            id: id,
            title: title,
            content: content,
            time: time
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "ArticleRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.ArticleRow = Ti.UI.createTableViewRow({
        id: "ArticleRow"
    });
    $.__views.ArticleRow && $.addTopLevelView($.__views.ArticleRow);
    GotoDetail ? $.__views.ArticleRow.addEventListener("click", GotoDetail) : __defers["$.__views.ArticleRow!click!GotoDetail"] = true;
    $.__views.__alloyId3 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId3"
    });
    $.__views.ArticleRow.add($.__views.__alloyId3);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        width: Ti.UI.FILL,
        left: "10dp",
        top: "10dp",
        layout: "vertical",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId3.add($.__views.row);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.row.add($.__views.title);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "13dp"
        },
        id: "content"
    });
    $.__views.row.add($.__views.content);
    $.__views.__alloyId4 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId4"
    });
    $.__views.__alloyId3.add($.__views.__alloyId4);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var DateUtility = require("DateUtility");
    var args = arguments[0] || {};
    var id = args.id || "";
    var title = args.title || "";
    var content = args.content || "";
    var time = args.time || "";
    $.title.text = title;
    $.content.text = DateUtility.ToString(time, "yyyy-MM-dd");
    __defers["$.__views.ArticleRow!click!GotoDetail"] && $.__views.ArticleRow.addEventListener("click", GotoDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;