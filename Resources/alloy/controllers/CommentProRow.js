function Controller() {
    function GotoDetail() {
        Alloy.createController("product", {
            id: id,
            face: pics,
            title: title,
            Storeid: storeId,
            content: content
        }).getView().open();
    }
    function Comment(e) {
        e.cancelBubble = true;
        Alloy.createController("CommentSubmit", {
            pid: id,
            storeid: storeId
        }).getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "CommentProRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.CommentProRow = Ti.UI.createTableViewRow({
        id: "CommentProRow"
    });
    $.__views.CommentProRow && $.addTopLevelView($.__views.CommentProRow);
    GotoDetail ? $.__views.CommentProRow.addEventListener("click", GotoDetail) : __defers["$.__views.CommentProRow!click!GotoDetail"] = true;
    $.__views.__alloyId31 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId31"
    });
    $.__views.CommentProRow.add($.__views.__alloyId31);
    $.__views.row = Ti.UI.createView({
        backgroundFocusedColor: "#ADCFE6",
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId31.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        image: "http://placehold.it/50x50",
        width: "100dp",
        left: "0dp",
        top: "10dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderColor: "#fff",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId32 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        right: "30dp",
        height: Ti.UI.SIZE,
        id: "__alloyId32"
    });
    $.__views.row.add($.__views.__alloyId32);
    $.__views.title = Ti.UI.createLabel({
        color: "#fff",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId32.add($.__views.title);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#eee",
        font: {
            fontSize: "13dp"
        },
        id: "content"
    });
    $.__views.__alloyId32.add($.__views.content);
    $.__views.__alloyId33 = Ti.UI.createView({
        right: "0",
        height: Ti.UI.SIZE,
        width: "30dp",
        id: "__alloyId33"
    });
    $.__views.row.add($.__views.__alloyId33);
    Comment ? $.__views.__alloyId33.addEventListener("click", Comment) : __defers["$.__views.__alloyId33!click!Comment"] = true;
    $.__views.btnComment = Ti.UI.createImageView({
        image: "/ic_comment.png",
        width: "35dp",
        height: Ti.UI.SIZE,
        id: "btnComment"
    });
    $.__views.__alloyId33.add($.__views.btnComment);
    $.__views.__alloyId34 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#343434",
        id: "__alloyId34"
    });
    $.__views.__alloyId31.add($.__views.__alloyId34);
    $.__views.__alloyId35 = Ti.UI.createView({
        height: "1dp",
        bottom: "1dp",
        backgroundColor: "#4F5050",
        id: "__alloyId35"
    });
    $.__views.__alloyId31.add($.__views.__alloyId35);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var id = args.id || "";
    var face = args.face || "";
    var title = args.title || "";
    var content = args.content || "";
    var price = args.price || "";
    var pics = args.pics || "";
    var storeId = args.storeId || "";
    Ti.API.info(id);
    $.face.setImage(Alloy.Globals.WebUrl + face);
    $.title.setText(title);
    $.content.setText(String.format("会员价：%.2f元", parseFloat(price)));
    __defers["$.__views.CommentProRow!click!GotoDetail"] && $.__views.CommentProRow.addEventListener("click", GotoDetail);
    __defers["$.__views.__alloyId33!click!Comment"] && $.__views.__alloyId33.addEventListener("click", Comment);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;