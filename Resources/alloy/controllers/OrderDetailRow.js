function Controller() {
    function GotoProduct() {
        UI.Open("product", {
            id: pid
        });
    }
    function Repeal(e) {
        var url = Alloy.Globals.ApiUrl + "/order.ashx?action=repeal&id=" + id;
        Ajax.get(url, function(data) {
            if ("ok" == data) {
                var prompt = new Dialog.prompt();
                prompt.Show("申请成功！", 2e3);
                $.price.setText(String.format("购买价:%1$.2f元 \r\n 状态：%2$s \r\n 编号：%3$s \r\n 验证码：%4$s", parseFloat(price), "申请退款", number, password));
            } else alert("此订单无法申请退款");
            $.btnRepeal.setVisible(false);
        });
        e.cancelBubble = true;
        return false;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "OrderDetailRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.OrderDetailRow = Ti.UI.createTableViewRow({
        id: "OrderDetailRow"
    });
    $.__views.OrderDetailRow && $.addTopLevelView($.__views.OrderDetailRow);
    GotoProduct ? $.__views.OrderDetailRow.addEventListener("click", GotoProduct) : __defers["$.__views.OrderDetailRow!click!GotoProduct"] = true;
    $.__views.__alloyId107 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId107"
    });
    $.__views.OrderDetailRow.add($.__views.__alloyId107);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row",
        bottom: "10dp"
    });
    $.__views.__alloyId107.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId108 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        right: "30dp",
        height: Ti.UI.SIZE,
        id: "__alloyId108"
    });
    $.__views.row.add($.__views.__alloyId108);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId108.add($.__views.title);
    $.__views.price = Ti.UI.createLabel({
        font: {
            fontSize: "13dp"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        top: "5dp",
        width: Ti.UI.FILL,
        id: "price"
    });
    $.__views.__alloyId108.add($.__views.price);
    $.__views.btnRepeal = Ti.UI.createView({
        backgroundImage: "/btn2_bg.png",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: Ti.UI.SIZE,
        id: "btnRepeal"
    });
    $.__views.__alloyId107.add($.__views.btnRepeal);
    Repeal ? $.__views.btnRepeal.addEventListener("click", Repeal) : __defers["$.__views.btnRepeal!click!Repeal"] = true;
    $.__views.__alloyId109 = Ti.UI.createLabel({
        color: "#333",
        font: {
            fontSize: 12
        },
        height: Ti.UI.SIZE,
        top: "5",
        bottom: "5",
        text: "申请退款",
        id: "__alloyId109"
    });
    $.__views.btnRepeal.add($.__views.__alloyId109);
    $.__views.__alloyId110 = Ti.UI.createView({
        height: "1dp",
        backgroundColor: "#999",
        id: "__alloyId110"
    });
    $.__views.__alloyId107.add($.__views.__alloyId110);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Ajax = require("Ajax");
    var Dialog = require("Dialog");
    var args = arguments[0] || {};
    var id = args.id;
    var pid = args.pid;
    args.num;
    var price = args.price;
    var name = args.name;
    var face = args.face;
    var state = args.state;
    var password = args.password;
    var OrderState = args.OrderState || "";
    var stateName = "未使用";
    $.btnRepeal.setVisible(false);
    if (1 != OrderState) switch (parseInt(state)) {
      case 0:
        stateName = "未使用";
        $.btnRepeal.setVisible(true);
        break;

      case 1:
        stateName = "已使用";
        break;

      case 2:
        stateName = "申请退款";
        break;

      case 3:
        stateName = "拒绝退款";
        break;

      case 4:
        stateName = "退款成功";
    }
    var number = id;
    while (8 > number.length) number = "0" + number;
    $.face.setImage(Alloy.Globals.WebUrl + face);
    $.title.setText(name);
    $.price.setText(String.format("购买价:%1$.2f元 \r\n 状态：%2$s \r\n 编号：%3$s \r\n 验证码：%4$s", parseFloat(price), stateName, number, password));
    __defers["$.__views.OrderDetailRow!click!GotoProduct"] && $.__views.OrderDetailRow.addEventListener("click", GotoProduct);
    __defers["$.__views.btnRepeal!click!Repeal"] && $.__views.btnRepeal.addEventListener("click", Repeal);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;