function Controller() {
    function Modify() {
        if (!/^(((\(\d{2,3}\))|(\d{3}\-))?13\d{9})|(((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$)$/.test($.tfPhone.value)) {
            alert("请输入正确的联系电话");
            return false;
        }
        if (!/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test($.tfEmail.value)) {
            alert("请输入正确的邮箱地址");
            return false;
        }
        var param = {
            idguid: Alloy.Globals.LoginToken,
            RealName: $.tfName.value,
            Email: $.tfEmail.value,
            Tel: $.tfPhone.value
        };
        Ti.API.debug("data", JSON.stringify(param));
        Ajax.post(Alloy.Globals.ApiUrl + "/user.ashx?action=update", param, function(data) {
            if ("exist user" == data) alert("此用户名已存在，请换个用户名吧。"); else {
                var prompt = new Dialog.prompt();
                prompt.Show("修改成功！", 2e3);
                Close();
            }
        });
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "SelfModify";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId156 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId156"
    });
    $.__views.TitleBar.add($.__views.__alloyId156);
    Close ? $.__views.__alloyId156.addEventListener("click", Close) : __defers["$.__views.__alloyId156!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId156.add($.__views.btnBack);
    $.__views.__alloyId157 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "基本信息修改",
        id: "__alloyId157"
    });
    $.__views.TitleBar.add($.__views.__alloyId157);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin",
        left: "10",
        right: "10",
        bottom: "10",
        layout: "vertical"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.__alloyId158 = Ti.UI.createLabel({
        left: 10,
        font: {
            fontSize: 14
        },
        height: 30,
        color: "#333",
        text: "昵称",
        id: "__alloyId158"
    });
    $.__views.MainWin.add($.__views.__alloyId158);
    $.__views.tfName = Ti.UI.createTextField({
        left: 0,
        font: {
            fontSize: 14
        },
        keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
        returnKeyType: Titanium.UI.RETURNKEY_DEFAULT,
        borderStyle: Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
        width: "90%",
        id: "tfName",
        hintText: "在此输入昵称"
    });
    $.__views.MainWin.add($.__views.tfName);
    $.__views.__alloyId159 = Ti.UI.createLabel({
        left: 10,
        font: {
            fontSize: 14
        },
        height: 30,
        color: "#333",
        text: "联系电话",
        id: "__alloyId159"
    });
    $.__views.MainWin.add($.__views.__alloyId159);
    $.__views.tfPhone = Ti.UI.createTextField({
        left: 0,
        font: {
            fontSize: 14
        },
        keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
        returnKeyType: Titanium.UI.RETURNKEY_DEFAULT,
        borderStyle: Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
        width: "90%",
        id: "tfPhone",
        hintText: "在此输入联系电话"
    });
    $.__views.MainWin.add($.__views.tfPhone);
    $.__views.__alloyId160 = Ti.UI.createLabel({
        left: 10,
        font: {
            fontSize: 14
        },
        height: 30,
        color: "#333",
        text: "常用邮箱",
        id: "__alloyId160"
    });
    $.__views.MainWin.add($.__views.__alloyId160);
    $.__views.tfEmail = Ti.UI.createTextField({
        left: 0,
        font: {
            fontSize: 14
        },
        keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
        returnKeyType: Titanium.UI.RETURNKEY_DEFAULT,
        borderStyle: Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
        width: "90%",
        id: "tfEmail",
        hintText: "在此输入常用邮箱"
    });
    $.__views.MainWin.add($.__views.tfEmail);
    $.__views.__alloyId161 = Ti.UI.createView({
        height: "35dp",
        width: "90%",
        top: "10",
        id: "__alloyId161"
    });
    $.__views.MainWin.add($.__views.__alloyId161);
    Modify ? $.__views.__alloyId161.addEventListener("click", Modify) : __defers["$.__views.__alloyId161!click!Modify"] = true;
    $.__views.__alloyId162 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId162"
    });
    $.__views.__alloyId161.add($.__views.__alloyId162);
    $.__views.__alloyId163 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId163"
    });
    $.__views.__alloyId161.add($.__views.__alloyId163);
    $.__views.__alloyId164 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "提交信息",
        id: "__alloyId164"
    });
    $.__views.__alloyId163.add($.__views.__alloyId164);
    $.__views.__alloyId165 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId165"
    });
    $.__views.__alloyId161.add($.__views.__alloyId165);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var Dialog = require("Dialog");
    Ajax.json(Alloy.Globals.ApiUrl + "/user.ashx?action=getuserbyguid&IdGUID=" + Alloy.Globals.LoginToken, function(item) {
        $.tfName.value = item.RealName;
        $.tfPhone.value = item.Tel;
        $.tfEmail.value = item.Email;
    });
    __defers["$.__views.__alloyId156!click!Close"] && $.__views.__alloyId156.addEventListener("click", Close);
    __defers["$.__views.__alloyId161!click!Modify"] && $.__views.__alloyId161.addEventListener("click", Modify);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;