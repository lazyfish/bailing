function Controller() {
    function GotoDetail() {
        Alloy.createController("product", {
            id: id,
            face: pics,
            title: title,
            StoreId: storeId,
            content: content
        }).getView().open();
    }
    function Del(e) {
        var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=del&id=" + id;
        Ajax.get(url, function(data) {
            if ("ok" == data) {
                var prompt = new Dialog.prompt();
                prompt.Show("删除成功！", 2e3);
                delFun(e);
            }
        });
        e.cancelBubble = true;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "FavoriteProRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.FavoriteProRow = Ti.UI.createTableViewRow({
        id: "FavoriteProRow"
    });
    $.__views.FavoriteProRow && $.addTopLevelView($.__views.FavoriteProRow);
    GotoDetail ? $.__views.FavoriteProRow.addEventListener("click", GotoDetail) : __defers["$.__views.FavoriteProRow!click!GotoDetail"] = true;
    $.__views.__alloyId55 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId55"
    });
    $.__views.FavoriteProRow.add($.__views.__alloyId55);
    $.__views.row = Ti.UI.createView({
        backgroundFocusedColor: "#ADCFE6",
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId55.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        image: "http://placehold.it/50x50",
        width: "100dp",
        left: "0dp",
        top: "10dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderColor: "#fff",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId56 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        right: "30dp",
        height: Ti.UI.SIZE,
        id: "__alloyId56"
    });
    $.__views.row.add($.__views.__alloyId56);
    $.__views.title = Ti.UI.createLabel({
        color: "#fff",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId56.add($.__views.title);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#eee",
        font: {
            fontSize: "13dp"
        },
        id: "content"
    });
    $.__views.__alloyId56.add($.__views.content);
    $.__views.__alloyId57 = Ti.UI.createImageView({
        right: "0",
        width: "30dp",
        height: Ti.UI.SIZE,
        image: "/ic_del.png",
        id: "__alloyId57"
    });
    $.__views.row.add($.__views.__alloyId57);
    Del ? $.__views.__alloyId57.addEventListener("click", Del) : __defers["$.__views.__alloyId57!click!Del"] = true;
    $.__views.__alloyId58 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#343434",
        id: "__alloyId58"
    });
    $.__views.__alloyId55.add($.__views.__alloyId58);
    $.__views.__alloyId59 = Ti.UI.createView({
        height: "1dp",
        bottom: "1dp",
        backgroundColor: "#4F5050",
        id: "__alloyId59"
    });
    $.__views.__alloyId55.add($.__views.__alloyId59);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var id = args.id || "";
    var face = args.face || "";
    var title = args.title || "";
    var content = args.content || "";
    var price = args.price || "";
    var pics = args.pics || "";
    var storeId = args.storeId || "";
    Ti.API.info(id);
    $.face.setImage(Alloy.Globals.WebUrl + face);
    $.title.setText(title);
    $.content.setText(String.format("会员价：%.2f元", parseFloat(price)));
    __defers["$.__views.FavoriteProRow!click!GotoDetail"] && $.__views.FavoriteProRow.addEventListener("click", GotoDetail);
    __defers["$.__views.__alloyId57!click!Del"] && $.__views.__alloyId57.addEventListener("click", Del);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;