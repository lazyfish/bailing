function Controller() {
    function Close() {
        $.win.close();
    }
    function LoadItem(item) {
        var dataItem = UI.Controller("OrderDetailRow", {
            id: item.id,
            pid: item.Pid,
            num: item.Amount,
            price: item.Price,
            name: item.ProName,
            face: item.CoverPicture,
            state: item.State,
            password: item.Password,
            OrderState: OrderState
        });
        dataList.push(dataItem);
        return dataItem;
    }
    function LoadFinish(data, total) {
        $.table.setData(dataList);
        parseInt(total) > dataList.length && $.table.appendRow(moreView);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "OrderDetail";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId103 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId103"
    });
    $.__views.TitleBar.add($.__views.__alloyId103);
    Close ? $.__views.__alloyId103.addEventListener("click", Close) : __defers["$.__views.__alloyId103!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId103.add($.__views.btnBack);
    $.__views.__alloyId104 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "订单明细",
        id: "__alloyId104"
    });
    $.__views.TitleBar.add($.__views.__alloyId104);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.__alloyId105 = Ti.UI.createView({
        layout: "vertical",
        top: "0",
        height: Ti.UI.SIZE,
        id: "__alloyId105"
    });
    $.__views.MainWin.add($.__views.__alloyId105);
    $.__views.header = Ti.UI.createView({
        left: "10dp",
        right: "10dp",
        top: "10dp",
        height: Ti.UI.SIZE,
        id: "header"
    });
    $.__views.__alloyId105.add($.__views.header);
    $.__views.sn = Ti.UI.createLabel({
        top: "0",
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "sn"
    });
    $.__views.header.add($.__views.sn);
    $.__views.sum = Ti.UI.createLabel({
        top: "25dp",
        left: "0",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "13dp"
        },
        id: "sum"
    });
    $.__views.header.add($.__views.sum);
    $.__views.time = Ti.UI.createLabel({
        top: "25dp",
        right: "0",
        color: "#666",
        font: {
            fontSize: "13dp"
        },
        id: "time"
    });
    $.__views.header.add($.__views.time);
    $.__views.__alloyId106 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId106"
    });
    $.__views.__alloyId105.add($.__views.__alloyId106);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table",
        top: "70dp"
    });
    $.__views.MainWin.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var DateUtility = require("DateUtility");
    var args = arguments[0] || {};
    var id = args.id || "";
    var sn = args.sn || "";
    var price = args.price || "";
    var OrderState = args.OrderState || "";
    var time = args.time || "";
    var url = "";
    var moreView = null;
    var dataList = new Array();
    $.sn.text = sn;
    $.sum.text = String.format("总额:%.2f元", parseFloat(price));
    $.time.text = DateUtility.ToString(time, "yyyy-MM-dd HH:mm:ss");
    url = Alloy.Globals.ApiUrl + "/order.ashx?action=orderdetail&orderid=" + id;
    Ti.API.info(url);
    moreView || (moreView = UI.Controller("MoreRow", {
        url: url,
        index: 0,
        itemFun: LoadItem,
        finishFun: LoadFinish
    }));
    Ajax.json(url, LoadItem, LoadFinish);
    __defers["$.__views.__alloyId103!click!Close"] && $.__views.__alloyId103.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;