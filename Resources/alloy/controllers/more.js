function Controller() {
    function GoHistory() {
        Alloy.createController("History").getView().open();
    }
    function GoSuggestion() {
        Alloy.createController("Suggestion").getView().open();
    }
    function CallTel() {
        Ti.Platform.openURL("tel:" + tel);
    }
    function GoAbout() {
        Alloy.createController("About").getView().open();
    }
    function GoHelp() {
        Alloy.createController("Help").getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "more";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.more = Ti.UI.createScrollView({
        layout: "vertical",
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        scrollType: "vertical",
        id: "more"
    });
    $.__views.more && $.addTopLevelView($.__views.more);
    $.__views.__alloyId236 = Ti.UI.createView({
        height: "35dp",
        top: "10",
        layout: "horizontal",
        width: "90%",
        id: "__alloyId236"
    });
    $.__views.more.add($.__views.__alloyId236);
    $.__views.__alloyId237 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId237"
    });
    $.__views.__alloyId236.add($.__views.__alloyId237);
    $.__views.__alloyId238 = Ti.UI.createView({
        left: "0",
        right: "0",
        height: Ti.UI.FILL,
        width: "90%",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        layout: "horizontal",
        id: "__alloyId238"
    });
    $.__views.__alloyId236.add($.__views.__alloyId238);
    $.__views.__alloyId239 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: "48%",
        font: {
            fontSize: "14dp"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        top: "6",
        text: "点评/分享",
        id: "__alloyId239"
    });
    $.__views.__alloyId238.add($.__views.__alloyId239);
    $.__views.__alloyId240 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: "48%",
        font: {
            fontSize: "14dp"
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        top: "6",
        text: "添加商户",
        id: "__alloyId240"
    });
    $.__views.__alloyId238.add($.__views.__alloyId240);
    $.__views.__alloyId241 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId241"
    });
    $.__views.__alloyId236.add($.__views.__alloyId241);
    $.__views.__alloyId242 = Ti.UI.createView({
        left: 10,
        right: 10,
        top: 10,
        height: Ti.UI.SIZE,
        layout: "vertical",
        backgroundImage: "/more_bg.png",
        id: "__alloyId242"
    });
    $.__views.more.add($.__views.__alloyId242);
    $.__views.__alloyId243 = Ti.UI.createView({
        top: "10dp",
        layout: "horizontal",
        height: "30dp",
        id: "__alloyId243"
    });
    $.__views.__alloyId242.add($.__views.__alloyId243);
    GoHistory ? $.__views.__alloyId243.addEventListener("click", GoHistory) : __defers["$.__views.__alloyId243!click!GoHistory"] = true;
    $.__views.__alloyId244 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_history.png",
        id: "__alloyId244"
    });
    $.__views.__alloyId243.add($.__views.__alloyId244);
    $.__views.__alloyId245 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        width: Ti.UI.SIZE,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "浏览历史",
        id: "__alloyId245"
    });
    $.__views.__alloyId243.add($.__views.__alloyId245);
    $.__views.__alloyId246 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId246"
    });
    $.__views.__alloyId242.add($.__views.__alloyId246);
    $.__views.__alloyId247 = Ti.UI.createView({
        top: "10dp",
        layout: "horizontal",
        height: "30dp",
        id: "__alloyId247"
    });
    $.__views.__alloyId242.add($.__views.__alloyId247);
    GoSuggestion ? $.__views.__alloyId247.addEventListener("click", GoSuggestion) : __defers["$.__views.__alloyId247!click!GoSuggestion"] = true;
    $.__views.__alloyId248 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_feedbook.png",
        id: "__alloyId248"
    });
    $.__views.__alloyId247.add($.__views.__alloyId248);
    $.__views.__alloyId249 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        width: Ti.UI.SIZE,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "意见反馈",
        id: "__alloyId249"
    });
    $.__views.__alloyId247.add($.__views.__alloyId249);
    $.__views.__alloyId250 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId250"
    });
    $.__views.__alloyId242.add($.__views.__alloyId250);
    $.__views.__alloyId251 = Ti.UI.createView({
        top: "10dp",
        layout: "horizontal",
        height: "30dp",
        id: "__alloyId251"
    });
    $.__views.__alloyId242.add($.__views.__alloyId251);
    GoAbout ? $.__views.__alloyId251.addEventListener("click", GoAbout) : __defers["$.__views.__alloyId251!click!GoAbout"] = true;
    $.__views.__alloyId252 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_about.png",
        id: "__alloyId252"
    });
    $.__views.__alloyId251.add($.__views.__alloyId252);
    $.__views.__alloyId253 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        width: Ti.UI.SIZE,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "关于百领",
        id: "__alloyId253"
    });
    $.__views.__alloyId251.add($.__views.__alloyId253);
    $.__views.__alloyId254 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId254"
    });
    $.__views.__alloyId242.add($.__views.__alloyId254);
    $.__views.__alloyId255 = Ti.UI.createView({
        top: "10dp",
        layout: "horizontal",
        height: "30dp",
        id: "__alloyId255"
    });
    $.__views.__alloyId242.add($.__views.__alloyId255);
    GoHelp ? $.__views.__alloyId255.addEventListener("click", GoHelp) : __defers["$.__views.__alloyId255!click!GoHelp"] = true;
    $.__views.__alloyId256 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_help.png",
        id: "__alloyId256"
    });
    $.__views.__alloyId255.add($.__views.__alloyId256);
    $.__views.__alloyId257 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        width: Ti.UI.SIZE,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "新手帮助",
        id: "__alloyId257"
    });
    $.__views.__alloyId255.add($.__views.__alloyId257);
    $.__views.__alloyId258 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId258"
    });
    $.__views.__alloyId242.add($.__views.__alloyId258);
    $.__views.__alloyId259 = Ti.UI.createView({
        top: "10dp",
        layout: "horizontal",
        height: "30dp",
        id: "__alloyId259"
    });
    $.__views.__alloyId242.add($.__views.__alloyId259);
    CallTel ? $.__views.__alloyId259.addEventListener("click", CallTel) : __defers["$.__views.__alloyId259!click!CallTel"] = true;
    $.__views.__alloyId260 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_tel.png",
        id: "__alloyId260"
    });
    $.__views.__alloyId259.add($.__views.__alloyId260);
    $.__views.ContactTitle = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        width: Ti.UI.SIZE,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        id: "ContactTitle",
        text: "联系客服"
    });
    $.__views.__alloyId259.add($.__views.ContactTitle);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var tel = "";
    Ajax.json(Alloy.Globals.ApiUrl + "/system.ashx", function(item) {
        $.ContactTitle.setText(String.format("联系客户：%s", item.Tel));
        tel = item.Tel;
    });
    __defers["$.__views.__alloyId243!click!GoHistory"] && $.__views.__alloyId243.addEventListener("click", GoHistory);
    __defers["$.__views.__alloyId247!click!GoSuggestion"] && $.__views.__alloyId247.addEventListener("click", GoSuggestion);
    __defers["$.__views.__alloyId251!click!GoAbout"] && $.__views.__alloyId251.addEventListener("click", GoAbout);
    __defers["$.__views.__alloyId255!click!GoHelp"] && $.__views.__alloyId255.addEventListener("click", GoHelp);
    __defers["$.__views.__alloyId259!click!CallTel"] && $.__views.__alloyId259.addEventListener("click", CallTel);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;