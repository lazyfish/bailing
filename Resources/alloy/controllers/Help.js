function Controller() {
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Help";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId69 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId69"
    });
    $.__views.TitleBar.add($.__views.__alloyId69);
    Close ? $.__views.__alloyId69.addEventListener("click", Close) : __defers["$.__views.__alloyId69!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId69.add($.__views.btnBack);
    $.__views.Title = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        id: "Title",
        text: "新手帮助"
    });
    $.__views.TitleBar.add($.__views.Title);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox",
        bottom: "40dp"
    });
    $.__views.MainWin.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Ajax = require("Ajax");
    var url = Alloy.Globals.ApiUrl + "/artical.ashx?action=list&type=70";
    Ti.API.info(url);
    Ajax.json(url, function(item) {
        return UI.Controller("ArticleRow", {
            id: item.id,
            title: item.Title,
            content: item.ArtContent,
            time: item.AddTime
        });
    }, function(data) {
        $.table.setData(data);
    });
    __defers["$.__views.__alloyId69!click!Close"] && $.__views.__alloyId69.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;