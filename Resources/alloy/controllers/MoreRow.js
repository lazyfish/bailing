function Controller() {
    function LoadMore() {
        var newUrl = "";
        index += 1;
        newUrl = -1 == url.indexOf("?") ? url + "?NowPage=" + index : url + "&NowPage=" + index;
        Ajax.json(newUrl, itemFun, finishFun);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "MoreRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.MoreRow = Ti.UI.createTableViewRow({
        height: "30",
        id: "MoreRow"
    });
    $.__views.MoreRow && $.addTopLevelView($.__views.MoreRow);
    LoadMore ? $.__views.MoreRow.addEventListener("click", LoadMore) : __defers["$.__views.MoreRow!click!LoadMore"] = true;
    $.__views.__alloyId99 = Ti.UI.createLabel({
        font: {
            fontSize: 14
        },
        color: "#333",
        text: "查看更多",
        id: "__alloyId99"
    });
    $.__views.MoreRow.add($.__views.__alloyId99);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var args = arguments[0] || {};
    var url = args.url || "";
    var index = args.index || 1;
    var itemFun = args.itemFun || "";
    var finishFun = args.finishFun || "";
    __defers["$.__views.MoreRow!click!LoadMore"] && $.__views.MoreRow.addEventListener("click", LoadMore);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;