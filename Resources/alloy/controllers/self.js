function Controller() {
    function ReadUser() {
        Ajax.json(Alloy.Globals.ApiUrl + "/user.ashx?action=getuserbyguid&IdGUID=" + Alloy.Globals.LoginToken, function(item) {
            $.UserName.text = item.RealName;
            $.Experience.text = item.Experience;
            $.Grade.text = item.Grade;
            NextExperience = item.Experience;
        });
        Ajax.get(Alloy.Globals.ApiUrl + "/user.ashx?action=HasSign&idguid=" + Alloy.Globals.LoginToken, function(data) {
            "True" == data && ($.SignButtonText.text = "已签到");
        });
    }
    function GotoCar() {
        UI.Open("BuyCar");
    }
    function GotoUnPayOrder() {
        UI.Open("Order", {
            State: "1"
        });
    }
    function GotoFinishOrder() {
        UI.Open("Order", {
            State: "7"
        });
    }
    function GotoFavorite() {
        UI.Open("Favorite");
    }
    function GotoFavoriteStore() {
        UI.Open("FavoriteStore");
    }
    function GotoUnComment() {
        UI.Open("ProComment");
    }
    function GooModifyUser() {
        UI.Open("SelfModify");
    }
    function GoMyCard() {
        UI.Open("MyCard");
    }
    function GoBLCard() {
        UI.Open("CardDetail", {
            NextExperience: NextExperience
        });
    }
    function Sign() {
        if (!hasSign) {
            var url = Alloy.Globals.ApiUrl + "/user.ashx?action=Sign&idguid=" + Alloy.Globals.LoginToken;
            Ajax.get(url, function() {
                ReadUser();
                alert("签到成功");
            });
        }
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "self";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createView({
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.__alloyId279 = Ti.UI.createScrollView({
        layout: "vertical",
        width: Ti.UI.FILL,
        height: Ti.UI.SIZE,
        scrollType: "vertical",
        id: "__alloyId279"
    });
    $.__views.win.add($.__views.__alloyId279);
    $.__views.head = Ti.UI.createView({
        backgroundColor: "#B9D2E2",
        height: "120dp",
        id: "head"
    });
    $.__views.__alloyId279.add($.__views.head);
    $.__views.face = Ti.UI.createView({
        width: "70dp",
        height: "70dp",
        borderWeight: "0.5dp",
        borderColor: "#bbb",
        backgroundImage: "/user_face.png",
        left: "20dp",
        top: "20dp",
        id: "face"
    });
    $.__views.head.add($.__views.face);
    $.__views.headText = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        left: "100dp",
        right: 100,
        top: "20dp",
        id: "headText"
    });
    $.__views.head.add($.__views.headText);
    $.__views.UserName = Ti.UI.createLabel({
        left: "0",
        color: "#333",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp",
            fontWeight: "bold"
        },
        id: "UserName"
    });
    $.__views.headText.add($.__views.UserName);
    $.__views.__alloyId280 = Ti.UI.createView({
        top: "3dp",
        left: "0",
        height: Ti.UI.SIZE,
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId280"
    });
    $.__views.headText.add($.__views.__alloyId280);
    $.__views.__alloyId281 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "积分：",
        id: "__alloyId281"
    });
    $.__views.__alloyId280.add($.__views.__alloyId281);
    $.__views.Grade = Ti.UI.createLabel({
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        color: "#444",
        font: {
            fontSize: "14dp"
        },
        id: "Grade"
    });
    $.__views.__alloyId280.add($.__views.Grade);
    $.__views.__alloyId282 = Ti.UI.createView({
        top: "3dp",
        left: "0",
        height: Ti.UI.SIZE,
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId282"
    });
    $.__views.headText.add($.__views.__alloyId282);
    $.__views.__alloyId283 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "经验值：",
        id: "__alloyId283"
    });
    $.__views.__alloyId282.add($.__views.__alloyId283);
    $.__views.Experience = Ti.UI.createLabel({
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        color: "#444",
        font: {
            fontSize: "14dp"
        },
        id: "Experience"
    });
    $.__views.__alloyId282.add($.__views.Experience);
    $.__views.SignButton = Ti.UI.createView({
        height: "35dp",
        id: "SignButton",
        width: Ti.UI.SIZE,
        right: "10"
    });
    $.__views.head.add($.__views.SignButton);
    Sign ? $.__views.SignButton.addEventListener("click", Sign) : __defers["$.__views.SignButton!click!Sign"] = true;
    $.__views.__alloyId284 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId284"
    });
    $.__views.SignButton.add($.__views.__alloyId284);
    $.__views.__alloyId285 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.SIZE,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId285"
    });
    $.__views.SignButton.add($.__views.__alloyId285);
    $.__views.SignButtonText = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        id: "SignButtonText",
        text: "签到"
    });
    $.__views.__alloyId285.add($.__views.SignButtonText);
    $.__views.__alloyId286 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId286"
    });
    $.__views.SignButton.add($.__views.__alloyId286);
    $.__views.__alloyId287 = Ti.UI.createView({
        layout: "vertical",
        left: "10dp",
        right: "10dp",
        id: "__alloyId287"
    });
    $.__views.__alloyId279.add($.__views.__alloyId287);
    $.__views.__alloyId288 = Ti.UI.createLabel({
        top: "10dp",
        font: {
            fontSize: "13dp"
        },
        height: "25dp",
        left: "10dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        color: "#555",
        text: "个人信息",
        id: "__alloyId288"
    });
    $.__views.__alloyId287.add($.__views.__alloyId288);
    $.__views.__alloyId289 = Ti.UI.createView({
        backgroundColor: "#bbb",
        height: "1dp",
        id: "__alloyId289"
    });
    $.__views.__alloyId287.add($.__views.__alloyId289);
    $.__views.__alloyId290 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId290"
    });
    $.__views.__alloyId287.add($.__views.__alloyId290);
    GooModifyUser ? $.__views.__alloyId290.addEventListener("click", GooModifyUser) : __defers["$.__views.__alloyId290!click!GooModifyUser"] = true;
    $.__views.icModify = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_modify_password.png",
        id: "icModify"
    });
    $.__views.__alloyId290.add($.__views.icModify);
    $.__views.__alloyId291 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "基本资料修改",
        id: "__alloyId291"
    });
    $.__views.__alloyId290.add($.__views.__alloyId291);
    $.__views.__alloyId292 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId292"
    });
    $.__views.__alloyId287.add($.__views.__alloyId292);
    $.__views.__alloyId293 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId293"
    });
    $.__views.__alloyId287.add($.__views.__alloyId293);
    GoMyCard ? $.__views.__alloyId293.addEventListener("click", GoMyCard) : __defers["$.__views.__alloyId293!click!GoMyCard"] = true;
    $.__views.__alloyId294 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_vip_card.png",
        id: "__alloyId294"
    });
    $.__views.__alloyId293.add($.__views.__alloyId294);
    $.__views.__alloyId295 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "我的卡包",
        id: "__alloyId295"
    });
    $.__views.__alloyId293.add($.__views.__alloyId295);
    $.__views.__alloyId296 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId296"
    });
    $.__views.__alloyId287.add($.__views.__alloyId296);
    $.__views.__alloyId297 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId297"
    });
    $.__views.__alloyId287.add($.__views.__alloyId297);
    GoBLCard ? $.__views.__alloyId297.addEventListener("click", GoBLCard) : __defers["$.__views.__alloyId297!click!GoBLCard"] = true;
    $.__views.__alloyId298 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_card.png",
        id: "__alloyId298"
    });
    $.__views.__alloyId297.add($.__views.__alloyId298);
    $.__views.__alloyId299 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "百领卡",
        id: "__alloyId299"
    });
    $.__views.__alloyId297.add($.__views.__alloyId299);
    $.__views.__alloyId300 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId300"
    });
    $.__views.__alloyId287.add($.__views.__alloyId300);
    $.__views.__alloyId301 = Ti.UI.createLabel({
        top: "10dp",
        font: {
            fontSize: "13dp"
        },
        height: "25dp",
        left: "10dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        color: "#555",
        text: "订单",
        id: "__alloyId301"
    });
    $.__views.__alloyId287.add($.__views.__alloyId301);
    $.__views.__alloyId302 = Ti.UI.createView({
        backgroundColor: "#bbb",
        height: "1dp",
        id: "__alloyId302"
    });
    $.__views.__alloyId287.add($.__views.__alloyId302);
    $.__views.__alloyId303 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId303"
    });
    $.__views.__alloyId287.add($.__views.__alloyId303);
    GotoUnPayOrder ? $.__views.__alloyId303.addEventListener("click", GotoUnPayOrder) : __defers["$.__views.__alloyId303!click!GotoUnPayOrder"] = true;
    $.__views.icUnPay = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_unpaid.png",
        id: "icUnPay"
    });
    $.__views.__alloyId303.add($.__views.icUnPay);
    $.__views.__alloyId304 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "待付款",
        id: "__alloyId304"
    });
    $.__views.__alloyId303.add($.__views.__alloyId304);
    $.__views.__alloyId305 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId305"
    });
    $.__views.__alloyId287.add($.__views.__alloyId305);
    $.__views.__alloyId306 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId306"
    });
    $.__views.__alloyId287.add($.__views.__alloyId306);
    GotoFinishOrder ? $.__views.__alloyId306.addEventListener("click", GotoFinishOrder) : __defers["$.__views.__alloyId306!click!GotoFinishOrder"] = true;
    $.__views.icPay = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_paid.png",
        id: "icPay"
    });
    $.__views.__alloyId306.add($.__views.icPay);
    $.__views.__alloyId307 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "已付款",
        id: "__alloyId307"
    });
    $.__views.__alloyId306.add($.__views.__alloyId307);
    $.__views.__alloyId308 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId308"
    });
    $.__views.__alloyId287.add($.__views.__alloyId308);
    $.__views.__alloyId309 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId309"
    });
    $.__views.__alloyId287.add($.__views.__alloyId309);
    GotoUnComment ? $.__views.__alloyId309.addEventListener("click", GotoUnComment) : __defers["$.__views.__alloyId309!click!GotoUnComment"] = true;
    $.__views.__alloyId310 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_comment2.png",
        id: "__alloyId310"
    });
    $.__views.__alloyId309.add($.__views.__alloyId310);
    $.__views.__alloyId311 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "未评论的产品",
        id: "__alloyId311"
    });
    $.__views.__alloyId309.add($.__views.__alloyId311);
    $.__views.__alloyId312 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId312"
    });
    $.__views.__alloyId287.add($.__views.__alloyId312);
    $.__views.__alloyId313 = Ti.UI.createLabel({
        top: "10dp",
        font: {
            fontSize: "13dp"
        },
        height: "25dp",
        left: "10dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        color: "#555",
        text: "账户设置",
        id: "__alloyId313"
    });
    $.__views.__alloyId287.add($.__views.__alloyId313);
    $.__views.__alloyId314 = Ti.UI.createView({
        backgroundColor: "#bbb",
        height: "1dp",
        id: "__alloyId314"
    });
    $.__views.__alloyId287.add($.__views.__alloyId314);
    $.__views.__alloyId315 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId315"
    });
    $.__views.__alloyId287.add($.__views.__alloyId315);
    GotoCar ? $.__views.__alloyId315.addEventListener("click", GotoCar) : __defers["$.__views.__alloyId315!click!GotoCar"] = true;
    $.__views.__alloyId316 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_car2.png",
        id: "__alloyId316"
    });
    $.__views.__alloyId315.add($.__views.__alloyId316);
    $.__views.__alloyId317 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "购物车",
        id: "__alloyId317"
    });
    $.__views.__alloyId315.add($.__views.__alloyId317);
    $.__views.__alloyId318 = Ti.UI.createView({
        backgroundColor: "#bbb",
        height: "1dp",
        id: "__alloyId318"
    });
    $.__views.__alloyId287.add($.__views.__alloyId318);
    $.__views.__alloyId319 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId319"
    });
    $.__views.__alloyId287.add($.__views.__alloyId319);
    GotoFavorite ? $.__views.__alloyId319.addEventListener("click", GotoFavorite) : __defers["$.__views.__alloyId319!click!GotoFavorite"] = true;
    $.__views.__alloyId320 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_fav.png",
        id: "__alloyId320"
    });
    $.__views.__alloyId319.add($.__views.__alloyId320);
    $.__views.__alloyId321 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "收藏的产品",
        id: "__alloyId321"
    });
    $.__views.__alloyId319.add($.__views.__alloyId321);
    $.__views.__alloyId322 = Ti.UI.createView({
        backgroundColor: "#bbb",
        height: "1dp",
        id: "__alloyId322"
    });
    $.__views.__alloyId287.add($.__views.__alloyId322);
    $.__views.__alloyId323 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId323"
    });
    $.__views.__alloyId287.add($.__views.__alloyId323);
    GotoFavoriteStore ? $.__views.__alloyId323.addEventListener("click", GotoFavoriteStore) : __defers["$.__views.__alloyId323!click!GotoFavoriteStore"] = true;
    $.__views.__alloyId324 = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        image: "/ic_fav.png",
        id: "__alloyId324"
    });
    $.__views.__alloyId323.add($.__views.__alloyId324);
    $.__views.__alloyId325 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "收藏的门店",
        id: "__alloyId325"
    });
    $.__views.__alloyId323.add($.__views.__alloyId325);
    $.__views.__alloyId326 = Ti.UI.createView({
        backgroundColor: "#bbb",
        height: "1dp",
        id: "__alloyId326"
    });
    $.__views.__alloyId287.add($.__views.__alloyId326);
    $.__views.__alloyId327 = Ti.UI.createView({
        top: "10dp",
        height: "30dp",
        layout: "horizontal",
        id: "__alloyId327"
    });
    $.__views.__alloyId287.add($.__views.__alloyId327);
    $.__views.icModifyPassword = Ti.UI.createImageView({
        width: "15dp",
        left: "15dp",
        height: Ti.UI.SIZE,
        id: "icModifyPassword"
    });
    $.__views.__alloyId327.add($.__views.icModifyPassword);
    $.__views.__alloyId328 = Ti.UI.createLabel({
        color: "#555",
        left: "20dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "15dp"
        },
        text: "修改账户密码",
        id: "__alloyId328"
    });
    $.__views.__alloyId327.add($.__views.__alloyId328);
    $.__views.__alloyId329 = Ti.UI.createView({
        backgroundColor: "#ccc",
        left: "10dp",
        right: "10dp",
        height: "1dp",
        id: "__alloyId329"
    });
    $.__views.__alloyId287.add($.__views.__alloyId329);
    $.__views.__alloyId330 = Ti.UI.createLabel({
        color: "#fff",
        backgroundColor: "#E84048",
        font: {
            fontSize: "14dp"
        },
        height: "30dp",
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        borderRadius: 10,
        width: "80%",
        top: "10dp",
        text: "退出账号",
        id: "__alloyId330"
    });
    $.__views.__alloyId287.add($.__views.__alloyId330);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var UI = require("UI");
    var NextExperience = 0;
    ReadUser();
    var hasSign = false;
    __defers["$.__views.SignButton!click!Sign"] && $.__views.SignButton.addEventListener("click", Sign);
    __defers["$.__views.__alloyId290!click!GooModifyUser"] && $.__views.__alloyId290.addEventListener("click", GooModifyUser);
    __defers["$.__views.__alloyId293!click!GoMyCard"] && $.__views.__alloyId293.addEventListener("click", GoMyCard);
    __defers["$.__views.__alloyId297!click!GoBLCard"] && $.__views.__alloyId297.addEventListener("click", GoBLCard);
    __defers["$.__views.__alloyId303!click!GotoUnPayOrder"] && $.__views.__alloyId303.addEventListener("click", GotoUnPayOrder);
    __defers["$.__views.__alloyId306!click!GotoFinishOrder"] && $.__views.__alloyId306.addEventListener("click", GotoFinishOrder);
    __defers["$.__views.__alloyId309!click!GotoUnComment"] && $.__views.__alloyId309.addEventListener("click", GotoUnComment);
    __defers["$.__views.__alloyId315!click!GotoCar"] && $.__views.__alloyId315.addEventListener("click", GotoCar);
    __defers["$.__views.__alloyId319!click!GotoFavorite"] && $.__views.__alloyId319.addEventListener("click", GotoFavorite);
    __defers["$.__views.__alloyId323!click!GotoFavoriteStore"] && $.__views.__alloyId323.addEventListener("click", GotoFavoriteStore);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;