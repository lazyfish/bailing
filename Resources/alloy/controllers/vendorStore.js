function Controller() {
    function Init() {
        LoadArea();
        LoadSortItem();
        BindTable();
    }
    function BindTable() {
        Ti.Geolocation.getCurrentPosition(function(e) {
            url = Alloy.Globals.ApiUrl + "/store.ashx?action=getstorelist&storeid=" + id + "&lng=" + e.coords.longitude + "&lat=" + e.coords.latitude;
            moreView || (moreView = UI.Controller("MoreRow", {
                url: url,
                index: 0,
                itemFun: LoadItem,
                finishFun: LoadFinish
            }));
            Ti.API.info(url);
            Ajax.json(url, LoadItem, LoadFinish);
        });
    }
    function LoadItem(item) {
        var dataItem = Alloy.createController("vendorStoreRow", {
            id: item.id,
            face: Alloy.Globals.WebUrl + item.StorePicUrl,
            title: item.StoreName,
            content: item.ContactTel,
            range: item.Range
        }).getView();
        dataList.push(dataItem);
        return dataItem;
    }
    function LoadFinish(data, total) {
        $.table.setData(dataList);
        parseInt(total) > dataList.length && $.table.appendRow(moreView);
        actInd.hide();
    }
    function LoadArea() {
        areaDialog = new ListDialog.option();
        left = Alloy.Globals.width / 2;
        areaDialog.Init($.win, {
            left: left,
            top: "30dp"
        }, {
            width: "140dp",
            height: "240dp"
        }, []);
        Ajax.json(Alloy.Globals.ApiUrl + "/ct.ashx?action=getarea&code=350500", function(item) {
            return {
                id: item.CTCode,
                name: item.CTName
            };
        }, function(data) {
            data.unshift({
                id: "350500",
                name: "全城"
            });
            areaDialog.BindData(data);
        });
        areaDialog.OnRootClick = function(e) {
            area = e.id;
            BindTable();
            areaDialog.Hide();
        };
    }
    function LoadSortItem() {
        sortDialog = new ListDialog.option();
        left = Alloy.Globals.width / 2;
        sortDialog.Init($.win, {
            left: left,
            top: "70dp"
        }, {
            width: "140dp",
            height: "80dp"
        }, []);
        var data = [ {
            id: 0,
            name: "默认排序"
        }, {
            id: 1,
            name: "按人气从高到低"
        } ];
        sortDialog.OnRootClick = function(e) {
            sort = e.id;
            BindTable();
            sortDialog.Hide();
        };
        sortDialog.BindData(data);
        sortDialog.SetCurrentRoot(0);
    }
    function Close() {
        $.win.close();
    }
    function ShowAreaWin() {
        areaDialog.Show();
        $.SortTab.setBackgroundImage("");
        $.SortTab.setColor("#84868D");
        $.AreaTab.setBackgroundImage("/tool_left.png");
        $.AreaTab.setColor("#fff");
    }
    function ShowSortWin() {
        sortDialog.Show();
        $.SortTab.setBackgroundImage("/tool_right.png");
        $.SortTab.setColor("#fff");
        $.AreaTab.setBackgroundImage("");
        $.AreaTab.setColor("#84868D");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vendorStore";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId376 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId376"
    });
    $.__views.TitleBar.add($.__views.__alloyId376);
    Close ? $.__views.__alloyId376.addEventListener("click", Close) : __defers["$.__views.__alloyId376!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId376.add($.__views.btnBack);
    $.__views.Vendor = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "连锁门店",
        id: "Vendor"
    });
    $.__views.TitleBar.add($.__views.Vendor);
    $.__views.MainWin = Ti.UI.createView({
        layout: "vertical",
        backgroundColor: "#fff",
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.Toolbar = Ti.UI.createView({
        height: "45dp",
        backgroundImage: "/tag_bg.png",
        width: Ti.UI.FILL,
        id: "Toolbar"
    });
    $.__views.MainWin.add($.__views.Toolbar);
    $.__views.__alloyId377 = Ti.UI.createView({
        top: "5dp",
        height: "35dp",
        backgroundImage: "/store_type_bg_a.png",
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId377"
    });
    $.__views.Toolbar.add($.__views.__alloyId377);
    $.__views.__alloyId378 = Ti.UI.createView({
        top: "5dp",
        right: 5,
        left: 5,
        height: "25dp",
        backgroundImage: "/store_type_bg_b.png",
        layout: "horizontal",
        width: Ti.UI.SIZE,
        id: "__alloyId378"
    });
    $.__views.__alloyId377.add($.__views.__alloyId378);
    $.__views.AreaTab = Ti.UI.createLabel({
        width: 100,
        font: {
            fontSize: 14
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#fff",
        backgroundImage: "/tool_left.png",
        text: "地区",
        id: "AreaTab"
    });
    $.__views.__alloyId378.add($.__views.AreaTab);
    ShowAreaWin ? $.__views.AreaTab.addEventListener("click", ShowAreaWin) : __defers["$.__views.AreaTab!click!ShowAreaWin"] = true;
    $.__views.SortTab = Ti.UI.createLabel({
        width: 100,
        font: {
            fontSize: 14
        },
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: "25dp",
        color: "#84868D",
        text: "排序",
        id: "SortTab"
    });
    $.__views.__alloyId378.add($.__views.SortTab);
    ShowSortWin ? $.__views.SortTab.addEventListener("click", ShowSortWin) : __defers["$.__views.SortTab!click!ShowSortWin"] = true;
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox"
    });
    $.__views.MainWin.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var ListDialog = require("ListDialog");
    var UI = require("UI");
    var args = arguments[0] || {};
    var id = args.id || "";
    var vendor = args.vendor || "";
    var areaDialog;
    var sortDialog;
    var url = "";
    var moreView = null;
    var dataList = new Array();
    $.Vendor.setText(vendor);
    Init();
    var actInd = Titanium.UI.createActivityIndicator({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#fff",
        message: "获取位置中..."
    });
    $.win.add(actInd);
    actInd.show();
    __defers["$.__views.__alloyId376!click!Close"] && $.__views.__alloyId376.addEventListener("click", Close);
    __defers["$.__views.AreaTab!click!ShowAreaWin"] && $.__views.AreaTab.addEventListener("click", ShowAreaWin);
    __defers["$.__views.SortTab!click!ShowSortWin"] && $.__views.SortTab.addEventListener("click", ShowSortWin);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;