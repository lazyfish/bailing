function Controller() {
    function Close() {
        $.win.close();
    }
    function BeforeLoad(e) {
        Ti.API.debug("BeforeLoad:" + e.url);
        if (-1 != e.url.indexOf("#")) {
            discount = parseInt(e.url.substr(e.url.indexOf("#") + 1));
            Ti.API.debug("discount:" + discount);
            discount && (10 == discount || 0 == discount ? $.btnText.setText("无折扣购买") : $.btnText.setText(discount + "折购买"));
        }
    }
    function Load() {}
    function Buy() {
        loadView.Show("正在放进购物车中");
        var url = Alloy.Globals.ApiUrl + "/car.ashx?action=add";
        var param = {
            idguid: Alloy.Globals.LoginToken,
            storeid: storeId,
            vendorId: vendorId,
            pid: id,
            Discount: parseFloat(discount) / 10,
            SelNum: 1
        };
        Ti.API.debug("Discount", param.Discount, discount, parseFloat(discount) / 10);
        Ajax.post(url, param, function(data) {
            loadView.Hide();
            "ok" == data ? prompt.Show("已放入购物车！", 2e3) : prompt.Show(data, 2e3);
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Dial";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId45 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId45"
    });
    $.__views.TitleBar.add($.__views.__alloyId45);
    Close ? $.__views.__alloyId45.addEventListener("click", Close) : __defers["$.__views.__alloyId45!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId45.add($.__views.btnBack);
    $.__views.Title = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        id: "Title"
    });
    $.__views.TitleBar.add($.__views.Title);
    $.__views.WebView = Ti.UI.createWebView({
        id: "WebView",
        top: "40dp",
        bottom: "50"
    });
    $.__views.win.add($.__views.WebView);
    Load ? $.__views.WebView.addEventListener("load", Load) : __defers["$.__views.WebView!load!Load"] = true;
    BeforeLoad ? $.__views.WebView.addEventListener("beforeload", BeforeLoad) : __defers["$.__views.WebView!beforeload!BeforeLoad"] = true;
    $.__views.__alloyId46 = Ti.UI.createView({
        height: "35dp",
        width: "90%",
        id: "__alloyId46"
    });
    $.__views.win.add($.__views.__alloyId46);
    Buy ? $.__views.__alloyId46.addEventListener("click", Buy) : __defers["$.__views.__alloyId46!click!Buy"] = true;
    $.__views.__alloyId47 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId47"
    });
    $.__views.__alloyId46.add($.__views.__alloyId47);
    $.__views.__alloyId48 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId48"
    });
    $.__views.__alloyId46.add($.__views.__alloyId48);
    $.__views.btnText = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "无折扣购买",
        id: "btnText"
    });
    $.__views.__alloyId48.add($.__views.btnText);
    $.__views.__alloyId49 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId49"
    });
    $.__views.__alloyId46.add($.__views.__alloyId49);
    exports.destroy = function() {};
    _.extend($, $.__views);
    require("UI");
    var Dialog = require("Dialog");
    var Ajax = require("Ajax");
    var args = arguments[0] || {};
    var id = args.id;
    var vendorId = args.vendorId;
    var storeId = args.storeId;
    args.sum;
    var url = Alloy.Globals.WebUrl + "/Mobile/dial.aspx?idguiid=" + Alloy.Globals.LoginToken + "&id=" + id + "&r=" + Math.round(1e4 * Math.random());
    args.close;
    var prompt = new Dialog.prompt();
    var loadView = new Dialog.load();
    var discount = 10;
    $.WebView.url = url;
    __defers["$.__views.__alloyId45!click!Close"] && $.__views.__alloyId45.addEventListener("click", Close);
    __defers["$.__views.WebView!load!Load"] && $.__views.WebView.addEventListener("load", Load);
    __defers["$.__views.WebView!beforeload!BeforeLoad"] && $.__views.WebView.addEventListener("beforeload", BeforeLoad);
    __defers["$.__views.__alloyId46!click!Buy"] && $.__views.__alloyId46.addEventListener("click", Buy);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;