function Controller() {
    function GotoProduct() {
        UI.Open("product", {
            id: pid
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "HistoryRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.HistoryRow = Ti.UI.createTableViewRow({
        id: "HistoryRow"
    });
    $.__views.HistoryRow && $.addTopLevelView($.__views.HistoryRow);
    GotoProduct ? $.__views.HistoryRow.addEventListener("click", GotoProduct) : __defers["$.__views.HistoryRow!click!GotoProduct"] = true;
    $.__views.__alloyId72 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId72"
    });
    $.__views.HistoryRow.add($.__views.__alloyId72);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId72.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId73 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        height: Ti.UI.SIZE,
        id: "__alloyId73"
    });
    $.__views.row.add($.__views.__alloyId73);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId73.add($.__views.title);
    $.__views.price = Ti.UI.createLabel({
        font: {
            fontSize: "13dp"
        },
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        top: "5dp",
        id: "price"
    });
    $.__views.__alloyId73.add($.__views.price);
    $.__views.time = Ti.UI.createLabel({
        font: {
            fontSize: "12dp"
        },
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        id: "time"
    });
    $.__views.__alloyId73.add($.__views.time);
    $.__views.__alloyId74 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId74"
    });
    $.__views.__alloyId72.add($.__views.__alloyId74);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    require("Ajax");
    require("Dialog");
    var DateUtility = require("DateUtility");
    var args = arguments[0] || {};
    args.id || "";
    var pid = args.pid || "";
    var time = args.time || "";
    var price = args.price || "";
    var name = args.name || "";
    var face = args.face || "";
    Ti.API.debug("face:", Alloy.Globals.WebUrl + "/uploads/" + face);
    $.face.setImage(Alloy.Globals.WebUrl + face);
    $.title.setText(name);
    $.time.setText(String.format("查看时间：%s", DateUtility.ToString(time, "yyyy-MM-dd HH:mm")));
    $.price.setText(String.format("售价:%.2f元", parseFloat(price)));
    __defers["$.__views.HistoryRow!click!GotoProduct"] && $.__views.HistoryRow.addEventListener("click", GotoProduct);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;