function Controller() {
    function Close() {
        $.win.close();
    }
    function RemoveRow(e) {
        $.table.deleteRow(e.index);
    }
    function Settle() {
        UI.Open("BuyView", {
            sum: sum,
            close: Close
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "BuyCar";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId5 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId5"
    });
    $.__views.TitleBar.add($.__views.__alloyId5);
    Close ? $.__views.__alloyId5.addEventListener("click", Close) : __defers["$.__views.__alloyId5!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId5.add($.__views.btnBack);
    $.__views.__alloyId6 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "购物车",
        id: "__alloyId6"
    });
    $.__views.TitleBar.add($.__views.__alloyId6);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.listBox = Ti.UI.createView({
        layout: "vertical",
        id: "listBox",
        bottom: "40dp"
    });
    $.__views.MainWin.add($.__views.listBox);
    $.__views.table = Ti.UI.createTableView({
        backgroundColor: "#ffffff",
        separatorStyle: "0",
        height: Ti.UI.FILL,
        id: "table"
    });
    $.__views.listBox.add($.__views.table);
    $.__views.__alloyId7 = Ti.UI.createView({
        height: "40dp",
        bottom: "0",
        id: "__alloyId7"
    });
    $.__views.MainWin.add($.__views.__alloyId7);
    $.__views.__alloyId8 = Ti.UI.createView({
        height: "35dp",
        width: "90%",
        id: "__alloyId8"
    });
    $.__views.__alloyId7.add($.__views.__alloyId8);
    Settle ? $.__views.__alloyId8.addEventListener("click", Settle) : __defers["$.__views.__alloyId8!click!Settle"] = true;
    $.__views.__alloyId9 = Ti.UI.createView({
        left: "0",
        backgroundImage: "/btn_red_left.png",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId9"
    });
    $.__views.__alloyId8.add($.__views.__alloyId9);
    $.__views.__alloyId10 = Ti.UI.createView({
        left: "8dp",
        right: "8dp",
        height: Ti.UI.FILL,
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
        backgroundImage: "/btn_red_mid.png",
        id: "__alloyId10"
    });
    $.__views.__alloyId8.add($.__views.__alloyId10);
    $.__views.__alloyId11 = Ti.UI.createLabel({
        color: "#333",
        height: Ti.UI.SIZE,
        width: Ti.UI.SIZE,
        font: {
            fontSize: "14dp"
        },
        text: "结算",
        id: "__alloyId11"
    });
    $.__views.__alloyId10.add($.__views.__alloyId11);
    $.__views.__alloyId12 = Ti.UI.createView({
        backgroundImage: "/btn_red_right.png",
        right: "0",
        backgroundRepeat: "false",
        height: Ti.UI.FILL,
        width: "8dp",
        id: "__alloyId12"
    });
    $.__views.__alloyId8.add($.__views.__alloyId12);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var Ajax = require("Ajax");
    require("Dialog");
    arguments[0] || {};
    var sum = 0;
    var url = Alloy.Globals.ApiUrl + "/car.ashx?action=list&idguid=" + Alloy.Globals.LoginToken;
    Ti.API.debug(url);
    Ajax.json(url, function(item) {
        Ti.API.debug(String.format("%s|%s|%s", item.Price, item.SelNum, item.Discount));
        sum += parseFloat(item.Price) * parseFloat(item.SelNum) * parseFloat(item.Discount);
        Ti.API.debug(sum);
        return UI.Controller("BuyCarRow", {
            id: item.id,
            pid: item.ProductId,
            num: item.SelNum,
            price: sum,
            name: item.ProName,
            face: item.CoverPicture,
            type: item.Types,
            delFun: RemoveRow
        });
    }, function(data) {
        $.table.setData(data);
    });
    __defers["$.__views.__alloyId5!click!Close"] && $.__views.__alloyId5.addEventListener("click", Close);
    __defers["$.__views.__alloyId8!click!Settle"] && $.__views.__alloyId8.addEventListener("click", Settle);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;