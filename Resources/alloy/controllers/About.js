function Controller() {
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "About";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId0 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId0"
    });
    $.__views.TitleBar.add($.__views.__alloyId0);
    Close ? $.__views.__alloyId0.addEventListener("click", Close) : __defers["$.__views.__alloyId0!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId0.add($.__views.btnBack);
    $.__views.__alloyId1 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "关于百领",
        id: "__alloyId1"
    });
    $.__views.TitleBar.add($.__views.__alloyId1);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin",
        left: "10",
        right: "10",
        bottom: "10",
        layout: "horizontal"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.Introduce = Ti.UI.createWebView({
        disableBounce: "true",
        id: "Introduce"
    });
    $.__views.MainWin.add($.__views.Introduce);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var Ajax = require("Ajax");
    var Utility = require("Utility");
    Ajax.json(Alloy.Globals.ApiUrl + "/system.ashx", function(item) {
        $.Introduce.html = Utility.GetWebView(item.ArtIntro);
    });
    __defers["$.__views.__alloyId0!click!Close"] && $.__views.__alloyId0.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;