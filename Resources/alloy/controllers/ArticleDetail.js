function Controller() {
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "ArticleDetail";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#fff",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId2 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId2"
    });
    $.__views.TitleBar.add($.__views.__alloyId2);
    Close ? $.__views.__alloyId2.addEventListener("click", Close) : __defers["$.__views.__alloyId2!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId2.add($.__views.btnBack);
    $.__views.Title = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        id: "Title",
        text: "新手帮助"
    });
    $.__views.TitleBar.add($.__views.Title);
    $.__views.MainWin = Ti.UI.createScrollView({
        top: "40dp",
        id: "MainWin",
        left: "10",
        right: "10",
        bottom: "10",
        layout: "vertical"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.Title = Ti.UI.createLabel({
        id: "Title",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
        height: Ti.UI.SIZE,
        text: "新手帮助"
    });
    $.__views.MainWin.add($.__views.Title);
    $.__views.Introduce = Ti.UI.createWebView({
        disableBounce: "true",
        id: "Introduce",
        height: Ti.UI.SIZE
    });
    $.__views.MainWin.add($.__views.Introduce);
    exports.destroy = function() {};
    _.extend($, $.__views);
    require("Ajax");
    var Utility = require("Utility");
    var args = arguments[0] || {};
    var title = args.title || "";
    var content = args.content || "";
    args.time || "";
    $.Title.setText(title);
    $.Introduce.html = Utility.GetWebView(content);
    __defers["$.__views.__alloyId2!click!Close"] && $.__views.__alloyId2.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;