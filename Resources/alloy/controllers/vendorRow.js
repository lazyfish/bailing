function Controller() {
    function GotoDetail() {
        Alloy.createController("vendorStore", {
            id: id,
            vendor: title
        }).getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "vendorRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.vendorRow = Ti.UI.createTableViewRow({
        id: "vendorRow"
    });
    $.__views.vendorRow && $.addTopLevelView($.__views.vendorRow);
    GotoDetail ? $.__views.vendorRow.addEventListener("click", GotoDetail) : __defers["$.__views.vendorRow!click!GotoDetail"] = true;
    $.__views.__alloyId373 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId373"
    });
    $.__views.vendorRow.add($.__views.__alloyId373);
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId373.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        image: "http://placehold.it/50x50",
        width: "100dp",
        left: "0dp",
        top: "0dp",
        height: Ti.UI.SIZE,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId374 = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        id: "__alloyId374"
    });
    $.__views.row.add($.__views.__alloyId374);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId374.add($.__views.title);
    $.__views.content = Ti.UI.createLabel({
        top: "5dp",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        font: {
            fontSize: "13dp"
        },
        id: "content"
    });
    $.__views.__alloyId374.add($.__views.content);
    $.__views.__alloyId375 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId375"
    });
    $.__views.__alloyId373.add($.__views.__alloyId375);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var id = args.id || "";
    var face = args.face || "";
    var title = args.title || "";
    var content = args.content || "";
    $.face.setImage(face);
    $.title.setText(title);
    $.content.setText(content);
    __defers["$.__views.vendorRow!click!GotoDetail"] && $.__views.vendorRow.addEventListener("click", GotoDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;