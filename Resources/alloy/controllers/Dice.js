function Controller() {
    function Start() {
        var animateOption = Titanium.UI.createAnimation({
            transform: Ti.UI.create2DMatrix().rotate(179),
            duration: 300
        });
        image.animate(animateOption, function() {
            var animateOption2 = Titanium.UI.createAnimation({
                transform: Ti.UI.create2DMatrix().rotate(359),
                duration: 300
            });
            image.animate(animateOption2, function() {
                var animateOption2 = Titanium.UI.createAnimation({
                    transform: Ti.UI.create2DMatrix().rotate(0),
                    duration: 1
                });
                image.animate(animateOption2);
            });
        });
    }
    function Close() {
        $.win.close();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "Dice";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.win = Ti.UI.createWindow({
        top: Alloy.Globals.WindowTop,
        backgroundColor: "#404141",
        id: "win"
    });
    $.__views.win && $.addTopLevelView($.__views.win);
    $.__views.TitleBar = Ti.UI.createView({
        top: "0dp",
        backgroundImage: "/bg_title.png",
        backgroundColor: "#2B2B2B",
        height: "40dp",
        id: "TitleBar"
    });
    $.__views.win.add($.__views.TitleBar);
    $.__views.__alloyId50 = Ti.UI.createView({
        height: "100%",
        width: Ti.UI.SIZE,
        left: "0dp",
        id: "__alloyId50"
    });
    $.__views.TitleBar.add($.__views.__alloyId50);
    Close ? $.__views.__alloyId50.addEventListener("click", Close) : __defers["$.__views.__alloyId50!click!Close"] = true;
    $.__views.btnBack = Ti.UI.createImageView({
        image: "/ic_back.png",
        width: "20dp",
        left: "10dp",
        height: Ti.UI.SIZE,
        id: "btnBack"
    });
    $.__views.__alloyId50.add($.__views.btnBack);
    $.__views.__alloyId51 = Ti.UI.createLabel({
        color: "#fff",
        font: {
            fontSize: "18dp"
        },
        text: "骰子",
        id: "__alloyId51"
    });
    $.__views.TitleBar.add($.__views.__alloyId51);
    $.__views.MainWin = Ti.UI.createView({
        top: "40dp",
        id: "MainWin"
    });
    $.__views.win.add($.__views.MainWin);
    $.__views.__alloyId52 = Ti.UI.createImageView({
        id: "__alloyId52",
        top: "20dp",
        image: "/bobing_bowl.png"
    });
    $.__views.MainWin.add($.__views.__alloyId52);
    $.__views.Test = Ti.UI.createView({
        id: "Test"
    });
    $.__views.MainWin.add($.__views.Test);
    $.__views.background = Ti.UI.createView({
        width: Ti.UI.FILL,
        height: Ti.UI.FILL,
        left: "0",
        top: "0",
        right: "0",
        bottom: "0",
        opacity: 0,
        zIndex: 10,
        backgroundColor: "#000",
        id: "background"
    });
    $.__views.MainWin.add($.__views.background);
    $.__views.reImg1 = Ti.UI.createImageView({
        id: "reImg1",
        top: "80dp",
        visible: "false",
        left: "50dp",
        width: "40dp",
        height: Ti.UI.SIZE,
        image: "/d1.png"
    });
    $.__views.MainWin.add($.__views.reImg1);
    $.__views.reImg2 = Ti.UI.createImageView({
        id: "reImg2",
        top: "80dp",
        visible: "false",
        left: "90dp",
        width: "40dp",
        height: Ti.UI.SIZE,
        image: "/d1.png"
    });
    $.__views.MainWin.add($.__views.reImg2);
    $.__views.reImg3 = Ti.UI.createImageView({
        id: "reImg3",
        top: "80dp",
        visible: "false",
        left: "130",
        width: "40dp",
        height: Ti.UI.SIZE,
        image: "/d1.png"
    });
    $.__views.MainWin.add($.__views.reImg3);
    $.__views.reImg4 = Ti.UI.createImageView({
        id: "reImg4",
        top: "120dp",
        visible: "false",
        left: "50dp",
        width: "40dp",
        height: Ti.UI.SIZE,
        image: "/d1.png"
    });
    $.__views.MainWin.add($.__views.reImg4);
    $.__views.reImg5 = Ti.UI.createImageView({
        id: "reImg5",
        top: "120dp",
        visible: "false",
        left: "130",
        width: "40dp",
        height: Ti.UI.SIZE,
        image: "/d1.png"
    });
    $.__views.MainWin.add($.__views.reImg5);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var image = UI.C("image", {
        image: "/rotate-static.png"
    });
    image.Bind("click", function() {
        Start();
    });
    $.win.add(image);
    __defers["$.__views.__alloyId50!click!Close"] && $.__views.__alloyId50.addEventListener("click", Close);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;