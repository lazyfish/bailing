function Controller() {
    function GoInfo() {
        UI.Open("CardDetail", {
            id: data.Id,
            NextExperience: data.NextExperience
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "CardRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.CardRow = Ti.UI.createTableViewRow({
        id: "CardRow"
    });
    $.__views.CardRow && $.addTopLevelView($.__views.CardRow);
    $.__views.__alloyId28 = Ti.UI.createView({
        layout: "vertical",
        height: Ti.UI.SIZE,
        id: "__alloyId28"
    });
    $.__views.CardRow.add($.__views.__alloyId28);
    GoInfo ? $.__views.__alloyId28.addEventListener("click", GoInfo) : __defers["$.__views.__alloyId28!click!GoInfo"] = true;
    $.__views.row = Ti.UI.createView({
        height: Ti.UI.SIZE,
        left: "10dp",
        top: "10dp",
        right: "10dp",
        id: "row"
    });
    $.__views.__alloyId28.add($.__views.row);
    $.__views.face = Ti.UI.createImageView({
        height: "50dp",
        left: "0dp",
        top: "0dp",
        width: 70,
        borderWeight: "1dp",
        borderRadius: 10,
        borderColor: "#bbb",
        id: "face"
    });
    $.__views.row.add($.__views.face);
    $.__views.__alloyId29 = Ti.UI.createView({
        layout: "vertical",
        left: "80dp",
        height: Ti.UI.SIZE,
        id: "__alloyId29"
    });
    $.__views.row.add($.__views.__alloyId29);
    $.__views.title = Ti.UI.createLabel({
        color: "#333",
        width: Ti.UI.FILL,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        left: 0,
        font: {
            fontSize: "16dp",
            fontWeight: "normal"
        },
        id: "title"
    });
    $.__views.__alloyId29.add($.__views.title);
    $.__views.vendor = Ti.UI.createLabel({
        font: {
            width: Ti.UI.FILL,
            fontSize: "14dp"
        },
        left: 0,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        id: "vendor"
    });
    $.__views.__alloyId29.add($.__views.vendor);
    $.__views.discount = Ti.UI.createLabel({
        font: {
            width: Ti.UI.FILL,
            fontSize: "14dp"
        },
        left: 0,
        textAlign: Ti.UI.TEXT_ALIGNMENT_LEFT,
        color: "#666",
        id: "discount"
    });
    $.__views.__alloyId29.add($.__views.discount);
    $.__views.__alloyId30 = Ti.UI.createView({
        height: "1dp",
        top: "10dp",
        backgroundColor: "#999",
        id: "__alloyId30"
    });
    $.__views.__alloyId28.add($.__views.__alloyId30);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var UI = require("UI");
    var args = arguments[0] || {};
    var data = args.data;
    $.face.setImage(Alloy.Globals.WebUrl + data.Face);
    $.vendor.setText(String.format("商家：%s", data.VendorName));
    $.discount.setText(String.format("享受折扣：%s折", data.Discount));
    __defers["$.__views.__alloyId28!click!GoInfo"] && $.__views.__alloyId28.addEventListener("click", GoInfo);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;