var Ajax = require("Ajax");

var args = arguments[0] || {};
var url = args.url || '';
var index = args.index||1;
var itemFun = args.itemFun || '';
var finishFun = args.finishFun || '';

function LoadMore(){
	var newUrl = ""
	index += 1;
	if (url.indexOf("?") ==-1) {
		newUrl =url+ "?NowPage=" + index;
	}
	else{
		newUrl =url+ "&NowPage=" + index;
	}
	Ajax.json(newUrl,itemFun,finishFun)
}