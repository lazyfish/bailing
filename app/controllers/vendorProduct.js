var windowGround;
Init();
function Init() {
	windowGround = Ti.UI.createView({
		width : Ti.UI.FILL,
		height : Ti.UI.FILL,
		left : "0",
		top : "0",
		right : "0",
		bottom : "0",
		opacity : 0,
		zIndex : 10,
		visible : false,
		backgroundColor : "#000"
	})
	windowGround.addEventListener("click", function() {
		windowGround.animate({
			opacity : 0,
			visible : false,
			duration : 500
		})
		typeWin.animate({
			opacity : 0,
			visible : false,
			duration : 500
		})
		childTypeWin.animate({
			opacity : 0,
			visible : false,
			duration : 500
		})
	})
	$.win.add(windowGround)
	BindTable()
}

function BindTable() {
	var data = [];
	for (var i = 0; i < 10; i++) {
		data.push(Alloy.createController("vendorProRow", {
			face : "/images/ad1.jpg",
			title : "Row" + i,
			content : "Content" + i
		}).getView())
	};
	$.table.setData(data)
}

var typeWin = Ti.UI.createView({
	backgroundColor : "#f3f3f3",
	width : "150dp",
	left : "5dp",
	height : "220dp",
	top : "70dp",
	keepScreenOn : true,
	opacity : 0,
	visible : false,
	zIndex : 20
})
var childTypeWin = Ti.UI.createView({
	backgroundColor : "#ccc",
	width : "150dp",
	left : "155dp",
	height : "220dp",
	top : "70dp",
	opacity : 0,
	visible : false,
	zIndex : 20
})
$.win.add(typeWin)
$.win.add(childTypeWin)
var data = [], childData = []
for (var i = 0; i < 7; i++) {
	data.push(Ti.UI.createTableViewRow({
		title : "Type" + i,
		height : "30dp",
		color : "#333",
		font : {
			fontSize : "14dp"
		}
	}))
	childData.push(Ti.UI.createTableViewRow({
		title : "ChildType" + i,
		height : "30dp",
		color : "#333",
		font : {
			fontSize : "14dp"
		}
	}))
	if (i == 0) {
		data[i].setBackgroundColor("#ccc")
	};
};

var lastTypeIndex = 0;
var listView = Ti.UI.createTableView({
	separatorColor : "#bbb",
	backgroundColor : "#f3f3f3",
	data : data
})
listView.addEventListener("click", function(e) {
	var lastRow = e.section.getRows()[lastTypeIndex];
	lastRow.setBackgroundColor("#f3f3f3");
	var current = e.row;
	current.setBackgroundColor("#ccc");

	listView.updateRow(lastTypeIndex, lastRow);
	listView.updateRow(e.index, current);
	lastTypeIndex = e.index;
})
var listView2 = Ti.UI.createTableView({
	separatorColor : "#ccc",
	backgroundColor : "#ccc",
	data:childData
})
typeWin.add(listView);
childTypeWin.add(listView2);
//弹窗
function ShowWindow() {
	windowGround.animate({
		opacity : 0.4,
		visible : true,
		duration : 500
	})
	Ti.API.info("弹窗")
}

function ShowTypeWin() {
	ShowWindow()
	typeWin.animate({
		opacity : 1,
		visible : true,
		duration : 500
	})
	childTypeWin.animate({
		opacity : 1,
		visible : true,
		duration : 500
	})
}
function GotoDetail(e){
	Alloy.createController("product").getView().open();
}

function Close(e) {
	$.win.close()
}
