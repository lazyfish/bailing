var Ajax = require("Ajax");
var UI = require("UI");
var DateUtility = require("DateUtility");


var url = Alloy.Globals.ApiUrl + "/history.ashx?action=list&idguid=" + Alloy.Globals.LoginToken;
Ti.API.debug(url)
Ajax.json(url, function(item) {
	return UI.Controller("HistoryRow", {
		id: item.id,
		pid: item.Pid,
		price: item.Price,
		name: item.ProName,
		time: item.AddTime,
		face: item.CoverPicture
	})
},function(data){
	$.table.setData(data);
})
function Close(e) {
	$.win.close()
}

