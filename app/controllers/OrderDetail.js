var Ajax = require("Ajax");
var UI = require("UI");
var DateUtility = require("DateUtility");

var args = arguments[0] || {};
var id = args.id || '';
var sn = args.sn || '';
var price = args.price || '';
var OrderState = args.OrderState || '';
var time = args.time || '';
var url = '';
var moreView = null;
var dataList = new Array();

$.sn.text = sn;
$.sum.text = String.format("总额:%.2f元", parseFloat(price));
$.time.text = DateUtility.ToString(time,"yyyy-MM-dd HH:mm:ss");;

url = Alloy.Globals.ApiUrl + "/order.ashx?action=orderdetail&orderid=" + id;
Ti.API.info(url)
if (!moreView) {
	moreView = UI.Controller("MoreRow", {
		url: url,
		index: 0,
		itemFun: LoadItem,
		finishFun: LoadFinish
	});
};
Ajax.json(url, LoadItem,LoadFinish)
function Close(e) {
	$.win.close()
}

function LoadItem(item) {
	var dataItem = UI.Controller("OrderDetailRow", {
		id: item.id,
		pid: item.Pid,
		num: item.Amount,
		price: item.Price,
		name: item.ProName,
		face: item.CoverPicture,
		state:item.State,
		password:item.Password,
		OrderState:OrderState
	});
	dataList.push(dataItem)
	return dataItem;
}

function LoadFinish(data, total) {
	$.table.setData(dataList)
	if (parseInt(total) > dataList.length) {
		$.table.appendRow(moreView)
	};
}
