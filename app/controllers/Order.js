var UI = require("UI");
var Ajax = require("Ajax");
var args = arguments[0] || {};
var State = args.State || '';
var url = '';
var moreView = null;
var dataList = new Array();

if (State == 1) {
	$.Title.text = "未付款的订单"
} else {
	$.Title.text = "已付款的订单"
}

url = Alloy.Globals.ApiUrl + "/order.ashx?action=list&idguid=" + Alloy.Globals.LoginToken + "&State=" + State;
Ti.API.info(url)
if (!moreView) {
	moreView = UI.Controller("MoreRow", {
		url: url,
		index: 0,
		itemFun: LoadItem,
		finishFun: LoadFinish
	});
};
Ajax.json(url, LoadItem, LoadFinish)

function LoadItem(item) {
	var dataItem = UI.Controller("OrderRow", {
		id: item.id,
		sn: item.Sn,
		price: item.Sum,
		time: item.CreateTime,
		OrderState:State
	})
	dataList.push(dataItem)
	return dataItem;
}

function LoadFinish(data, total) {
	$.table.setData(dataList)
	if (parseInt(total) > dataList.length) {
		$.table.appendRow(moreView)
	};
}

function Close(e) {
	$.win.close()
}