var Ajax = require("Ajax");
var UI = require("UI");
var Dialog = require("Dialog");

function Close(e) {
	$.win.close()
}

function Login(e) {
	UI.Open("Login")
	$.win.close();
}

function Submit(e) {
	//验证信息合法
	if ($.txtUserName.value.length < 5 && $.txtUserName.value.length > 15) {
		alert("用户名应多于5个字且少于15个字")
		return false;
	};
	if ($.txtPwd.value != $.txtPwd2.value) {
		alert("两次输入的密码不匹配，请重新输入")
		return false;
	};

	var param = {
		UserName: $.txtUserName.value,
		Password: $.txtPwd.value,
		Tel: $.txtTel.value
	}
	Ajax.post(Alloy.Globals.ApiUrl + "/user.ashx?action=reg", param, function(data) {
		try {
			if (data == "exist user") {
				alert("此用户已存在，请换个用户名!")
			} else {
				var users = Alloy.createCollection('UserModel');
				var model;
				users.fetch();
				if (users.length > 0) {
					model = users.at(0);
					model.set({
						Token: data
					})
				} else {
					model = Alloy.createModel("UserModel", {
						Token: data
					})
					users.add(model)
				}
				model.save();
				Alloy.Globals.LoginToken = data
				var prompt = new Dialog.prompt();
				prompt.Show("注册成功！", 2000)
				setTimeout(function() {
					$.win.close()
				}, 2000);
			}
		} catch (e) {
			alert(e.message)
		}
	})
}