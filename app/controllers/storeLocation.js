var args = arguments[0] || {};
var longitude = args.longitude;
var latitude = args.latitude;

Ti.Geolocation.purpose = 'Get Current Location';
Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
if (Ti.Geolocation.locationServicesEnabled) {
	Ti.Geolocation.distanceFilter = 10;
	Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_NETWORK;

} else {
	alert('Please enable location services');
}
Ti.Geolocation.getCurrentPosition(function(e) {
	var url = Alloy.Globals.WebUrl + "/mobile/map.aspx";
	if (e.success) {
		// url +=String.format("?long=%s&lat=%s&tolong=%s&tolat=%s",e.coords.longitude,e.coords.latitude,longitude,latitude) ;
		url += "?long=" + e.coords.longitude + "&lat=" + e.coords.latitude + "&tolong=" + longitude + "&tolat=" + latitude;
	};
	Ti.API.info(url)
	$.MapView.url = url;
})

$.MapView.addEventListener('beforeload', function(e) {})

function Close(e) {
	$.win.close()
}