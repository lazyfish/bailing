var Ajax = require("Ajax");
var UI = require("UI");
var Dialog = require("Dialog");
var Utility = require("Utility");
var args = arguments[0] || {};
var id = args.id || '';
var name = args.name || '';
var hasLoadedPro = false;
var hasLoadedActivity = false;
var longitude = 0;
var latitude = 0;
var vendorId = ""
var discount = 100;
var vipName = "";
var activityUrl = '';
var activityMoreView = null;
var activityDataList = new Array();
var productRrl = '';
var productMoreView = null;
var productDataList = new Array();

$.Title.setText(name);
ReadStoreInfo()
BindActivity()


function ReadStoreInfo() {
	var url = Alloy.Globals.ApiUrl + "/store.ashx?action=info&id=" + id
	Ajax.json(url, function(item) {
		longitude = item.CoordinateLng
		latitude = item.CoordinateLat
		vendorId = item.Parent;
		$.Introduce.html = Utility.GetWebView(item.StoreInfo);
		var pics = item.StorePicUrl.split(',');
		if (pics[0]) {
			$.Faces.image = Alloy.Globals.WebUrl + pics[0]
		};
		//获取当前用户折扣
		if (Alloy.Globals.LoginToken) {
			var url = Alloy.Globals.ApiUrl + "/user.ashx?action=getLevelByVendor&idguid=" + Alloy.Globals.LoginToken + "&vendor=" + vendorId
			Ajax.get(url, function(data) {
				var json = JSON.parse(data)
				if (json.length > 0) {
					vipName = json[0].name
					discount = json[0].discount
				};
				BindProduct();
			})
		};
	});
}


function OpenMap(e) {
	UI.Open("storeLocation", {
		longitude: longitude,
		latitude: latitude
	})
}

function Close(e) {
	$.win.close()
}

function ShowIntroduce(e) {
	$.scrolView.setCurrentPage(2);
	$.border3.setBackgroundColor("#bbb")
	$.border2.setBackgroundColor("#bbb")
	$.border1.setBackgroundColor("#B13531")
}

function ShowActivity(e) {
	$.scrolView.setCurrentPage(0);
	$.border2.setBackgroundColor("#B13531")
	$.border1.setBackgroundColor("#bbb")
	$.border3.setBackgroundColor("#bbb")
}

function ShowProduct(e) {
	$.scrolView.setCurrentPage(1);
	$.border3.setBackgroundColor("#B13531")
	$.border1.setBackgroundColor("#bbb")
	$.border2.setBackgroundColor("#bbb")
}

function BindProduct() {
	hasLoadedPro = true;
	productUrl = Alloy.Globals.ApiUrl + "/product.ashx?action=getprobystoreid&id=" + id;
	productMoreView = UI.Controller("MoreRow", {
		url: productUrl,
		index: 0,
		itemFun: LoadProductItem,
		finishFun: LoadProductFinish
	});
	Ajax.json(productUrl, LoadProductItem, LoadProductFinish);
}

function LoadProductItem(item, i, length, total) {
	var dataItem = UI.Controller("vendorProRow", {
		id: item.id,
		face: item.CoverPicture,
		title: item.ProName,
		content: item.ProContent,
		Storeid: id,
		vipName: vipName,
		vendorId: vendorId,
		discount: discount,
		price: item.oPrice,
		pics: item.ProPics
	});
	productDataList.push(dataItem)
	return dataItem;
}

function LoadProductFinish(data, total) {
	$.ProductTable.setData(productDataList);
	if (parseInt(total) > productDataList.length) {
		$.ProductTable.appendRow(productMoreView)
	};
}


function BindActivity() {
	hasLoadedActivity = true;
	activityUrl = Alloy.Globals.ApiUrl + "/product.ashx?action=getprobystoreid&t=2&id=" + id;
	Ti.API.debug("BindActivity")
	activityMoreView = UI.Controller("MoreRow", {
		url: activityUrl,
		index: 0,
		itemFun: LoadActivityItem,
		finishFun: LoadActivityFinish
	});
	Ajax.json(activityUrl, LoadActivityItem, LoadActivityFinish);
}

function LoadActivityItem(item, i, length, total) {
	var dataItem = UI.Controller("activityRow", {
		id: item.id,
		face: item.CoverPicture,
		title: item.ProName,
		content: item.ProContent,
		Storeid: id,
		vendorId: vendorId,
		price: item.oPrice,
		pics: item.ProPics
	});
	activityDataList.push(dataItem)
	return dataItem;
}

function LoadActivityFinish(data, total) {
	$.ActivityTable.setData(activityDataList);
	if (parseInt(total) > activityDataList.length) {
		$.ActivityTable.appendRow(activityMoreView)
	};
}

function ChangePage(e) {
	if (e.currentPage == 0) {
		ShowActivity();
	} else if (e.currentPage == 1) {
		ShowProduct();
	} else if (e.currentPage == 2) {
		ShowIntroduce();
	}
}

var haveFav = false;

function Fav() {
	if (!haveFav) {
		var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=add&idguid=" + Alloy.Globals.LoginToken + "&conid=" + id + "&contype=2";
		Ajax.get(url, function(data) {
			if (data == "ok") {
				var prompt = new Dialog.prompt();
				prompt.Show("收藏成功！", 2000)
				$.btnUnFav.setImage("/star_on.png")
			};
		})
	}
}