var Ajax = require("Ajax");

BindTable();

Ti.Geolocation.purpose = 'Get Current Location';
Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
if (Ti.Geolocation.locationServicesEnabled) {
	Ti.Geolocation.distanceFilter = 10;
	Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_NETWORK;

} else {
	alert('请启用您的GPS定位服务');
}

var actInd = Titanium.UI.createActivityIndicator({
	width: Ti.UI.SIZE,
	height: Ti.UI.SIZE,
	color: "#fff",
	message: '获取位置中...'
});
$.win.add(actInd)
actInd.show();

var hasLoad = false;
function BindTable() {
	Titanium.Geolocation.addEventListener('location', function(e) {
		if (!hasLoad) {
			GetTable(e.coords)
		};
	});
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (e.success) {
			if (!hasLoad) {
				GetTable(e.coords)
			};
		}
	})
}

function GetTable(coords) {
	hasLoad = true;
	var url = Alloy.Globals.ApiUrl + "/store.ashx?action=random&lng=" + coords.longitude + "&lat=" + coords.latitude;
	Ajax.json(url, CreateStoreItem, FinishLoadStore)

}


function CreateStoreItem(item, i, length, total) {
	return Alloy.createController("vendorStoreRow", {
		face: Alloy.Globals.WebUrl + item.StorePicUrl,
		title: item.StoreName,
		id: item.id,
		content: item.ContactTel,
		range: item.Range + "米"
	}).getView()
}

function FinishLoadStore(data) {
	$.table.setData(data)
	actInd.hide();
}


function Close(e) {
	$.win.close()
}
