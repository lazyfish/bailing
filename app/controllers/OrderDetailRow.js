var UI = require("UI");
var Ajax = require("Ajax");
var Dialog = require("Dialog");
var args = arguments[0] || {};
var id = args.id;
var pid = args.pid;
var num = args.num;
var price = args.price;
var name = args.name;
var face = args.face;
var state = args.state;
var password = args.password;
var OrderState = args.OrderState || '';

var stateName = "未使用";
$.btnRepeal.setVisible(false);
if (OrderState != 1) {
	switch (parseInt(state)) {
		case 0: // 未使用
			stateName = "未使用";
			$.btnRepeal.setVisible(true);
			break;
		case 1: // 已使用
			stateName = "已使用";
			break;
		case 2: // 申请退款
			stateName = "申请退款";
			break;
		case 3: // 拒绝退款
			stateName = "拒绝退款";
			break;
		case 4: // 退款成功
			stateName = "退款成功";
			break;
	}
};
var number = id;
while (number.length < 8) {
	number = "0" + number;
}


$.face.setImage(Alloy.Globals.WebUrl + face)
$.title.setText(name)
$.price.setText(String.format("购买价:%1$.2f元 \r\n 状态：%2$s \r\n 编号：%3$s \r\n 验证码：%4$s", parseFloat(price), stateName, number, password));

function GotoProduct() {
	UI.Open("product", {
		id: pid
	});
}

function Repeal(e) {
	var url = Alloy.Globals.ApiUrl + "/order.ashx?action=repeal&id=" + id;
	Ajax.get(url, function(data) {
		if (data == "ok") {
			var prompt = new Dialog.prompt();
			prompt.Show("申请成功！", 2000);
			$.price.setText(String.format("购买价:%1$.2f元 \r\n 状态：%2$s \r\n 编号：%3$s \r\n 验证码：%4$s", parseFloat(price), "申请退款", number, password));
		} else {
			alert("此订单无法申请退款");
		}

		$.btnRepeal.setVisible(false);
	})
	e.cancelBubble = true;
	return false;
}