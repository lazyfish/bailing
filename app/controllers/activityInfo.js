var UI = require("UI");
var Ajax = require("Ajax");
var Dialog = require("Dialog");
var Utility = require("Utility");
var args = arguments[0] || {};
var id = args.id || '';
var face = args.face || '';
var title = args.title || '';
var StoreId = args.Storeid || '';
var vendorId = args.vendorId || '';
var content = args.content || '';
var discount = args.discount || '';
var price = args.price || '';

function Close(e) {
	$.win.close()
}

function GotoCar() {
	UI.Open("BuyCar");
}

// Ajax.get(Alloy.Globals.ApiUrl + "/history.ashx?action=add&idguid="+Alloy.Globals.LoginToken+"&pid=" + id,function(){
// })
BindPics();
Ajax.json(Alloy.Globals.ApiUrl + "/product.ashx?action=info&id=" + id, function(item) {
	$.Title.setText(item.ProName);
	$.Price.setText(String.format("参与价：%.2f元", parseFloat(item.Price)))
	SetProductContent(item.ProContent)
})

//获取购物车件数
if (Alloy.Globals.LoginToken) {
	Ajax.get(Alloy.Globals.ApiUrl + "/car.ashx?action=count&idguid=" + Alloy.Globals.LoginToken, function(data) {
		$.btnCarNumber.text = data;
	})
};
//设置产品介绍

function SetProductContent(content) {
	$.Introduce.html = Utility.GetWebView(content);
}

function BindPics() {
	var url = Alloy.Globals.ApiUrl + "/product.ashx?action=getpropic&id=" + id;
	Ajax.json(url, function(item) {
		var view = UI.C("view", {
			left: "10dp",
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE
		})
		var pic = UI.C("image", {
			image: Alloy.Globals.WebUrl + item.PictureUrl,
			height: "150dp",
			width: Ti.UI.SIZE
		})
		view.add(pic);
		$.Faces.add(view);
	})
}

var haveFav = false;

function Fav() {
	if (!haveFav) {
		var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=add&idguid=" + Alloy.Globals.LoginToken + "&conid=" + id + "&storeid=" + StoreId + "&contype=1";
		Ajax.get(url, function(data) {
			if (data == "ok") {
				var prompt = new Dialog.prompt();
				prompt.Show("收藏成功！", 2000)
				$.btnUnFav.setImage("/star_on.png")
			};
		})
	}
}

function Buy() {
	var url = Alloy.Globals.ApiUrl + "/car.ashx?action=add";
	var param = {
		idguid: Alloy.Globals.LoginToken,
		storeid: StoreId,
		vendorId: vendorId,
		pid: id,
		Discount: 1,
		SelNum: 1
	}
	Ajax.post(url, param, function(data) {
		if (data == "ok") {
			var prompt = new Dialog.prompt();
			$.btnCarNumber.text = parseInt($.btnCarNumber.text) + 1;
			prompt.Show("已放入购物车！", 2000)
		};
	})
}

function BindComment() {
	var url = Alloy.Globals.ApiUrl + "/comment.ashx?action=list&proid=" + id + "&storeid=" + StoreId;
	Ajax.json(url, function(item, i, length, total) {
		return Alloy.createController("commentRow", {
			title: item.username,
			content: item.cm_content,
			date: item.addtime
		}).getView()
	}, function(data) {
		$.CommendTable.setData(data)
	})
}

function ShowIntroduce(e) {
	$.scrolView.setCurrentPage(0);
	$.border2.setBackgroundColor("#bbb")
	$.border1.setBackgroundColor("#B13531")
}

function ShowCommend(e) {
	$.scrolView.setCurrentPage(1);
	$.border2.setBackgroundColor("#B13531")
	$.border1.setBackgroundColor("#bbb")
	BindComment();
}

function ChangePage(e) {
	if (e.currentPage == 0) {
		$.border2.setBackgroundColor("#bbb")
		$.border1.setBackgroundColor("#B13531")
	} else if (e.currentPage == 1) {
		$.border2.setBackgroundColor("#B13531")
		$.border1.setBackgroundColor("#bbb")
	}
}

function Reward() {
	UI.Open('Dial', {
		id: id,
		sum: price,
		storeId: StoreId,
		vendorId: vendorId
	})
}