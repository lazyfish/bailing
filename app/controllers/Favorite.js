var UI = require("UI");
var Ajax = require("Ajax");
var args = arguments[0] || {};
var sum = 0.00;
var moreView = null;
var dataList = new Array();
var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=list&idguid=" + Alloy.Globals.LoginToken + "&t=1";
Ti.API.info(url)
if (!moreView) {
	moreView = UI.Controller("MoreRow", {
		url: url,
		index: 0,
		itemFun: LoadItem,
		finishFun: LoadFinish
	});
};
Ajax.json(url, LoadItem, LoadFinish)

function Close(e) {
	$.win.close()
}

function RemoveRow(e) {
	$.table.deleteRow(e.index)
}

function LoadItem(item) {
	var dataItem = UI.Controller("FavoriteProRow", {
			id      : item.pid,
			face    : item.CoverPicture,
			title   : item.ProName,
			content : item.ProContent,
			price   : item.oPrice,
			pics    : item.ProPics,
			storeId    : item.StoreId,
			delFun  : RemoveRow
		});
	dataList.push(dataItem)
	return dataItem;
}

function LoadFinish(data, total) {
	$.table.setData(dataList)
	if (parseInt(total) > dataList.length) {
		$.table.appendRow(moreView)
	};
}