var UI = require("UI");
var Ajax = require("Ajax");
var Dialog = require("Dialog");
var args = arguments[0] || {};
var sum = 0.00;
var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=list&idguid=" + Alloy.Globals.LoginToken + "&t=2";
var moreView = null;
var dataList = new Array();
Ti.API.info(url)
if (!moreView) {
	moreView = UI.Controller("MoreRow", {
		url: url,
		index: 0,
		itemFun: LoadItem,
		finishFun: LoadFinish
	});
};
Ajax.json(url, LoadItem, LoadFinish)

function Close(e) {
	$.win.close()
}

function RemoveRow(e) {
	$.table.deleteRow(e.index)
}

function LoadItem(item) {
	var dataItem = UI.Controller("FavoriteStoreRow", {
		face: Alloy.Globals.WebUrl + item.StorePicUrl,
		title: item.StoreName,
		id: item.id,
		storeId: item.storeId,
		content: item.ContactTel,
		delFun: RemoveRow
	});
	dataList.push(dataItem)
	return dataItem;
}

function LoadFinish(data, total) {
	$.table.setData(dataList)
	if (parseInt(total) > dataList.length) {
		$.table.appendRow(moreView)
	};
}