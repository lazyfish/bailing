var Ajax = require("Ajax");
var Dialog = require("Dialog");

Ajax.json(Alloy.Globals.ApiUrl + "/user.ashx?action=getuserbyguid&IdGUID=" + Alloy.Globals.LoginToken, function(item) {
	$.tfName.value = item.RealName;
	$.tfPhone.value = item.Tel;
	$.tfEmail.value = item.Email;
})

function Modify() {
	if (!/^(((\(\d{2,3}\))|(\d{3}\-))?13\d{9})|(((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$)$/.test($.tfPhone.value)) {
		alert("请输入正确的联系电话");
		return false;
	};
	if (!/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test($.tfEmail.value)) {
		alert("请输入正确的邮箱地址");
		return false;
	};
	var param = {
		idguid: Alloy.Globals.LoginToken,
		RealName: $.tfName.value,
		Email: $.tfEmail.value,
		Tel: $.tfPhone.value,
	};
	Ti.API.debug("data",JSON.stringify(param))
	Ajax.post(Alloy.Globals.ApiUrl + "/user.ashx?action=update",param , function(data) {
		if (data == 'exist user') {
			alert("此用户名已存在，请换个用户名吧。")
		}
		else {
			var prompt = new Dialog.prompt();
			prompt.Show("修改成功！", 2000)
			Close();
		}
	})
}

function Close(e) {
	$.win.close()
}


// HashMap<String, String> params = new HashMap<String, String>();
// 			params.put("idguid", getLoginUserToken());
// 			params.put("RealName", et_real_name.getvalue().toString());
// 			params.put("Email", et_email.getvalue().toString());
// 			params.put("Tel", et_tel.getvalue().toString());
// 			params.put("flag", "true");

// String url = _convalue.getString(R.string.api_url) + "/user.ashx?action=";
// 		if(Boolean.valueOf(params[0].get("flag"))){
// 			url += "update";
// 		} else {
// 			url += "reg";
// 		}
// 		params[0].remove("flag");
// 		String result = WebUtility.Post(url, params[0]);
// 		if (result.endsWith("exist user")) {
// 			_postEvent.onError("此用户已存在，请换个用户名吧。");
// 		}
// 		else{
// 			_postEvent.onSuccess(result);
// 		}