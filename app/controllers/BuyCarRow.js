var UI = require("UI");
var Ajax = require("Ajax");
var Dialog = require("Dialog");
var args = arguments[0] || {};
var id = args.id;
var pid = args.pid;
var num = args.num;
var price = args.price;
var name = args.name;
var face = args.face;
var type = args.type;
var delFun = args.delFun;

$.face.setImage(Alloy.Globals.WebUrl + face)
$.title.setText(name)
$.price.setText(String.format("售价:%.2f元  数量:%d", parseFloat(price), parseInt(num)));

function GotoProduct() {
	if (type == "1") {
		UI.Open("product", {
			id: pid
		});
	} else {
		UI.Open("activityInfo", {
			id: pid
		});
	}
}

function Del(e) {
	var url = Alloy.Globals.ApiUrl + "/car.ashx?action=del&id=" + id;
	Ajax.get(url, function(data) {
		if (data == "ok") {
			var prompt = new Dialog.prompt();
			prompt.Show("删除成功！", 2000)
			delFun(e);
		};
	})
	e.cancelBubble = true;
}