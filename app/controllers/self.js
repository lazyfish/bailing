var Ajax = require("Ajax");
var UI = require("UI");
var NextExperience = 0;
ReadUser();

function ReadUser() {
	Ajax.json(Alloy.Globals.ApiUrl + "/user.ashx?action=getuserbyguid&IdGUID=" + Alloy.Globals.LoginToken, function(item) {
		$.UserName.text = item.RealName;
		$.Experience.text = item.Experience;
		$.Grade.text = item.Grade;
		NextExperience = item.Experience;
	})
	Ajax.get(Alloy.Globals.ApiUrl + "/user.ashx?action=HasSign&idguid=" + Alloy.Globals.LoginToken,function(data){
		if (data=="True") {
			$.SignButtonText.text="已签到";
		}
	})
}
var hasSign = false;

function GotoCar() {
	UI.Open("BuyCar");
}

function GotoUnPayOrder() {;
	UI.Open("Order", {
		State: "1"
	});
}

function GotoFinishOrder() {
	UI.Open("Order", {
		State: "7"
	});
}

function GotoFavorite() {
	UI.Open("Favorite")
}

function GotoFavoriteStore() {
	UI.Open("FavoriteStore");;
}

function GotoUnComment() {
	UI.Open("ProComment");
}

function GooModifyUser() {
	UI.Open("SelfModify");
}

function GoMyCard() {
	UI.Open("MyCard");
}

function GoBLCard() {
	UI.Open("CardDetail", {
		NextExperience: NextExperience
	});
}

function Sign() {
	if (!hasSign) {
		var url = Alloy.Globals.ApiUrl + "/user.ashx?action=Sign&idguid=" + Alloy.Globals.LoginToken;
		Ajax.get(url, function(data) {
			ReadUser() ;
			alert("签到成功");
		})
	}
}