var UI = require("UI");
var Dialog = require("Dialog");
var Ajax = require("Ajax");
var args = arguments[0] || {};
var id = args.id;
var vendorId = args.vendorId;
var storeId = args.storeId;
var sum = args.sum;
var url = Alloy.Globals.WebUrl + "/Mobile/dial.aspx?idguiid=" + Alloy.Globals.LoginToken + "&id=" + id + "&r=" + Math.round(Math.random() * 10000);
var closeFun = args.close;
var prompt = new Dialog.prompt();
var loadView = new Dialog.load();
var discount=10;
$.WebView.url = url;

function Close() {
	$.win.close();
}

function BeforeLoad(e) {
	Ti.API.debug("BeforeLoad:" + e.url)
	if (e.url.indexOf('#') != -1) {
		discount = parseInt(e.url.substr(e.url.indexOf('#') + 1));
		Ti.API.debug("discount:" + discount)
		if (discount) {
			if (discount == 10 || discount == 0) {
				$.btnText.setText("无折扣购买");
			} else {
				$.btnText.setText(discount + "折购买");
			}
		};
	};
}

function Load(e) {
}

function Buy() {
	loadView.Show("正在放进购物车中");
	var url = Alloy.Globals.ApiUrl + "/car.ashx?action=add";
	var param = {
		idguid: Alloy.Globals.LoginToken,
		storeid: storeId,
		vendorId: vendorId,
		pid: id,
		Discount: parseFloat(discount) / 10.00,
		SelNum: 1
	}
	Ti.API.debug("Discount",param.Discount,discount,parseFloat(discount) / 10.00);
	Ajax.post(url, param, function(data) {
		loadView.Hide();
		if (data == "ok") {
			// $.btnCarNumber.text = parseInt($.btnCarNumber.text) + 1;
			prompt.Show("已放入购物车！", 2000)
		}
		else{
			prompt.Show(data, 2000)
		}
	})
}

// if (url.lastIndexOf("#") != -1) {
// 				double discount = VarUtil.ParseInt(url.substring(
// 						url.lastIndexOf("#") + 1, url.length()));
// 				Log("折扣："
// 						+ url.substring(url.lastIndexOf("#") + 1, url.length()));
// 				_discount = String.valueOf(discount);
// 				if(_discount.equals("10") || _discount.equals("0")){
// 					btn_submit.setText("无折扣购买");
// 				} else {
// 					btn_submit.setText(_discount + "折购买");
// 				}
// 			}