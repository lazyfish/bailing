var Ajax = require("Ajax");
var Dialog = require("Dialog");
var args = arguments[0] || {};
var pid = args.pid || '';
var storeid = args.storeid || '';

function Close(e) {
	$.win.close()
}

function Submit(e) {
	var url = Alloy.Globals.ApiUrl + "/comment.ashx?action=add&idguid=" + Alloy.Globals.LoginToken;
	var param = {
		content: $.txtContent.value,
		proid: pid,
		storeid: storeid
	};
	Ajax.post(url, param, function(data) {
		var prompt = new Dialog.prompt();
		prompt.Show("评论成功！", 2000)
		setTimeout(function() {
			$.win.close()
		}, 1500)
	})
}