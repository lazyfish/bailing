var Ajax = require("Ajax");
var UI = require("UI");
var ListDialog = require("ListDialog");
var args = arguments[0] || {};
var type = args.type || '' //当前类别
var keyword = args.keyword || '' //当前关键字
var area = ''; //当前地区
var sort = 0;
var typeDialog;
var areaDialog;
var sortDialog = null;
var url = '';
var moreView = null;
var dataList = new Array();


Init();

function Init() {
	LoadIndustryType()
	LoadArea()
	LoadSortItem();
	BindTable()
}

function BindTable() {
	url = Alloy.Globals.ApiUrl + "/store.ashx?action=getverdonlist&t=" + type + "&area=" + area + "&keyword=" + keyword + "&sort=" + sort
	if (!moreView) {
		moreView = UI.Controller("MoreRow", {
			url: url,
			index: 0,
			itemFun: LoadItem,
			finishFun: LoadFinish
		});
	};
	Ti.API.info(url)
	Ajax.json(url, LoadItem, LoadFinish);
}

function LoadItem(item) {
	var dataItem = Alloy.createController("vendorRow", {
		id: item.id,
		face: Alloy.Globals.WebUrl + item.StorePicUrl,
		title: item.StoreName,
		content: ""
	}).getView()
	dataList.push(dataItem)
	return dataItem;
}

function LoadFinish(data, total) {
	$.table.setData(dataList)
	if (parseInt(total) > dataList.length) {
		$.table.appendRow(moreView)
	};
}


//**********加载行业类别*********

//行业顶级分类

function LoadIndustryType() {
	typeDialog = new ListDialog.option();
	typeDialog.Init($.win, {
		left: "5dp",
		top: "70dp"
	}, {
		width: "300dp",
		height: "240dp"
	}, [], []);
	typeDialog.OnRootClick = function(e) {
		if (e.id == 0) {
			type = e.id
			BindTable()
			typeDialog.Hide();
		} else {
			Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + e.id, LoadingIndustryChildItem, LoadedIndustryChildItem)
		}
	}
	typeDialog.OnChildClick = function(e) {
		type = e.id
		BindTable()
		typeDialog.Hide();
	}

	Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=0", LoadingIndustryRootItem, LoadedIndustryRootItem)
}

function LoadingIndustryRootItem(item, i, length, total) {
	return {
		id: item.id,
		name: item.ItemName
	}
}

function LoadedIndustryRootItem(data) {
	data.unshift({
		id: "0",
		name: "全部"
	})
	typeDialog.BindData(data);
	if (data.length > 1) {
		Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + type || data[1].id, LoadingIndustryChildItem, LoadedIndustryChildItem)
	};
	typeDialog.SetCurrentRoot(type);
}

//行业子分类

function LoadingIndustryChildItem(item, i, length, total) {
	return {
		id: item.id,
		name: item.ItemName,
		parent: item.ParentId
	}
}

function LoadedIndustryChildItem(data) {
	data.unshift({
		id: typeDialog._rootId,
		name: "全部"
	})
	typeDialog.BindData(null, data);
}

//**********加载行业类别结束*********
//**********加载排序类别*********
function LoadSortItem() {
	sortDialog = new ListDialog.option();
	left = Alloy.Globals.width / 2;
	sortDialog.Init($.win, {
		left: left,
		top: "70dp"
	}, {
		width: "140dp",
		height: "80dp"
	}, []);
	var data = [{
		id: 0,
		name: "默认排序"
	}, {
		id: 1,
		name: "按人气从高到低"
	}];
	sortDialog.OnRootClick = function(e) {
		sort = e.id;
		BindTable()
		sortDialog.Hide();
	}
	sortDialog.BindData(data)
	sortDialog.SetCurrentRoot(0);
}
//**********加载排序类别结束*********

//************加载地区************

function LoadArea() {
	areaDialog = new ListDialog.option();
	left = Alloy.Globals.width / 2 - 70;
	areaDialog.Init($.win, {
		left: left,
		top: "70dp"
	}, {
		width: "140dp",
		height: "240dp"
	}, []);
	Ajax.json(Alloy.Globals.ApiUrl + "/ct.ashx?action=getarea&code=350500",
		function(item) {
			return {
				id: item.CTCode,
				name: item.CTName
			}
		}, function(data) {
			data.unshift({
				id: '350500',
				name: '全城'
			})
			areaDialog.BindData(data)
		})
	areaDialog.OnRootClick = function(e) {
		area = e.id;
		BindTable()
		areaDialog.Hide();
	}
}
//************加载地区结束************


//**********事件***********

function ShowTypeWin() {
	typeDialog.Show();
	$.SortTab.setBackgroundImage("")
	$.SortTab.setColor("#84868D");
	$.AreaTab.setBackgroundImage("")
	$.AreaTab.setColor("#84868D");
	$.TypeTab.setBackgroundImage("/tool_left.png")
	$.TypeTab.setColor("#fff");
}

function ShowAreaWin() {
	areaDialog.Show();
	$.SortTab.setBackgroundImage("")
	$.SortTab.setColor("#84868D");
	$.AreaTab.setBackgroundImage("/tool_mid.png")
	$.AreaTab.setColor("#fff");
	$.TypeTab.setBackgroundImage("")
	$.TypeTab.setColor("#84868D");
}

function ShowSortWin() {
	sortDialog.Show();
	$.SortTab.setBackgroundImage("/tool_right.png")
	$.SortTab.setColor("#fff");
	$.AreaTab.setBackgroundImage("")
	$.AreaTab.setColor("#84868D");
	$.TypeTab.setBackgroundImage("")
	$.TypeTab.setColor("#84868D");
}

function Close(e) {
	$.win.close()
}

//**********事件结束***********