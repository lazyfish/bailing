var UI = require("UI");
var Ajax = require("Ajax");
var Dialog = require("Dialog");
var DateUtility = require("DateUtility");
var args = arguments[0] || {};
var id = args.id || '';
var pid = args.pid || '';
var time = args.time || '';
var price = args.price || '';
var name = args.name || '';
var face = args.face || '';

Ti.API.debug("face:",Alloy.Globals.WebUrl+ "/uploads/" + face)
$.face.setImage(Alloy.Globals.WebUrl + face)
$.title.setText(name)
$.time.setText(String.format("查看时间：%s",DateUtility.ToString(time,"yyyy-MM-dd HH:mm")));
$.price.setText(String.format("售价:%.2f元", parseFloat(price)));

function GotoProduct() {
	UI.Open("product",{id:pid});
}
