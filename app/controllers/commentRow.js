var args = arguments[0] || {};
var title = args.title || '';
var content = args.content || '';
var date = args.date || new Date();
var dateUtility = require("DateUtility");

$.Title.setText(title);
$.Content.setText(content);
if (date) {
	$.Time.setText(dateUtility.ToString(date, "yyyy-MM-dd HH:mm:ss"));
}
else{
	$.Time.setText('----');
}