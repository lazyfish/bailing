var args = arguments[0] || {};
var id = args.id || '';
var face = args.face || '';
var title = args.title || '';
var content = args.content || '';
var price = args.price || '';
var pics = args.pics || '';
var storeId = args.storeId || '';


Ti.API.info(id)
$.face.setImage(Alloy.Globals.WebUrl + face)
$.title.setText(title)
$.content.setText(String.format("会员价：%.2f元",parseFloat(price)))

function GotoDetail(e) {
	Alloy.createController("product", {
		id: id,
		face: pics,
		title: title,
		StoreId: storeId,
		content: content
	}).getView().open();
}

function Del(e) {
	var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=del&id=" + id;
	Ajax.get(url, function(data) {
		if (data == "ok") {
			var prompt = new Dialog.prompt();
			prompt.Show("删除成功！", 2000)
			delFun(e);
		};
	})
	e.cancelBubble = true;
}