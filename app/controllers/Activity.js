var Ajax = require("Ajax");
var UI = require("UI");

var pageIndex = 1;

BindActivity() ;

function BindActivity() {
	var url = Alloy.Globals.ApiUrl + "/product.ashx?action=getgroup&psize=6&NowPage="+pageIndex;
	Ajax.json(url, function(item, i, length, total) {
		return UI.Controller("activityRow", {
			id: item.id,
			face: item.CoverPicture,
			title: item.ProName,
			content: item.ProContent,
			Storeid: item.StoreId,
			vendorId:item.VendorId,
			price: item.Price,
			pics: item.ProPics
		})
	}, function(data) {
		$.table.setData(data)
	})
}