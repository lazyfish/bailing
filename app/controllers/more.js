var Ajax = require("Ajax");
var tel = "";
Ajax.json(Alloy.Globals.ApiUrl + "/system.ashx",function(item){
	$.ContactTitle.setText(String.format("联系客户：%s",item.Tel ));
	tel =item.Tel;
})

function GotoWeiboShare(e) {
	Alloy.createController("ShareWeibo").getView().open();
}

function GotoDice() {
	Alloy.createController("Dice").getView().open();
}
function GoHistory(){
	Alloy.createController("History").getView().open();
}
function GoSuggestion(){
	Alloy.createController("Suggestion").getView().open();
}

function CallTel(){
	Ti.Platform.openURL('tel:' + tel);
}

function GoAbout(){
	Alloy.createController("About").getView().open();
}
function GoHelp(){
	Alloy.createController("Help").getView().open();
}