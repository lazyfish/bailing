var Ajax = require("Ajax");
var UI = require("UI");
var args = arguments[0] || {};
var type = args.type||'' //当前类别
var keyword = args.keyword||'' //当前关键字

var url = Alloy.Globals.ApiUrl + "/product.ashx?action=search&keyword"+keyword;
Ajax.json(url, function(item, i, length, total) {
	Ti.API.info(item.ProContent)
	return UI.Controller("vendorProRow", {
		id: item.id,
		face: item.CoverPicture,
		title: item.ProName,
		content: item.ProContent,
		price: item.oPrice,
		pics: item.ProPics
	})
}, function(data) {
	$.table.setData(data)
})

function Close(e) {
	$.win.close()
}