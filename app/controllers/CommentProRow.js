var args = arguments[0] || {};
var id = args.id || '';
var face = args.face || '';
var title = args.title || '';
var content = args.content || '';
var price = args.price || '';
var pics = args.pics || '';
var storeId = args.storeId || '';


Ti.API.info(id)
$.face.setImage(Alloy.Globals.WebUrl + face)
$.title.setText(title)
$.content.setText(String.format("会员价：%.2f元",parseFloat(price)))

function GotoDetail(e) {
	Alloy.createController("product", {
		id: id,
		face: pics,
		title: title,
		Storeid: storeId,
		content: content
	}).getView().open();
}
function Comment(e){
	e.cancelBubble = true;
	Alloy.createController("CommentSubmit", {
		pid:id,
		storeid:storeId
	}).getView().open();
}