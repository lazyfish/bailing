var UI = require("UI");
var Ajax = require("Ajax");
var Dialog = require("Dialog");


function Close(e) {
	$.win.close()
}

function Register(e) {
	UI.Open("Register")
	$.win.close();
}

function Submit(e) {
	var param = {
		name: $.UserName.value,
		password: $.Password.value
	}
	Ajax.post(Alloy.Globals.ApiUrl + "/user.ashx?action=login", param, function(data) {
		try {
			if (data == "no exist") {
				alert("当前用户不存在!")
			} else if (data == "error") {
				alert("用户名或密码错误，请重新输入!")
			} else {
				var users = Alloy.createCollection('UserModel');
				var model;
				users.fetch();
				if (users.length > 0) {
					model = users.at(0);
					model.set({
						Token: data
					})
				} else {
					model = Alloy.createModel("UserModel", {
						Token: data
					})
					users.add(model)
				}
				model.save();
				Alloy.Globals.LoginToken = data
				var prompt = new Dialog.prompt();
				prompt.Show("登录成功！",2000)
				// alert("登录成功！")
				setTimeout(function() {
					$.win.close()
				}, 2000);
			}
		} catch (e) {
			alert(e.message)
		}
	})
}