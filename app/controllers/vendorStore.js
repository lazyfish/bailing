var Ajax = require("Ajax");
var ListDialog = require("ListDialog");
var UI = require("UI");
var args = arguments[0] || {};
var id = args.id || '';
var vendor = args.vendor || '';
var windowGround;
var areaDialog;
var sortDialog;
var url = '';
var moreView = null;
var dataList = new Array();

$.Vendor.setText(vendor);
Init();

function Init() {
	LoadArea();
	LoadSortItem();
	BindTable();
}

var actInd = Titanium.UI.createActivityIndicator({
	width: Ti.UI.SIZE,
	height: Ti.UI.SIZE,
	color: "#fff",
	message: '获取位置中...'
});
$.win.add(actInd);
actInd.show();

function BindTable() {
	Ti.Geolocation.getCurrentPosition(function(e) {
		url = Alloy.Globals.ApiUrl + "/store.ashx?action=getstorelist&storeid=" + id + "&lng=" + e.coords.longitude + "&lat=" + e.coords.latitude;
		if (!moreView) {
			moreView = UI.Controller("MoreRow", {
				url: url,
				index: 0,
				itemFun: LoadItem,
				finishFun: LoadFinish
			});
		};
		Ti.API.info(url);
		Ajax.json(url, LoadItem, LoadFinish);
	});
}

function LoadItem(item) {
	var dataItem = Alloy.createController("vendorStoreRow", {
		id: item.id,
		face: Alloy.Globals.WebUrl + item.StorePicUrl,
		title: item.StoreName,
		content: item.ContactTel,
		range: item.Range
	}).getView();
	dataList.push(dataItem)
	return dataItem;
}

function LoadFinish(data, total) {
	$.table.setData(dataList);
	if (parseInt(total) > dataList.length) {
		$.table.appendRow(moreView)
	};
	actInd.hide();
}
//************加载地区************

function LoadArea() {
	areaDialog = new ListDialog.option();
	left = Alloy.Globals.width / 2;
	areaDialog.Init($.win, {
		left: left,
		top: "30dp"
	}, {
		width: "140dp",
		height: "240dp"
	}, []);
	Ajax.json(Alloy.Globals.ApiUrl + "/ct.ashx?action=getarea&code=350500",
		function(item) {
			return {
				id: item.CTCode,
				name: item.CTName
			};
		}, function(data) {
			data.unshift({
				id: '350500',
				name: '全城'
			});
			areaDialog.BindData(data);
		});
	areaDialog.OnRootClick = function(e) {
		area = e.id;
		BindTable();
		areaDialog.Hide();
	};
}
//************加载地区结束************

//**********加载排序类别*********
function LoadSortItem() {
	sortDialog = new ListDialog.option();
	left = Alloy.Globals.width / 2;
	sortDialog.Init($.win, {
		left: left,
		top: "70dp"
	}, {
		width: "140dp",
		height: "80dp"
	}, []);
	var data = [{
		id: 0,
		name: "默认排序"
	}, {
		id: 1,
		name: "按人气从高到低"
	}];
	sortDialog.OnRootClick = function(e) {
		sort = e.id;
		BindTable()
		sortDialog.Hide();
	}
	sortDialog.BindData(data)
	sortDialog.SetCurrentRoot(0);
}
//**********加载排序类别结束*********


function Close(e) {
	$.win.close();
}

//**********事件***********

function ShowAreaWin() {
	areaDialog.Show();
	$.SortTab.setBackgroundImage("");
	$.SortTab.setColor("#84868D");
	$.AreaTab.setBackgroundImage("/tool_left.png");
	$.AreaTab.setColor("#fff");
}

function ShowSortWin() {
	sortDialog.Show();
	$.SortTab.setBackgroundImage("/tool_right.png")
	$.SortTab.setColor("#fff");
	$.AreaTab.setBackgroundImage("")
	$.AreaTab.setColor("#84868D");
}

//**********事件结束***********