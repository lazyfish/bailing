var UI = require("UI");
//初始化用户信息
var users = Alloy.createCollection('UserModel');
users.fetch();
if (users.length > 0) {
	Alloy.Globals.LoginToken = users.at(0).get("Token")
};
Ti.API.debug("LoginId:" + Alloy.Globals.LoginToken)

$.index.open();

//定义选项卡Json信息
var mainTab = {
	icon: "/tag_home.png",
	hoverIcon: "/tag_home_pressed.png",
	controller: "main",
	callbackEvent: null
}
var activityTab = {
	icon: "/tag_group.png",
	hoverIcon: "/tag_group_pressed.png",
	controller: "Activity",
	callbackEvent: null
}
var vendorTab = {
	icon: "/tag_seller.png",
	hoverIcon: "/tag_seller_pressed.png",
	controller: "vendor",
	callbackEvent: null
}
var storeTab = {
	icon: "/tag_store.png",
	hoverIcon: "/tag_store_pressed.png",
	controller: "store",
	callbackEvent: null
}
var peopleTab = {
	icon: "/tag_people.png",
	hoverIcon: "/tag_people_pressed.png",
	controller: "self",
	callbackEvent: null
}
var moreTab = {
	icon: "/tag_more.png",
	hoverIcon: "/tag_more_pressed.png",
	controller: "more",
	callbackEvent: null
}
var tabJson = new Array(mainTab,activityTab, storeTab, peopleTab, moreTab);

//定义选项卡
var tabs = [$.HomeView, $.ActivityView, $.StoreView, $.PeopleView, $.MoreView];
var tabIcons = [$.HomeIcon, $.ActivityIcon, $.StoreIcon, $.PeopleIcon, $.MoreIcon];
var tabTitle = ["泉州",'活动信息', "周边店", "个人信息", "更多"]

//定义视图
var mainView = null;
var vendorView = null;
var storeView = null;
var peopleView = null;
var moreView = null;
var views = [mainView, storeView, peopleView, moreView]

views[0] = UI.Controller(tabJson[0].controller, {
	callback: tabJson[0].callbackEvent
})
$.MainView.add(views[0])
$.HomeIcon.setBackgroundImage(mainTab.hoverIcon);


//=======事件============

function ChangeMainTab(e) {
	ChangeTab(0)
}

function ChangeActivityTab(e) {
	ChangeTab(1)
}



function ChangeStoreTab(e) {
	ChangeTab(2)
}

function ChangePeopleTab(e) {
	if (Alloy.Globals.LoginToken == "") {
		UI.Open("Login");
	} else {
		ChangeTab(3)
	}
}

function ChangeMoreTab(e) {
	ChangeTab(4)
}

function InitTab(index) {
	for (var i in tabs) {
		if (index != i) {
			tabs[i].setColor("#9D9E9F")
			tabIcons[i].setBackgroundImage(tabJson[i].icon);
		}
	}
}

function ChangeTab(index) {
	if (views[index] == null) {
		views[index] = UI.Controller(tabJson[index].controller, {
			callback: tabJson[index].callbackEvent
		});
		$.MainView.add(views[index])
	};
	views[index].top=0;
	views[index].left=0;
	$.lblTitle.setText(tabTitle[index])
	tabs[index].setColor("#CC0000")
	tabIcons[index].setBackgroundImage(tabJson[index].hoverIcon);
	InitTab(index);
	for (var i in views) {
		if (views[i]) { //setOpacity
			// if (Alloy.Globals.osname == "ios") {
			// 	if (i == index) {
			// 		views[i].setOpacity(1)
			// 	} else
			// 		views[i].setOpacity(0)
			// }
			views[i].setVisible(i == index)
		};
	}
}


function Search(){
	UI.Open("Search");
}