var Ajax = require("Ajax");
var UI = require("UI");
var Dialog = require("Dialog");
var args = arguments[0] || {};
var callFun = args.callFun || null;
LoadParent();
var selectedId = "";

function LoadParent() {
	var url = Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=0";
	Ajax.json(url, function(item, i, length, total) {
		var view = InitItem(item.id, item.ItemName);
		view.Bind('click', function() {
			var children = $.ParentType.getChildren();
			for (var i in children) {
				children[i].setBackgroundImage("/content_bg.png");
			}
			this.setBackgroundImage("/catogory_bg_b.png")
			selectedId = "";
			LoadChild(this.tid);
		});
		$.ParentType.add(view);
	})
}

function LoadChild(id) {
	$.ChildType.removeAllChildren();
	var url = Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + id;
	Ajax.json(url, function(item, i, length, total) {
		var view = InitItem(item.id, item.ItemName);
		view.Bind('click', function() {
			selectedId = this.tid;
			var children = $.ChildType.getChildren();
			for (var i in children) {
				children[i].setBackgroundImage("/content_bg.png");
			}
			this.setBackgroundImage("/catogory_bg_b.png")
		});
		$.ChildType.add(view);
	})
}

function InitItem(id, name) {
	var viewStyle = {
		width: Ti.UI.SIZE,
		backgroundImage: "/content_bg.png",
		backgroundFocusedImage: "/catogory_bg_b.png",
		backgroundSelectedImage: "/catogory_bg_b.png",
		height: 30,
		left: "10",
		top: "10",
	};
	var fontStyle = {
		width: name.length * 18,
		height: 30,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		left: "10",
		right: "10",
		font: {
			size: 12
		},
		text: name,
		color: "#333333"
	}
	var viewItem = UI.C("view", viewStyle);
	var title = UI.C("label", fontStyle);
	viewItem.add(title);
	viewItem.tid = id;
	return viewItem;
}

function Ok() {
	if (selectedId != "") {
		var url = Alloy.Globals.ApiUrl + "/allclass.ashx?action=AddUserType&idguid=" + Alloy.Globals.LoginToken + "&TypeId=" + selectedId;
		Ti.API.debug("添加类别：" + url)
		Ajax.get(url, function(data) {
			var prompt = new Dialog.prompt();
			if (data == "ok") {
				prompt.Show("添加成功", 2000)
				callFun();
			} else {
				prompt.Show("添加失败，请重新添加", 2000)
			}
		})
	};
}


function Close(e) {
	$.win.close()
}