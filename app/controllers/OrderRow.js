var UI = require("UI");
var DateUtility = require("DateUtility");
var args = arguments[0] || {};
var id = args.id || '';
var sn = args.sn || '';
var price = args.price || '';
var time = args.time || '';
var OrderState = args.OrderState || '';
$.title.text = sn;
$.price.text=String.format('总额：%.2f元',parseFloat(price))
$.time.text = DateUtility.ToString(time,"yyyy-MM-dd HH:mm:ss");


function GotoDetail(e){
	UI.Open("OrderDetail",{id:id,sn:sn,price:price,time:time,
		OrderState:OrderState})
}