var Ajax = require("Ajax");

var location = null;
var hasLocation = false;
var hasShake = false;
var storeId = 0;
Ti.Geolocation.purpose = 'Get Current Location';
Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
if (Ti.Geolocation.locationServicesEnabled) {
	Ti.Geolocation.distanceFilter = 10;
	Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_NETWORK;

} else {
	alert('请启用您的GPS定位服务');
}
Titanium.Geolocation.addEventListener('location', function(e) {
	location = e.coords;
	hasLocation = true;
	if (hasShake) {
		Read();
	};
});
Ti.Geolocation.getCurrentPosition(function(e) {
	if (e.success) {
		location = e.coords;
		hasLocation = true;
		if (hasShake) {
			Read();
		};
	}
})
Ti.Gesture.addEventListener('shake', function(e) {
	hasShake = true;
	$.row.setVisible(true);
	if (hasLocation) {
		Read();
	};
})

function Read() {
	var url = Alloy.Globals.ApiUrl + "/store.ashx?action=shake&lng=" + location.longitude + "&lat=" + location.latitude;
	Ajax.json(url, function(item, i, length, total) {
		if (i == 0) {
			var face = Alloy.Globals.WebUrl + item.StorePicUrl;
			var title = item.StoreName;
			var id = item.id;
			var content = item.ContactTel;
			var range = item.Range + "米";
			$.face.setImage(face);
			$.title.setText(title);
			$.content.setText(content);
			$.range.setText(range);
			$.content.addEventListener('click', function(e) {
				Ti.Platform.openURL('tel:' + content);
				e.cancelBubble = true;
			});
			storeId = id;

		};
	}, function() {})
}

function GotoDetail(e) {
	if (storeId != 0) {
		Alloy.createController("storeDetail", {
			id: storeId
		}).getView().open();
	};
}

function Close(e) {
	$.win.close()
}