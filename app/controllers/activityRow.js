var args = arguments[0] || {};
var id = args.id || '';
var face = args.face || '';
var title = args.title || '';
var content = args.content || '';
var price = args.price || '';
var Storeid = args.Storeid || '';
var pics = args.pics || '';
var vendorId = args.vendorId || '';


Ti.API.info(id)
$.face.setImage(Alloy.Globals.WebUrl + face)
$.title.setText(title)
$.content.setText(String.format("参与价：%.2f元",parseFloat(price)))

function GotoDetail(e) {
	Alloy.createController("activityInfo", {
		id: id,
		face: pics,
		title: title,
		Storeid: Storeid,
		vendorId:vendorId,
		content: content,
		price: price
	}).getView().open();
}