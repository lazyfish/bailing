var UI = require("UI");
var Ajax = require("Ajax");
var Dialog = require("Dialog");
var args = arguments[0] || {};
var sum = 0.00;
var url = Alloy.Globals.ApiUrl + "/car.ashx?action=list&idguid=" + Alloy.Globals.LoginToken;
Ti.API.debug(url)
Ajax.json(url, function(item) {
	Ti.API.debug(String.format("%s|%s|%s", item.Price, item.SelNum, item.Discount))
	sum += parseFloat(item.Price) * parseFloat(item.SelNum) * parseFloat(item.Discount);
	Ti.API.debug(sum);
	return UI.Controller("BuyCarRow", {
		id: item.id,
		pid: item.ProductId,
		num: item.SelNum,
		price: sum,
		name: item.ProName,
		face: item.CoverPicture,
		type:item.Types,
		delFun: RemoveRow
	});
}, function(data) {
	$.table.setData(data);
});

function Close(e) {
	$.win.close();
}

function RemoveRow(e) {
	$.table.deleteRow(e.index)
}

function Settle(e) {
	// var url = Alloy.Globals.ApiUrl + "/order.ashx?action=add&idguid=" + Alloy.Globals.LoginToken + "&sum=" + sum;
	UI.Open("BuyView", {
		sum: sum,
		close: Close
	});
	// Ajax.get(url, function(data) {
	// 	var prompt = new Dialog.prompt();
	// 	prompt.Show("结算成功！", 2000);
	// 	setTimeout(function() {
	// 		$.win.close();
	// 	}, 2000);
	// })
}