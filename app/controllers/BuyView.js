var UI = require("UI");
var Dialog = require("Dialog");
var args = arguments[0] || {};
var sum = args.sum;
var url = Alloy.Globals.WebUrl + "/Mobile/alipay/MobilePay.aspx?action=add&idguid=" + Alloy.Globals.LoginToken + "&sum=" + sum;
var closeFun = args.close;
$.WebView.url = url;
Ti.API.debug(url);

function Close() {
	$.win.close();
}

function BeforeLoad(e) {
	Ti.API.info("BeforeLoad" + e.url);
}

var loadView = new Dialog.load();

function Load(e) {
	Ti.API.info("load" + e.url)
	if (e.url.indexOf("call_back_url.aspx") != -1) {
		loadView.Show("订单处理中，请稍等");
	} else if (e.url.indexOf("payfinish.html") != -1) {
		loadView.Hide();
		closeFun();
		Close();
		if (e.url.indexOf("result=ok") != -1) {
			//交易成功
			alert("交易成功")
			UI.Open("Order", {
				State: "7"
			})
		} else {
			alert("交易过程出现未知错误，请到订单中完成支付")
			UI.Open("Order", {
				State: "1"
			})
		}
	};
}