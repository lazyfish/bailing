var Ajax = require("Ajax");
var UI = require("UI");
var ListDialog = require("ListDialog");
var typeDialog;
var areaDialog;
var type = ''; //当前类别
var area = ''; //当前地区
var url = '';
var moreView = null;

Init();

function Init() {
	LoadIndustryType()
	LoadArea()
	BindTable();
}

var actInd = Titanium.UI.createActivityIndicator({
	width: Ti.UI.SIZE,
	height: Ti.UI.SIZE,
	color: "#fff",
	message: '获取位置中...'
});
$.win.add(actInd)
actInd.show();

//******配置定位信息******
Ti.Geolocation.purpose = 'Get Current Location';
Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
if (Alloy.Globals.osname == "ios") {
	if (Ti.Geolocation.locationServicesEnabled) {
		Ti.Geolocation.distanceFilter = 10;
		Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_NETWORK;

	} else {
		alert('请启用您的GPS定位服务');
	}
} else if (Alloy.Globals.osname == "android") {
	var providerGps = Ti.Geolocation.Android.createLocationProvider({
		name: Ti.Geolocation.PROVIDER_NETWORK,
		minUpdateDistance: 0.0,
		minUpdateTime: 0
	});
	Ti.Geolocation.Android.addLocationProvider(providerGps);
	Ti.Geolocation.Android.manualMode = true;
}

//******配置定位信息结束******

//**********加载行业类别*********

//行业顶级分类

function LoadIndustryType() {
	typeDialog = new ListDialog.option();
	typeDialog.Init($.win, {
		left: "5dp",
		top: "30dp"
	}, {
		width: "300dp",
		height: "240dp"
	}, [], []);
	typeDialog.OnRootClick = function(e) {
		if (e.id == 0) {
			type = e.id
			BindTable()
			typeDialog.Hide();
		} else {
			Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + e.id, LoadingIndustryChildItem, LoadedIndustryChildItem)
		}
	}
	typeDialog.OnChildClick = function(e) {
		type = e.id
		BindTable()
		typeDialog.Hide();
	}

	Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=0", LoadingIndustryRootItem, LoadedIndustryRootItem)
}

function LoadingIndustryRootItem(item, i, length, total) {
	return {
		id: item.id,
		name: item.ItemName
	}
}

function LoadedIndustryRootItem(data) {
	data.unshift({
		id: "0",
		name: "全部"
	})
	typeDialog.BindData(data);
	if (data.length > 1) {
		Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetIndustryChild&t=" + data[1].id, LoadingIndustryChildItem, LoadedIndustryChildItem)
	};
}

//行业子分类
function LoadingIndustryChildItem(item, i, length, total) {
	return {
		id: item.id,
		name: item.ItemName,
		parent: item.ParentId
	}
}

function LoadedIndustryChildItem(data) {
	data.unshift({
		id: typeDialog._rootId,
		name: "全部"
	})
	typeDialog.BindData(null, data);
}

//**********加载行业类别结束*********

//************加载地区************

function LoadArea() {
	areaDialog = new ListDialog.option();
	left = Alloy.Globals.width / 2;
	areaDialog.Init($.win, {
		left: left,
		top: "30dp"
	}, {
		width: "140dp",
		height: "240dp"
	}, []);
	Ajax.json(Alloy.Globals.ApiUrl + "/ct.ashx?action=getarea&code=350500",
		function(item) {
			return {
				id: item.CTCode,
				name: item.CTName
			}
		}, function(data) {
			data.unshift({
				id: '350500',
				name: '全城'
			})
			areaDialog.BindData(data)
		})
	areaDialog.OnRootClick = function(e) {
		area = e.id;
		BindTable()
		areaDialog.Hide();
	}
}
//************加载地区结束************

//*********绑定门店************

var hasLoad = false;

function BindTable() {
	Titanium.Geolocation.addEventListener('location', function(e) {
		if (!hasLoad) {
			GetTable(e.coords)
		};
	});
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (e.success) {
			if (!hasLoad) {
				GetTable(e.coords)
			};
		}
	})
}

function GetTable(coords) {
	hasLoad = true;
	url = Alloy.Globals.ApiUrl + "/store.ashx?action=getstorelist&lng=" + coords.longitude + "&lat=" + coords.latitude;
	if (type) {
		url += "&t=" + type;
	};
	if (area) {
		url += "&area=" + area;
	}
	Ti.API.info(url)
	if (!moreView) {
		moreView = UI.Controller("MoreRow", {
			url: url,
			index: 0,
			itemFun: CreateStoreItem,
			finishFun: FinishLoadStore
		});
	};
	Ajax.json(url, CreateStoreItem, FinishLoadStore)

}
var dataList = new Array();

function CreateStoreItem(item, i, length, total) {
	var dataItem = Alloy.createController("vendorStoreRow", {
		face: Alloy.Globals.WebUrl + item.StorePicUrl,
		title: item.StoreName,
		id: item.id,
		content: item.ContactTel,
		range: item.Range + "米"
	}).getView()
	dataList.push(dataItem)
	return dataItem;
}

function FinishLoadStore(data, total) {
	$.table.setData(dataList)
	if (parseInt(total) > dataList.length) {
		$.table.appendRow(moreView)
	};
	actInd.hide();
}

//*********绑定门店结束************

//**********事件***********

function ShowTypeWin() {
	typeDialog.Show();
	$.AreaTab.setBackgroundImage("")
	$.AreaTab.setColor("#84868D");
	$.TypeTab.setBackgroundImage("/tool_left.png")
	$.TypeTab.setColor("#fff");
}

function ShowAreaWin() {
	areaDialog.Show()
	$.AreaTab.setBackgroundImage("/tool_right.png")
	$.AreaTab.setColor("#fff");
	$.TypeTab.setBackgroundImage("")
	$.TypeTab.setColor("#84868D");
}

//**********事件结束***********