var Ajax = require("Ajax");
var Utility = require("Utility");
var UI = require("UI");
var args = arguments[0] || {};
var id = args.id || '';
var NextExperience = args.NextExperience||0;
var vendorId;
var vendorName;
Ti.API.debug("id:",id,"NextExperience",NextExperience)
if (id) {
	BindCardDetail();
	$.GoVendorButton.setVisible(true);
} else {
	ReadBailing();
	$.GoVendorButton.setVisible(false);
}

function ReadBailing() {
	var url = Alloy.Globals.ApiUrl + "/user.ashx?action=blcard&idguid=" + Alloy.Globals.LoginToken;
	Ti.API.debug("LoadCard" + url);
	Ajax.json(url, function(item,i) {
		if (i>0) {
			return false;
		};
		if (!NextExperience) {
			NextExperience = item.Grade
		};
		$.Title.setText("我的百领卡")
		$.Name.setText(String.format("会员卡名称：%s", item.Name));
		$.Discount.setText(String.format("享有折扣：%.2f折", parseFloat(item.Discount) / 100));
		$.Remark.html = Utility.GetWebView(item.Remark);
		$.Progress.setWidth((parseFloat(NextExperience) / parseFloat(item.Grade) * 100) + "%");
		$.CarFace.setImage(Alloy.Globals.WebUrl + item.Face);
		vendorId = item.VendorId;
		vendorName = item.VendorName;
	})
}

function BindCardDetail() {
	var url = Alloy.Globals.ApiUrl + "/user.ashx?action=cardinfo&id=" + id + "&idguid=" + Alloy.Globals.LoginToken;
	Ti.API.debug("LoadCard" + url);
	Ajax.json(url, function(item) {
		if (!NextExperience) {
			NextExperience = item.Experience
		};
		$.Vendor.setText(String.format("商     家：%s", item.VendorName));
		$.Name.setText(String.format("会员卡名称：%s", item.Name));
		$.Discount.setText(String.format("享有折扣：%.2f折", parseFloat(item.Discount) / 100));
		$.Remark.html = Utility.GetWebView(item.Remark);
		$.Progress.setWidth((parseFloat(item.Experience) / parseFloat(NextExperience) * 100) + "%");
		$.CarFace.setImage(Alloy.Globals.WebUrl + item.Face);
		vendorId = item.VendorId;
		vendorName = item.VendorName;
	})
}

function Close() {
	$.win.close();
}

function Go() {
	UI.Open("vendorStore", {
		id: vendorId,
		vendor: vendorName
	});
}
