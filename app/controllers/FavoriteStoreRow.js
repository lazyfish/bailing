var Ajax = require("Ajax");
var Dialog = require("Dialog");
var args = arguments[0] || {};
var id = args.id || '';
var storeId = args.storeId || '';
var face = args.face || '';
var title = args.title || '';
var content = args.content || '';
var delFun = args.delFun || '';

$.face.setImage(face)
$.title.setText(title)
$.content.setText(content)
$.content.addEventListener('click',function(e){
	Ti.Platform.openURL('tel:' + content);
	e.cancelBubble = true;
})

function GotoDetail(e){
	Alloy.createController("storeDetail",{id:storeId}).getView().open();
}

function Del(e) {
	var url = Alloy.Globals.ApiUrl + "/concern.ashx?action=del&id=" + id;
	Ajax.get(url, function(data) {
		if (data == "ok") {
			var prompt = new Dialog.prompt();
			prompt.Show("删除成功！", 2000)
			delFun(e);
		};
	})
	e.cancelBubble = true;
}