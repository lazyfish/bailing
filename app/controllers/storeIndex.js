try {
	InitInfoBox();
} catch(ex) {
	alert(ex.message)
}
function InitInfoBox() {
	try {
		GetInfoItem("商家简介", function(e) {
			Alloy.createController("vendorIntroduce").getView().open();
		})
		GetInfoItem("商品展示", function(e) {
			Alloy.createController("vendorProduct").getView().open();
			// Alloy.createController("product").getView().open();
		})
		GetInfoItem("连锁门店", function(e) {
			Alloy.createController("vendorStore").getView().open();
		})
		GetInfoItem("热线电话  400-000-00000", function(e) {
			Titanium.Platform.openURL('tel:10086')
		}, false)
	} catch(ex) {
		alert(ex.message)
	}
}

function GetInfoItem(title, clickEvent, addBorder) {
	addBorder = addBorder || true
	var view = Ti.UI.createView({
		height : "40dp",
		width:Ti.UI.FILL,
	})
	var label = Ti.UI.createLabel({
		text : title,
		color : "#fff",
		height : "40dp",
		left:"30dp",
		textAlign:Ti.UI.TEXT_ALIGNMENT_LEFT,
		font : {
			fontSize : "15dp",
			fontWeight : "bold"
		}
	})
	var ic = Ti.UI.createImageView({
		image:"/ic_arror_right.png",
		width:"7dp",
		height:Ti.UI.SIZE,
		right:"20dp"
	})
	if (clickEvent) {
		view.addEventListener("click", function(e) {
			clickEvent(e)
		})
	};
	if (addBorder) {
		var border = Ti.UI.createView({
			height : "1dp",
			backgroundColor : "#2E2E2E"
		})
		var border2 = Ti.UI.createView({
			height : "1dp",
			backgroundColor : "#6C6D6D"
		})
		$.InfoBox.add(border)
		$.InfoBox.add(border2)
	};
	view.add(label)
	view.add(ic)
	$.InfoBox.add(view)
}

function getProperties(title) {
	return {
		template : Ti.UI.LIST_ITEM_TEMPLATE_CONTACTS,
		properties : {
			title : title,
			color : "#fff",
			height : "40dp",
			font : {
				fontSize : "18dp",
				fontWeight : "bold"
			},
			accessoryType : Ti.UI.LIST_ACCESSORY_TYPE_DISCLOSURE
		}
	}
}

function Close(e) {
	$.win.close()
}
