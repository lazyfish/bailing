var UI = require("UI");
var Ajax = require("Ajax");
var url = '';
var moreView = null;
var dataList = new Array();


var url = Alloy.Globals.ApiUrl + "/user.ashx?action=card&idguid=" + Alloy.Globals.LoginToken;
if (!moreView) {
	moreView = UI.Controller("MoreRow", {
		url: url,
		index: 0,
		itemFun: LoadItem,
		finishFun: LoadFinish
	});
};
Ti.API.debug(url)
Ajax.json(url, LoadItem, LoadFinish);

function Close(e) {
	$.win.close();
}

function LoadItem(item) {
	var dataItem = UI.Controller("CardRow", {
		data: item
	});
	dataList.push(dataItem)
	return dataItem;
}

function LoadFinish(data, total) {
	$.table.setData(dataList)
	if (parseInt(total) > dataList.length) {
		$.table.appendRow(moreView)
	};
}