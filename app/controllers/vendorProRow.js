var args = arguments[0] || {};
var id = args.id || '';
var face = args.face || '';
var title = args.title || '';
var content = args.content || '';
var price = args.price || '';
var Storeid = args.Storeid || '';
var pics = args.pics || '';
var vendorId = args.vendorId || '';
var discount = args.discount || '';
var vipName = args.vipName || '';


$.face.setImage(Alloy.Globals.WebUrl + face)
$.title.setText(title)
$.content.setText("原价：" + parseFloat(price).toFixed(2) + "元")
if (vipName) {
	$.discountPrice.setText(vipName+"：" + parseFloat(price).toFixed(2) + "元")
};

function GotoDetail(e) {
	Alloy.createController("product", {
		id: id,
		face: pics,
		title: title,
		Storeid: Storeid,
		vendorId:vendorId,
		discount:discount,
		price:price,
		content: content
	}).getView().open();
}