var UI = require("UI");
var Ajax = require("Ajax");
var args = arguments[0] || {};

var url = Alloy.Globals.ApiUrl + "/product.ashx?action=getuncomment&idguid=" + Alloy.Globals.LoginToken;
Ti.API.info(url)
Ajax.json(url, function(item) {
	return UI.Controller("CommentProRow", {
			id : item.Pid,
			face : item.CoverPicture,
			title : item.ProName,
			content : item.ProContent,
			price : item.Price,
			storeId : item.StoreId,
			pics : item.ProPics
		})
}, function(data) {
	$.table.setData(data);
})

function Close(e) {
	$.win.close()
}
