var Ajax = require("Ajax");
var UI = require("UI");
var keyword = '';
var searchType = 0;
var url = '';
var moreView = null;
var dataList = new Array();

LoadDefaultKeyword();

function Search(e) {
	dataList = new Array();
	$.HotKeyword.setVisible(false);
	$.search.blur();
	if (searchType == 0) //搜索商品
	{
		BindProduct(e.value);
	} else {
		BindVendor(e.value);
	}
}

function BindProduct(keyword) {
	$.VendorTable.setVisible(false);
	$.ProductTable.setVisible(true);
	url = Alloy.Globals.ApiUrl + "/product.ashx?action=search&keyword=" + keyword;
	moreView = UI.Controller("MoreRow", {
		url: url,
		index: 0,
		itemFun: LoadProductItem,
		finishFun: LoadProductFinish
	});

	Ajax.json(url, LoadProductItem, LoadProductFinish)
}

function LoadProductItem(item, i, length, total) {

	var dataItem = UI.Controller("vendorProRow", {
		id: item.id,
		face: item.CoverPicture,
		title: item.ProName,
		content: item.ProContent,
		Storeid: item.StoreId,
		// vipName:vipName,
		vendorId: item.VendorId,
		// discount: discount,
		price: item.oPrice,
		pics: item.ProPics
	});
	dataList.push(dataItem)
	return dataItem;
}

function LoadProductFinish(data, total) {
	$.ProductTable.setData(dataList);
	if (parseInt(total) > dataList.length) {
		$.ProductTable.appendRow(moreView)
	};
}

function BindVendor(keyword) {
	$.VendorTable.setVisible(true);
	$.ProductTable.setVisible(false);
	url = Alloy.Globals.ApiUrl + "/store.ashx?action=getverdonlist&t=&area=&keyword=" + keyword + "&sort=";
	moreView = UI.Controller("MoreRow", {
		url: url,
		index: 0,
		itemFun: LoadVendorItem,
		finishFun: LoadVendorFinish
	});

	Ti.API.debug(url)
	Ajax.json(url, LoadVendorItem, LoadVendorFinish)
}

function LoadVendorItem(item, total) {
	var dataItem = Alloy.createController("vendorRow", {
		id: item.id,
		face: Alloy.Globals.WebUrl + item.StorePicUrl,
		title: item.StoreName,
		content: ""
	}).getView();
	dataList.push(dataItem)
	return dataItem;
}

function LoadVendorFinish(data) {
	$.VendorTable.setData(dataList);
	if (parseInt(total) > dataList.length) {
		$.VendorTable.appendRow(moreView);
	};
}

function ChangeSearchType(e) {
	searchType = e.index;
	Ti.API.debug(e.index);
}

function LoadDefaultKeyword() {
	var url = Alloy.Globals.ApiUrl + "/system.ashx";
	Ajax.json(url, function(item, i, length, total) {
		var keyList = item.HotTerm.split(',');
		for (var i in keyList) {
			if (keyList[i]) {
				$.HotKeyword.add(InitItem(keyList[i]));
			};
		}
	})
}

function InitItem(name) {
	var viewStyle = {
		width: Ti.UI.SIZE,
		backgroundImage: "/content_bg.png",
		backgroundFocusedImage: "/catogory_bg_b.png",
		backgroundSelectedImage: "/catogory_bg_b.png",
		height: 30,
		left: "10",
		top: "10",
	};
	var fontStyle = {
		width: name.length * 18,
		height: 30,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		left: "10",
		right: "10",
		font: {
			size: 12
		},
		text: name,
		color: "#333333"
	}
	var viewItem = UI.C("view", viewStyle);
	var title = UI.C("label", fontStyle);
	viewItem.add(title);
	return viewItem;
}

function Close(e) {
	$.win.close()
}