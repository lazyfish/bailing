var args = arguments[0] || {};
var ClickTypeCallback = args.callback || '';
var AdList = new Array();
var AdTitleList = new Array();
var titles = new Array();
var Ajax = require("Ajax");
var UI = require("UI");
var Dialog = require("Dialog");
LoadAd();
LoadType();

//***********加载类别***********

function LoadType() {
	Ajax.json(Alloy.Globals.ApiUrl + "/allclass.ashx?action=GetTypeByUser&idguid=" + Alloy.Globals.LoginToken, TypeItemFun, TypeLoaded)
}

function TypeItemFun(item, i, length, total) {
	if (i == 0) {
		$.TypeGrid.removeAllChildren();
	};
	try {
		$.TypeGrid.add(InitTypeItme(item.id, item.ItemName, Alloy.Globals.WebUrl + item.PicUrl, item.IsShow));
	} catch (e) {
		Ti.API.debug("Error:" + JSON.stringify(e))
	}
}

function TypeLoaded() {
	$.TypeGrid.add(InitTypeItme("0", "添加", "/ic_add.png"));
}

function InitTypeItme(id, name, face, show) {
	var viewWidth = (Alloy.Globals.width / 3) - 20;
	var viewStyle = {
		width: viewWidth,
		height: viewWidth,
		backgroundImage: "/bg1.png",
		left: "10",
		top: "10",
		right: "10"
	};
	var imgStyle = {
		width: "30dp",
		height: "30dp",
	};
	var titleStyle = {
		top: "10dp",
		color: "#fff",
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		font: {
			fontSize: "14dp"
		}
	};
	var viewBox = UI.C("view", viewStyle);
	var viewCenter = UI.C("view", {
		layout: "vertical",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE
	});
	var icon = UI.C("image", imgStyle)
	if (face) {
		icon.image = face;
	};
	var title = UI.C("label", titleStyle);
	title.text = name;
	viewCenter.add(icon);
	viewCenter.add(title);
	viewBox.add(viewCenter);
	viewBox.tid = id
	viewBox.Bind('click', function(e) {
		if (this.tid == "0") {
			UI.Open("Type", {
				callFun: LoadType
			});
		} else {
			UI.Open("vendor", {
				type: this.tid
			});
		}
	});
	if (show == "False") {
		viewBox.Bind('longpress', function(e) {
			var eventObj = this;
			var alertSure = new Dialog.alertSure();
			alertSure.Show('确认要删除此类别？', function() {
				var url = Alloy.Globals.ApiUrl + "/allclass.ashx?action=DelUserType&idguid=" + Alloy.Globals.LoginToken + "&TypeId=" + eventObj.tid;
				Ajax.get(url,function(){});
				$.TypeGrid.remove(eventObj);
			})
		});
	};

	return viewBox;
}

//***********加载类别结束***********
//***********加载广告************

function LoadAd() {
	Ajax.json(Alloy.Globals.ApiUrl + "/ad.ashx?action=list", AdItemFun, function() {
		for (var i in titles) {
			titles[i].Bind("click", function(e) {
				$.AdImages.scrollToView(parseInt(this.index));
				// $.AdImages.currentPage = this.index;
				for (var j in AdTitleList) {
					AdTitleList[j].setBackgroundColor("#666");
				}
				AdTitleList[this.index].setBackgroundColor("#C13C46")
			})
		}
	})
}

function AdItemFun(item, i, length) {
	var titleWidth = Alloy.Globals.width / length;
	var img = UI.C("image", {
		image: Alloy.Globals.WebUrl + item.PicUrl,
		width: "100%",
		height: "100%"
	})
	var titleStyle = {
		width: titleWidth,
		height: "30dp",
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		backgroundColor: "#666",
		backgroundSelectedColor: "#C13C46",
		color: "#fff",
		font: {
			fontSize: "14dp"
		}
	}
	if (i == 0) {
		titleStyle.backgroundColor = "#C13C46";
	};
	titleStyle.text = item.PicAlt
	var title = UI.C("label", titleStyle)
	title.index = i;
	AdTitleList.push(title);

	$.AdImages.addView(img)
	$.AdTitles.add(title)
	titles.push(title)
}
var adIndex = 1;

function ChangeAdTitle(e) {
	for (var i in AdTitleList) {
		AdTitleList[i].setBackgroundColor("#666");
	}
	AdTitleList[e.currentPage].setBackgroundColor("#C13C46")
}

function GotoTypeList(e) {
	UI.Open("vendor");
}

function Like(){
	UI.Open("Like");
}

function Shake(){
	UI.Open("Shake");
}





