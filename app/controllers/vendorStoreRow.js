var args = arguments[0] || {};
var id = args.id || '';
var face = args.face || '';
var title = args.title || '';
var content = args.content || '';
var range = args.range || '';

$.face.setImage(face)
$.title.setText(title)
$.content.setText(content)
$.range.setText(range)
$.content.addEventListener('click',function(e){
	Ti.Platform.openURL('tel:' + content);
	e.cancelBubble = true;
})

function GotoDetail(e){
	Alloy.createController("storeDetail",{id:id,name:title}).getView().open();
}
