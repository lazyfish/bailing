exports.definition = {
	config: {
		columns: {
		    "Url": "string",
		    "Json": "string"
		},
		adapter: {
			type: "sql",
			collection_name: "HttpCache",
			db_name:"HttpCache"
		}
	},
	extendModel: function(Model) {		
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});
		
		return Model;
	},
	extendCollection: function(Collection) {		
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			
		});
		
		return Collection;
	}
}

