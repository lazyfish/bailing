exports.definition = {
	config: {
		columns: {
		    "Token": "string"
		},
		adapter: {
			type: "sql",
			collection_name: "UserModel"
		}
	},		
	extendModel: function(Model) {		
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});
		
		return Model;
	},
	extendCollection: function(Collection) {		
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});
		
		return Collection;
	}
}

