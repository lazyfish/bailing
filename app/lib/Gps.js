exports.GetCurPoint = function(back,body,callback) {
	body.setZIndex(1)
	if (callback) {
		var mapview = Titanium.Map.createView({
			mapType: Titanium.Map.STANDARD_TYPE,
			region: {
				latitude: 37.389569,
				longitude: -122.050212,
				latitudeDelta: 0.1,
				longitudeDelta: 0.1
			},
			animate: true,
			opacity: 0,
			regionFit: true,
			zIndex: 0,
			userLocation: false
		});

		mapview.addEventListener('regionChanged', callback);
		back.add(mapview)
	};
}