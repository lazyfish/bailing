var Controls = [];
exports.GetById = function(id) {
	for (var i in Controls) {

	}
}
exports.C = function(element, style, id) {
	var view;
	if (element == "view") {
		view = Ti.UI.createView(style);
	} else if (element == "image") {
		view = Ti.UI.createImageView(style);
	} else if (element == "label") {
		view = Ti.UI.createLabel(style);
	}else if (element == "text") {
		view = Ti.UI.createTextField(style);
	}else if (element == "webview") {
		view = Ti.UI.createWebView(style);
	}
	if (id) {
		view.id = id;
		Controls.push(view);
	};
	view.Bind = function(event,fun){
		view.addEventListener(event,fun);
	}
	return view;
}
exports.Controller = function(name, param) {
	return Alloy.createController(name, param).getView()
}
exports.Open = function(name, param) {
	Alloy.createController(name, param).getView().open();
}
