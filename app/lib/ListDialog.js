exports.option = function() {
	var self = this;
	self = {
		lastTypeIndex: 1,
		_body: null,
		_windowGround: null,
		_typeBox: null,
		_typeWin: null,
		_childTypeWin: null,
		_rootList: null,
		_childList: null,
		_rootId: 0,
		Init: function(body, position, size, data, childData) {
			self._body = body;
			self._initBackground();
			self._initDialog(position, size, data, childData)
		},
		Show: function() {
			Ti.API.info("Show")
			self._windowGround.animate({
				opacity: 0.4,
				visible: true,
				duration: 500
			})
			self._typeBox.animate({
				opacity: 1,
				visible: true,
				duration: 500
			})
		},
		Hide: function() {
			self._windowGround.animate({
				opacity: 0,
				visible: false,
				duration: 500
			})
			self._typeBox.animate({
				opacity: 0,
				visible: false,
				duration: 500
			});
		},
		SetCurrentRoot: function(id) {
			var rootRows = _rootList.sections[0].rows;
			for (var i in rootRows) {
				var row = rootRows[i];
				if (row.id == id) {
					row.backgroundColor = "#505050";
				} else {
					row.setBackgroundColor("#5F5F5F");
				}
				_rootList.updateRow(i, row);
			}
		},
		OnRootClick: function(e) {

		},
		OnChildClick: function(e) {},
		BindData: function(data, childData) {
			var lastTypeIndex = 0;
			var rows = [],
				childRows = [];
			if (data) {
				for (var i in data) {
					var row = Ti.UI.createTableViewRow({
						title: data[i].name,
						id: data[i].id,
						height: "30dp",
						color: "#fff",
						font: {
							fontSize: "14dp"
						}
					})
					if (i == 1) {
						row.backgroundColor = "#505050";
						self._rootId = data.id;
					};
					rows.push(row)
				}

				var listView = Ti.UI.createTableView({
					separatorColor: "#bbb",
					backgroundColor: "#5F5F5F",
					data: rows
				})
				listView.addEventListener("ShowChild", this.OnRootClick);
				listView.addEventListener("click", function(e) {
					var lastRow = e.section.getRows()[self.lastTypeIndex];
					lastRow.setBackgroundColor("#5F5F5F");
					var current = e.row;
					current.setBackgroundColor("#505050");

					listView.updateRow(self.lastTypeIndex, lastRow);
					listView.updateRow(e.index, current);
					self.lastTypeIndex = e.index;
					Ti.API.info("id:" + current.id)
					self._rootId = current.id;
					listView.fireEvent('ShowChild', {
						id: current.id
					})
				})
				_rootList = listView;
				self._typeWin.add(listView);
			};

			if (childData && self._childTypeWin) {
				for (var i in childData) {
					childRows.push(Ti.UI.createTableViewRow({
						title: childData[i].name,
						id: childData[i].id,
						height: "30dp",
						color: "#fff",
						font: {
							fontSize: "14dp"
						}
					}))
				}

				var listView2 = Ti.UI.createTableView({
					backgroundColor: "#535353",
					separatorColor: "#ccc",
					data: childRows
				})
				listView2.addEventListener('ShowStore', this.OnChildClick)
				listView2.addEventListener('click', function(e) {
					listView2.fireEvent('ShowStore', {
						id: e.row.id
					})
				})
				_childList = listView2;
				self._childTypeWin.add(listView2);
			};
		},

		_initDialog: function(position, size, data, childData) {
			self._typeBox = Ti.UI.createView({
				width: size.width,
				left: position.left,
				top: position.top,
				opacity: 0,
				zIndex: 20,
				visible: false,
				layout: "vertical",
				height: size.height
			})
			var typeBoxTop = Ti.UI.createView({
				height: '10dp',
				backgroundImage: "/bg_top1.png"
			})
			var typeBoxMid = Ti.UI.createView({
				height:(parseInt(size.height.replace("dp",""))-20) + "dp",
				layout: "horizontal",
				backgroundImage: "/bg_mid1.png"
			})
			var typeBoxBottom = Ti.UI.createView({
				height: '10dp',
				backgroundImage: "/bg_bottom1.png"
			})
			self._typeWin = Ti.UI.createView({
				width: "49%",
				left: "2dp",
				height: "220dp",
			})
			typeBoxMid.add(self._typeWin)
			if (childData) {
				var typeBoxTopRight = Ti.UI.createView({
					height: '10dp',
					right: "0dp",
					width: "150dp",
					backgroundImage: "/bg_top_right1.png"
				})
				typeBoxTop.add(typeBoxTopRight)
				var typeBoxBottomRight = Ti.UI.createView({
					height: '10dp',
					right: "0dp",
					width: "150dp",
					backgroundImage: "/bg_bottom_right1.png"
				})
				typeBoxBottom.add(typeBoxBottomRight)
				self._childTypeWin = Ti.UI.createView({
					backgroundImage: "/bg_mid_right1.png",
					backgroundColor: "red",
					width: "49%",
					height: "220dp",
				})
				typeBoxMid.add(self._childTypeWin)
			} else {
				self._typeWin.width = Ti.UI.FILL
			}
			self._typeBox.add(typeBoxTop)
			self._typeBox.add(typeBoxMid)
			self._typeBox.add(typeBoxBottom)
			self._body.add(self._typeBox)

			self.BindData(data, childData);
		},
		_initBackground: function() {
			self._windowGround = Ti.UI.createView({
				width: Ti.UI.FILL,
				height: Ti.UI.FILL,
				left: "0",
				top: "0",
				right: "0",
				bottom: "0",
				opacity: 0,
				zIndex: 10,
				visible: false,
				backgroundColor: "#000"
			})
			self._windowGround.addEventListener("click", function() {
				self._windowGround.animate({
					opacity: 0,
					visible: false,
					duration: 500
				})
				self._typeBox.animate({
					opacity: 0,
					visible: false,
					duration: 500
				})
			})
			self._body.add(self._windowGround)
		}
	}
	return self;
}